<?php

class Dropship_Admin_Menu
{
	public function __construct()
	{
        add_action( 'admin_bar_menu', [ $this, 'dsp_remove_menu_bar_and_add_custom_menu_instead' ], 999 , 1);
        add_action( 'admin_head', [ $this, 'dsp_hide_menu_for_sale' ] );
        add_action( 'admin_menu', [ $this, 'dsp_register_admin_main_menu' ] );

        add_filter( 'login_redirect' , [ $this , 'dsp_admin_default_page' ] , 10, 3 );
        add_filter('woocommerce_product_data_tabs', [$this, 'dsp_remove_product_data'], 10, 1);
	}

	// ซ่อนเมนูและเพิ่มเมนูใหม่แทน
	function dsp_remove_menu_bar_and_add_custom_menu_instead( $wp_admin_bar ) {
		if (dsp_is_sale()) {
			// $wp_admin_bar->remove_node( 'wp-logo' );
			$wp_admin_bar->remove_node( 'comments' );
			$wp_admin_bar->remove_node( 'new-content' );
			$wp_admin_bar->remove_node( 'site-name' );
			$wp_admin_bar->remove_node( 'menu-toggle' );

			$links = [
				'dropship-dashboard' => __('การจัดการงานขาย', 'dropship'),
				'dropship-manage-catalog' => __('หน้ารวมสินค้า', 'dropship'),
				'dropship-manage-banners' => __('จัดการแบนเนอร์', 'dropship'),
			];

			foreach ($links as $id => $text) {
				$args = array(
					'id'    => $id,
					'title' => $text,
					'href'  => admin_url( 'admin.php?page=' . $id ),
					'meta'  => array(
						'class' => 'my-toolbar-page'
					)
				);
				$wp_admin_bar->add_node( $args );
			}
			?>
			<?php
		} else {
			$wp_admin_bar->remove_node( 'wp-logo' );
		}
	}

	// ซ่อนเมนูซ้าย และ เพิ่ม style sheet ทดสอบ
	function dsp_hide_menu_for_sale() {
		if (dsp_is_sale()) {
			remove_menu_page( 'edit-comments.php' );
			remove_menu_page( 'tools.php' );
			remove_menu_page( 'profile.php' );
			remove_menu_page( 'upload.php' );
			remove_menu_page( 'edit.php' );
			?>
				<style>
					@media only screen and (min-width: 780px) {
						.auto-fold #adminmenu, .auto-fold #adminmenu li.menu-top, .auto-fold #adminmenuback, .auto-fold #adminmenuwrap {
							display: none;
						}

						#wpcontent, #wpfooter , .auto-fold #wpcontent, .auto-fold #wpfooter {
							margin-left: 0px;
						}

						.my-toolbar-page {
							display: block;
						}
					}
				</style>
			<?php
		} elseif ( dsp_is_headsale( null , false) ) {
			remove_menu_page( 'options-general.php' );
            remove_menu_page( 'edit.php' );
            remove_menu_page( 'edit-comments.php' );
            remove_menu_page( 'tools.php' );
            remove_menu_page( 'plugins.php' );
            remove_menu_page( 'edit.php?post_type=page' );
            remove_menu_page( 'themes.php' );

			remove_menu_page( 'wp-mail-smtp' );

            remove_menu_page( 'woocommerce-marketing' );
			remove_menu_page('wc-admin&path=/analytics/overview');

            remove_submenu_page( 'woocommerce', 'wc-admin' );
            remove_submenu_page( 'woocommerce', 'wc-status' );
            remove_submenu_page( 'woocommerce', 'wc-addons' );
        } elseif ( dsp_is_operation() ) {
            remove_menu_page( 'edit.php' );
            remove_menu_page( 'edit-comments.php' );
            remove_menu_page( 'tools.php' );

			remove_menu_page( 'wp-mail-smtp' );

			remove_menu_page( 'woocommerce-marketing' );
			remove_menu_page('wc-admin&path=/analytics/overview');

            remove_submenu_page( 'woocommerce', 'wc-admin' );
            remove_submenu_page( 'woocommerce', 'wc-status' );
            remove_submenu_page( 'woocommerce', 'wc-addons' );
        }
	}

    // redirect มาหน้า dashboard
	function dsp_admin_default_page( $redirect_to, $request, $user ) {
		if ( ( dsp_is_sale($user) || dsp_is_headsale($user) ) ) {
			$redirect_to = admin_url('admin.php?page=dropship-dashboard');
		}
		if ( dsp_is_operation( $user ) ) {
			$redirect_to = admin_url('edit.php?post_type=shop_order');
		}
		return $redirect_to;
	}

    //  admin menu
	function dsp_register_admin_main_menu() {
		$Dropship_Admin_Dashboard = new Dropship_Admin_Dashboard();
		$Dropship_Admin_ManageCatalog = new Dropship_Admin_ManageCatalog();
		$Dropship_Admin_Commission = new Dropship_Admin_Commission();
		$Dropship_Admin_Banners = new Dropship_Admin_Banners();
		$Dropship_Admin_Order_Detail = new Dropship_Admin_Order_Detail();
		$Dropship_Admin_Settings = new Dropship_Admin_Settings();
		$Dropship_Admin_Report = new Dropship_Admin_Report();

		if ( dsp_is_sale() ) {
			add_menu_page(
				__('การจัดการงานขาย', 'dropship'),
				__('การจัดการงานขาย', 'dropship'),
				'edit_posts',
				'dropship-dashboard',
				[$Dropship_Admin_Dashboard, 'dsp_index'],
				'',
				6
			);

			add_submenu_page(
				'dropship-dashboard',
				__('หน้ารวมสินค้า', 'dropship'),
				__('หน้ารวมสินค้า', 'dropship'),
				'edit_posts',
				'dropship-manage-catalog',
				[$Dropship_Admin_ManageCatalog, 'dsp_index']
			);

			add_submenu_page(
				'dropship-dashboard',
				__('จัดการแบนเนอร์', 'dropship'),
				__('จัดการแบนเนอร์', 'dropship'),
				'edit_posts',
				'dropship-manage-banners',
				[$Dropship_Admin_Banners, 'dsp_index']
			);

			add_submenu_page(
				'-',
				__('รายละเอียดคำสั่งซื้อ', 'dropship'),
				__('รายละเอียดคำสั่งซื้อ', 'dropship'),
				'edit_posts',
				'dropship-order-detail',
				[$Dropship_Admin_Order_Detail, 'dsp_index']
			);

			return true;
		}

		add_menu_page(
			__('คำสั่งซื้อตามผู้ผลิต', 'dropship'),
			__('คำสั่งซื้อตามผู้ผลิต', 'dropship'),
			'edit_posts',
			'dropship-report',
			[$Dropship_Admin_Report, 'dsp_index'],
			'',
			6
		);

		add_submenu_page(
			'dropship-report',
			__('คำสั่งซื้อตามยี่ห้อ', 'dropship'),
			__('คำสั่งซื้อตามยี่ห้อ', 'dropship'),
			'edit_posts',
			'dropship-report-brand',
			[$Dropship_Admin_Report, 'dsp_index']
		);

		if ( dsp_is_headsale() ) {
			add_menu_page(
				__('การจัดการงานขาย', 'dropship'),
				__('การจัดการงานขาย', 'dropship'),
				'edit_posts',
				'dropship-dashboard',
				[$Dropship_Admin_Dashboard, 'dsp_index'],
				'',
				5
			);

			add_submenu_page(
				'dropship-dashboard',
				__('คอมมิชชั่น (%)', 'dropship'),
				__('คอมมิชชั่น (%)', 'dropship'),
				'edit_posts',
				'dropship-manage-commission',
				[$Dropship_Admin_Commission, 'dsp_index']
			);

			add_submenu_page(
				'dropship-dashboard',
				__('ตั้งค่า', 'dropship'),
				__('ตั้งค่า', 'dropship'),
				'edit_posts',
				'dropship-settings',
				[$Dropship_Admin_Settings, 'dsp_index']
			);

			add_submenu_page(
				'-',
				__('นำเข้าคอมมิชชั่น', 'dropship'),
				__('นำเข้าคอมมิชชั่น', 'dropship'),
				'edit_posts',
				'dropship-import-commission',
				[$Dropship_Admin_Commission, 'dsp_import_commission']
			);
		}

		add_submenu_page(
			'users.php',
			__('นำเข้าข้อมูลผู้ใช้', 'dropship'),
			__('นำเข้าข้อมูลผู้ใช้', 'dropship'),
			'edit_posts',
			'dropship-import-sale-user',
			[$Dropship_Admin_Settings, 'dsp_import_sale_user']
		);

		add_submenu_page(
			'-',
			__('นำเข้ารายงาน', 'dropship'),
			__('นำเข้ารายงาน', 'dropship'),
			'edit_posts',
			'dropship-import-report',
			[$Dropship_Admin_Report, 'dsp_import_report']
		);

		if ( dsp_is_operation() === false ) {
			global $submenu;
			$submenu['dropship-dashboard'][0][0] = esc_html__('แดชบอร์ด', 'dropship');
			$submenu['dropship-report'][0][0] = esc_html__('คำสั่งซื้อตามผู้ผลิต', 'dropship');
		}
	}

    function dsp_remove_product_data($tabs) {
        unset($tabs['linked_product']);
        unset($tabs['advanced']);

        return $tabs;
    }
}

return new Dropship_Admin_Menu();