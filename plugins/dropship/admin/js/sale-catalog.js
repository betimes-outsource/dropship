(function($) {
    'use strict';
    $(function() {
        $('body').on("click", "button.copy-product-to-catalog-url", function(e) {
            var id = $(this).data("id");
            dsp_copy_link_to_clipboard(id);
        });

        if ($("div#product-catalog-block").length > 0) {
            var page = 1;
            dsp_generate_products_catalog_sale(page);
        }

        $('body').on("click", "button.dsp-btn-product-to-catalog-page", function(e) {
            var page = $(this).data("page-id");
            dsp_generate_products_catalog_sale(page);
        });

        $('body').on("click", "input.dsp-checkbox-product-to-catalog", function(e) {
            var page = 1;
            dsp_generate_products_catalog_sale(page);
        });

        $('body').on("change", "select.dsp-select-product-to-catalog-self , select.dsp-select-product-to-catalog-order", function(e) {
            var page = 1;
            dsp_generate_products_catalog_sale(page);
        });

        function dsp_generate_products_catalog_sale(page) {
            var categories = $('input.dsp-checkbox-product-to-catalog:checked').map(function() {
                return this.value;
            }).get();
            var self = $("select.dsp-select-product-to-catalog-self").val();
            var order = $("select.dsp-select-product-to-catalog-order").val();

            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: dropshipjs.ajax_url,
                data: { action: 'dsp_get_product_catalog', page: page, categories: categories, self: self, order: order },
                beforeSend: function() {},
                success: function(res) {
                    if (res.status) {
                        $("#product-catalog-block").html(res.html);

                        var nameCatagories = '';
                        var nameCatagoriesTitle = 'ชื่อหมวดหมู่ :';
                        var showTitle = false;
                        if ($("input.dsp-checkbox-product-to-catalog:checked").is(':checked')) {
                            var nameCatagories = $("input.dsp-checkbox-product-to-catalog:checked").map(function() {
                                return $(this).data("name");
                            }).get().join(',');
                            var showTitle = true;
                        }

                        if (showTitle) {
                            var nameCatagoriesTitle = 'ชื่อหมวดหมู่ :';
                            $("#dsp-sale-cat-name").text(nameCatagoriesTitle + ' ' + nameCatagories);
                        } else {
                            $("#dsp-sale-cat-name").text(nameCatagories);
                        }
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alertify.error(textStatus);
                }
            });
        }

        function dsp_copy_link_to_clipboard(id) {
            /* Get the text field */
            var copyText = document.getElementById(id);
            console.log("dsp_copy_link_to_clipboard: ", copyText);
            console.log("copyText.value: ", copyText.value);
            /* Select the text field */
            copyText.select();
            copyText.setSelectionRange(0, 99999); /* For mobile devices */
            /* Copy the text inside the text field */
            // navigator.clipboard.writeText(copyText.value);
            document.execCommand("copy");
            /* Alert the copied text */
            alertify.success(dropshipjs.lang.copied);
        }

        $('body').on("click", "button#dsp-btn-catalog-url", function(e) {
            /* Get the text field */
            var copyText = document.getElementById("dsp-catalog-url");
            console.log("dsp-btn-catalog-url: ", copyText);
            /* Select the text field */
            copyText.select();
            copyText.setSelectionRange(0, 99999); /* For mobile devices */
            /* Copy the text inside the text field */
            document.execCommand("copy");
            /* Alert the copied text */
            alertify.success(dropshipjs.lang.copied);
        });

        // $('body').on("click", "button#dsp-btn-catalog-url", function(e) {
        //     /* Get the text field */
        //     var copyText = document.getElementById("dsp-catalog-url");
        //     /* Select the text field */
        //     copyText.select();
        //     copyText.setSelectionRange(0, 99999); /* For mobile devices */
        //     /* Copy the text inside the text field */
        //     navigator.clipboard.writeText(copyText.value);
        //     /* Alert the copied text */
        //     alertify.success("Copied");
        // });

        // deprecated
        $('body').on("click", ".generate-custom-url", function(e) {
            var div = $(this).closest("div");
            var url = $(div).find("input").val();
            if (url == '') {
                return;
            }
            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: dropshipjs.ajax_url,
                data: { url: $(div).find("input").val(), action: 'dsp_get_generate_custom_url' },
                beforeSend: function() {},
                success: function(res) {
                    if (res.status) {
                        $(div).find("input").val(res.data);
                    } else {
                        alertify.error(res.msg);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alertify.error(textStatus);
                }
            });
        });

        $('body').on("click", "button.add-product-to-catalog", function(e) {
            var in_catalog = $(this).hasClass("in-catalog");
            var btn = $(this);
            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: dropshipjs.ajax_url,
                data: { product_id: $(this).data('product-id'), action: 'dsp_add_product_catalog', remove: (in_catalog) ? "Y" : "N" },
                beforeSend: function() {},
                success: function(res) {
                    if (res.status) {
                        $(btn).text(res.txt);
                        if (in_catalog) {
                            $(btn).removeClass("in-catalog");
                            $(btn).removeClass("button-secondary");
                            $(btn).addClass("button-primary");
                        } else {
                            $(btn).addClass("in-catalog");
                            $(btn).removeClass("button-primary");
                            $(btn).addClass("button-secondary");
                        }
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alertify.error(textStatus);
                }
            });
        });
    });

})(jQuery);