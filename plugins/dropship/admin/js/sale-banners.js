(function ($) {
    'use strict';
    $(function () {
        $('body').on('click', '.dsp-sale-select-image', function (e) {
            e.preventDefault();
            var button = $(this),
                custom_uploader = wp.media({
                    title: dropshipjs.lang.insert_image,
                    library: {
                        // uploadedTo : wp.media.view.settings.post.id, // attach to the current post?
                        type: 'image'
                    },
                    button: {
                        text: dropshipjs.lang.use_this_image // button label text
                    },
                    multiple: false
                }).on('select', function () { // it also has "open" and "close" events
                    var attachment = custom_uploader.state().get('selection').first().toJSON();
                    var order = button.data("order");
                    button.closest("div").find("input[type='hidden']").val(attachment.id);
                    if (button.closest("div").find("img.dsp-banners").attr("src") !== undefined) {
                        button.closest("div").find("img.dsp-banners").attr("src", attachment.url);
                    } else {
                        // button.after('   <img src="' + attachment.url + '" class="dsp-banners">');
                        setTimeout(function () {
                            location.reload();
                        }, 2000)
                    }
                    dsp_update_banners_catalog_sale(order, attachment.id);
                }).open();
        });

        // on remove button click
        // $('body').on('click', '.misha-rmv', function(e) {
        //     e.preventDefault();
        //     var button = $(this);
        //     button.next().val(''); // emptying the hidden field
        //     button.hide().prev().html('Upload image');
        // });

        function dsp_update_banners_catalog_sale(order, image_id) {
            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: dropshipjs.ajax_url,
                data: { action: 'dsp_update_banners_catalog_sale', image_id: image_id, order: order },
                beforeSend: function () { },
                success: function (res) {
                    if (res.status) {
                        alertify.success(dropshipjs.lang.updated);
                    } else {
                        alertify.error(res.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alertify.error(textStatus);
                }
            });
        }
    });
})(jQuery);