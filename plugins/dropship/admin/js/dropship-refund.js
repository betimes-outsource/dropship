(function($) {
    'use strict';
    $(function() {
        $('body').on('click', 'div#woocommerce-order-actions button.save_order', function(e) {
            var old_status = $("#_dropship_order_status").val();
            var next_status = $("#order_status").val();

            if (next_status == "wc-refunded" && old_status != "wc-refunded") {
                e.preventDefault();
                var r = confirm(" ยืนยันการเปลี่ยนสถานะคำสั่งซื้อเป็น 'คืนเงิน' ! ");
                if (r == false) {
                    return false;
                }
            }

            $("#post").submit();
            return true;
        });
    });
})(jQuery);