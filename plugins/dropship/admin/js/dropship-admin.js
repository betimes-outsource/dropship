(function($) {
    'use strict';

    /**
     * All of the code for your admin-facing JavaScript source
     * should reside in this file.
     *
     * Note: It has been assumed you will write jQuery code here, so the
     * $ function reference has been prepared for usage within the scope
     * of this function.
     *
     * This enables you to define handlers, for when the DOM is ready:
     *
     * $(function() {
     *
     * });
     *
     * When the window is loaded:
     *
     * $( window ).load(function() {
     *
     * });
     *
     * ...and/or other possibilities.
     *
     * Ideally, it is not considered best practise to attach more than a
     * single DOM-ready or window-load handler for a particular page.
     * Although scripts in the WordPress core, Plugins and Themes may be
     * practising this, we should strive to set a better example in our own work.
     */

    $(function() {
        alertify.set('notifier', 'position', 'top-right');

        $.LoadingOverlaySetup({
            background: "rgba(0, 0, 0, 0.5)",
            image: dropshipjs.plugin_url + "public/image/loading.svg",
            imageAnimation: false,
            size: 15,
            imageColor: false
        });

        $(document).ajaxSend(function(event, jqxhr, settings) {
            // var URL = settings.data;
            // if (URL.includes("dsp")) {
            $.LoadingOverlay("show");
            // }
        });
        $(document).ajaxComplete(function(event, jqxhr, settings) {
            // var URL = settings.data;
            // if (URL.includes("dsp")) {
            $.LoadingOverlay("hide");
            // }
        });
        $(document).ajaxError(function(event, jqxhr, settings) {
            // var URL = settings.data;
            // if (URL.includes("dsp")) {
            $.LoadingOverlay("hide");
            // }
        });

        var from = $('input[name="daterange_start"]')
            .datepicker({
                dateFormat: "dd/mm/yy",
                gotoCurrent: true,
                // defaultDate: "+1w",
                // changeMonth: true,
                numberOfMonths: 1,
                showButtonPanel: true,
                beforeShow: function(input) {
                    setTimeout(
                        function() {
                            var buttonPane = $(input)
                                .datepicker("widget")
                                .find(".ui-datepicker-buttonpane");

                            $(
                                    "<button>", {
                                        text: "Clear",
                                        click: function() {
                                            // from._clearDate(input);
                                            // to._clearDate(input);
                                            $(from).val("");
                                            $(to).val("");
                                        },
                                    }
                                )
                                .appendTo(buttonPane)
                                .addClass(
                                    "ui-datepicker-clear ui-state-default ui-priority-primary ui-corner-all"
                                );
                        },
                        1
                    );
                },
                onSelect: function() {
                    to.val("");
                    to.datepicker("option", "minDate", dsp_getDate(this));
                },
            })
            .on(
                "change",
                function() {
                    to.datepicker("option", "minDate", dsp_getDate(this));
                }
            );
        var to = $('input[name="daterange_end"]')
            .datepicker({
                dateFormat: "dd/mm/yy",
                // defaultDate: "+1w",
                // changeMonth: true,
                numberOfMonths: 1,
                showButtonPanel: true,
                beforeShow: function(input) {
                    setTimeout(
                        function() {
                            var buttonPane = $(input)
                                .datepicker("widget")
                                .find(".ui-datepicker-buttonpane");
                            $(
                                    "<button>", {
                                        text: "Clear",
                                        click: function() {
                                            $(from).val("");
                                            $(to).val("");
                                        },
                                    }
                                )
                                .appendTo(buttonPane)
                                .addClass(
                                    "ui-datepicker-clear ui-state-default ui-priority-primary ui-corner-all"
                                );
                        },
                        1
                    );
                },
                onSelect: function() {
                    if (from.val() != undefined && from.val() != "" && to.val() != undefined && to.val() != "") {
                        // $(this).closest("form").submit();
                    }
                },
            })
            .on(
                "change",
                function() {
                    from.datepicker("option", "maxDate", dsp_getDate(this));
                }
            );

        $('body').on('click', '.dsp-setting-select-image', function(e) {
            e.preventDefault();
            var button = $(this),
                custom_uploader = wp.media({
                    title: dropshipjs.lang.insert_image,
                    library: {
                        // uploadedTo : wp.media.view.settings.post.id, // attach to the current post?
                        type: 'image'
                    },
                    button: {
                        text: dropshipjs.lang.use_this_image // button label text
                    },
                    multiple: false
                }).on('select', function() { // it also has "open" and "close" events
                    var attachment = custom_uploader.state().get('selection').first().toJSON();
                    button.closest("th").find("input[type='hidden']").val(attachment.id);
                    if (button.closest("th").find("input[type='text']").val() !== undefined) {
                        button.closest("th").find("input[type='text']").val(attachment.url);
                    } else {
                        button.after('<input type="text" class="_dsp-width-200" value="' + attachment.url + '" />');
                    }
                    // dsp_update_banners_catalog_sale(order, attachment.id);
                }).open();
        });
    });

    function dsp_getDate(element) {
        var dateFormat = "dd/mm/yy";
        var date;
        try {
            date = jQuery.datepicker.parseDate(dateFormat, element.value);
        } catch (error) {
            date = null;
        }
        return date;
    }

})(jQuery);