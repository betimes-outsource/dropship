(function($) {
    'use strict';
    $(function() {
        $('body').on('click', 'button#dsp-order-dashboard-export', function(e) {
            var _this = $(this);
            var form_id = _this.closest("form").attr("id");
            var form_action = _this.closest("form").data("action");
            var page = _this.closest("form").data("page");
            var formData = new FormData(document.getElementById(form_id));
            formData.append("action", form_action);
            formData.append("page", page);
            $.ajax({
                type: "POST",
                url: dropshipjs.ajax_url,
                contentType: false,
                processData: false,
                dataType: 'JSON',
                status: 200,
                data: formData,
                beforeSend: function() {},
                success: function(res) {
                    if (res.status) {
                        let link = document.createElement("a");
                        link.download = res.file_name;
                        link.href = res.link;
                        document.body.appendChild(link);
                        link.click();
                        document.body.removeChild(link);
                    } else {
                        alertify.error(res.message);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alertify.error(textStatus);
                }
            });
        });

        $('body').on('click', 'input#dsp-post-query-submit', function(e) {
            e.preventDefault();
            var _this = $(this);
            var form = _this.closest("form");
            var form_data = form.serializeArray();
            var exclude = ['_wpnonce', '_wp_http_referer', 'action', 'paged', 'action2'];
            form_data.forEach(element => {
                if (exclude.includes(element.name) == false) {
                    var newurl = window.location.href + '&' + element.name + '=' + element.value;
                    window.history.pushState({ path: newurl }, '', newurl);
                }
            });
            form.submit();
        });

        // == Start: button sidemenu show on mobile ==
        $('body').on('click', '#dsp-sidemenu-close', function(e) {
            $('.dsp-sidemenu').addClass('hide-menu');
        });
        $('body').on('click', '#dsp-sidemenu-open', function(e) {
            $('.dsp-sidemenu').removeClass('hide-menu');
        });
        // == End: button sidemenu show on mobile ==
    });
})(jQuery);