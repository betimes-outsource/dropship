<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.facebook.com/makkungs/
 * @since      1.0.0
 *
 * @package    Dropship
 * @subpackage Dropship/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Dropship
 * @subpackage Dropship/admin
 * @author     Mc <natdanai.opt@gmail.com>
 */
class Dropship_Admin
{

	public $isAdminRole = false;

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct($plugin_name, $version)
	{

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		$this->dsp_require_file();
	}

	private function dsp_require_file()
	{
		require plugin_dir_path(__FILE__) . 'ajax/dropship-rest-api.php';
		require plugin_dir_path(__FILE__) . 'page/dashboard.php';
		require plugin_dir_path(__FILE__) . 'page/manage-catalog.php';
		require plugin_dir_path(__FILE__) . 'page/commission.php';
		require plugin_dir_path(__FILE__) . 'page/affiliates.php';
		require plugin_dir_path(__FILE__) . 'page/order-detail.php';
		require plugin_dir_path(__FILE__) . 'page/banners.php';
		require plugin_dir_path(__FILE__) . 'page/report.php';
		require plugin_dir_path(__FILE__) . 'page/settings.php';
	}

	public function dsp_cronjob() {
		Dropship_Functions::get_instance()->dsp_update_order_status_pending_to_onhold();
	}

	public function enqueue_styles()
	{
		wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/dropship-admin.css', array(), $this->version, 'all');

		wp_enqueue_style($this->plugin_name . "-alertifycss", plugins_url($this->plugin_name) . '/vendor/alertifyjs/css/alertify.min.css', array(), $this->version, 'all');
		wp_enqueue_style($this->plugin_name . "-alertifycss-themes", plugins_url($this->plugin_name) . '/vendor/alertifyjs/css/themes/default.min.css', array(), $this->version, 'all');

		wp_enqueue_style($this->plugin_name . "-pure-style", plugin_dir_url(__FILE__) . 'css/pure-min.css', array(), $this->version, 'all');
		wp_enqueue_style($this->plugin_name . "-pure-grid", plugin_dir_url(__FILE__) . 'css/grids-responsive-min.css', array(), $this->version, 'all');
		wp_enqueue_style($this->plugin_name . "-jquery-modal", plugin_dir_url(__FILE__) . 'css/jquery.modal.min.css', array(), $this->version, 'all');

		wp_enqueue_style('e2b-admin-ui-css','http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css' , false , "1.9.0" , false);
	}

	public function enqueue_scripts_global() {
		wp_enqueue_script("dropship-refund", plugin_dir_url(__FILE__) . 'js/dropship-refund.js', array('jquery'), $this->version, false);
	}

	public function enqueue_scripts()
	{
		wp_enqueue_script($this->plugin_name , plugin_dir_url(__FILE__) . 'js/dropship-admin.js', array( 'jquery', 'jquery-ui-dialog', 'jquery-ui-position', 'jquery-ui-datepicker' ), $this->version, true);
		wp_enqueue_script($this->plugin_name . "-sale-banners", plugin_dir_url(__FILE__) . 'js/sale-banners.js', array('jquery'), $this->version, false);
		wp_enqueue_script($this->plugin_name . "-sale-catalog", plugin_dir_url(__FILE__) . 'js/sale-catalog.js', array('jquery'), $this->version, false);
		wp_enqueue_script($this->plugin_name . "-dashboard", plugin_dir_url(__FILE__) . 'js/dashboard.js', array('jquery'), $this->version, false);
		
		wp_localize_script($this->plugin_name, 'dropshipjs', 
			array( 
				'ajax_url' => admin_url( 'admin-ajax.php' ) , 
				'plugin_url' => DROPSHIP_PLUGIN_DIR_URL,
				'lang' => [
					'insert_image' => esc_html__('เพิ่มรูป', 'dropship'),
					'use_this_image' => esc_html__('ยืนยันการใช้รูป', 'dropship'),
					'updated' => esc_html__('อัปเดท!', 'dropship'),
					'copied' => esc_html__('คัดลอกแล้ว!',  'dropship'),
				]
			) 
		);

		wp_enqueue_script($this->plugin_name . "-alertifyjs", plugins_url( $this->plugin_name ) . '/vendor/alertifyjs/alertify.min.js', array('jquery'), $this->version, false);
		wp_enqueue_script($this->plugin_name . "-preload", plugins_url( $this->plugin_name ) . '/vendor/preload/loadingoverlay.min.js', array('jquery'), $this->version, false);
	}

	function dsp_remove_wc_order_statuses($statuses) {
		unset( $statuses['wc-pending'] );
		unset( $statuses['wc-failed'] );
		return $statuses;
	}

	function dsp_remove_wp_logo( $wp_admin_bar ) {
		$wp_admin_bar->remove_node( 'wp-logo' );
	}

	function dsp_add_my_own_logo( $wp_admin_bar ) {
		if ( dsp_is_sale() ) {
			$args = array(
				'id'    => 'my-logo',
				'meta' => array(
					'onclick' => '',
					'html' => '<a href="' . admin_url('admin.php?page=dropship-dashboard') . '"><img src="/wp-content/themes/dropship/assets/boss.png" style="width: 120px;margin-top: 10px;"></a>',
					'class' => '',
					'target' => '',
					'title' => ''
				)
			);
			$wp_admin_bar->add_node( $args );
		}
	}

	function dsp_register_taxonomy_dropship()
	{
		$terms = [ 'brand' => 'Brand' , 'supplier' => 'Supplier' ];
		foreach ($terms as $term => $text) {
			$name = $text;
			$names = $text . "s";
			$labels = array(
				'name'                       => esc_html__( $names , 'dropship' ),
				'singular_name'              => esc_html__( $name , 'dropship' ),
				'menu_name'                  => esc_html__( $name , 'dropship' ),
				'all_items'                  => esc_html__( "ทั้งหมด " . $names , 'dropship' ),
				'parent_item'                => esc_html__( "Parent " . $name , 'dropship' ),
				'parent_item_colon'          => esc_html__( "Parent " . $name . ":" , 'dropship' ),
				'new_item_name'              => esc_html__( "ใหม่" . $name . " ชื่อ" , 'dropship' ),
				'add_new_item'               => esc_html__( "เพิ่มใหม่ " . $name, 'dropship' ),
				'edit_item'                  => esc_html__( "แก้ไข " . $name, 'dropship' ),
				'update_item'                => esc_html__( "อัปเดท " . $name, 'dropship' ),
				'separate_items_with_commas' => esc_html__( "แยก" . $name . " ด้วย คอมมา", 'dropship' ),
				'search_items'               => esc_html__( "ค้นหา " . $names, 'dropship' ),
				'add_or_remove_items'        => esc_html__( "เพิ่มหรือลบ " . $names, 'dropship' ),
				'choose_from_most_used'      => esc_html__( "แสดงสินค้าที่เลือกขาย " . $names, 'dropship' ),
			);
			$args = array(
				'labels'                     => $labels,
				'hierarchical'               => true,
				'public'                     => true,
				'show_ui'                    => true,
				'show_admin_column'          => true,
				'show_in_nav_menus'          => true,
				'show_tagcloud'              => true,
				'capabilities' 				 => array( 
					'manage_terms' 	=> 'manage_' . $term,
					'edit_terms'	=> 'manage_' . $term,
					'delete_terms' 	=> 'manage_' . $term,
					'assign_terms' 	=> 'manage_' . $term,
				)
			);
			register_taxonomy($term, 'product', $args);
			register_taxonomy_for_object_type($term, 'product');
		}
	}

	function dsp_add_column_import_products_woocommerce( $options ) {
		$options['brand'] = esc_html__( 'ยี่ห้อ' , 'dropship' );
		$options['supplier'] = esc_html__( 'ผู้ผลิต' , 'dropship' );
		return $options;
	}

	function dsp_products_woocommerce_column_to_mapping_screen( $columns ) {
		$columns['brand'] = esc_html__( 'ยี่ห้อ' , 'dropship' );
		$columns['supplier'] = esc_html__( 'ผู้ผลิต' , 'dropship' );
		return $columns;
	}

	function dsp_products_woocommerce_process_import( $object, $data ) {
		if ( ! empty( $data['brand'] ) ) {
			$term_taxonomy_id = [];
			foreach ( explode("," , $data['brand'] ) as $brand) {
				$terms = term_exists( $brand , 'brand' );
				$term_taxonomy_id[] = (isset($terms['term_taxonomy_id'])) ? $terms['term_taxonomy_id'] : $terms;
			}
			if (!empty($term_taxonomy_id)) {
				wp_set_post_terms( $data['id'] , $term_taxonomy_id , 'brand' );
			}
		}

		if ( ! empty( $data['supplier'] ) ) {
			$term_taxonomy_id = [];
			foreach ( explode("," , $data['supplier'] ) as $supplier) {
				$terms = term_exists( $supplier , 'supplier' );
				$term_taxonomy_id[] = (isset($terms['term_taxonomy_id'])) ? $terms['term_taxonomy_id'] : $terms;
			}
			if (!empty($term_taxonomy_id)) {
				wp_set_post_terms( $data['id'] , $term_taxonomy_id , 'supplier' );
			}
		}
	
		return $object;
	}

	function dsp_usermeta_form_field_sale_code( $user ) {
		?>
		<table class="form-table">
			<tr>
				<th>
					<label for="_dsp_sale_id"><?php echo esc_html__( 'Sale ID' , 'dropship' ); ?></label>
				</th>
				<td>
					<input type="text" class="regular-text ltr" id="_dsp_sale_id" name="_dsp_sale_id" value="<?= esc_attr( get_user_meta( $user->ID, '_dsp_sale_id', true ) ) ?>" required>
				</td>
			</tr>
		</table>
		<?php
	}

	function dsp_usermeta_form_field_sale_code_update( $user_id )
	{
		// check that the current user have the capability to edit the $user_id
		if ( ! current_user_can( 'edit_user', $user_id ) ) {
			return false;
		}

		if ( Dropship_Model_Users::get_instance()->dsp_check_sale_id_duplicate( $_POST['_dsp_sale_id'] , $user_id ) ) {
			return false;
		}
	
		// create/update user meta for the $user_id
		return update_user_meta(
			$user_id,
			'_dsp_sale_id',
			$_POST['_dsp_sale_id']
		);
	}

	function dsp_remove_contact_info() {
		echo '<style> 
			@charset "UTF-8";
			@import url(https://fonts.googleapis.com/css?family=Sarabun);
			@import url(https://fonts.googleapis.com/css2?family=Kanit);
			tr.user-url-wrap , 
			tr.user-description-wrap , 
			tr.user-generate-reset-link-wrap , 
			tr.user-nickname-wrap , 
			table#fieldset-billing , 
			table#fieldset-shipping ,
			form#your-profile h2,
			table.form-table tr:nth-child(5)
			{ display: none; } </style>';
	}

	function dsp_my_login_logo () {
		echo '<style> 
			@charset "UTF-8";
			@import url(https://fonts.googleapis.com/css?family=Sarabun);
			@import url(https://fonts.googleapis.com/css2?family=Kanit);
			#login h1 a, .login h1 a {
			background-image: url(' . get_stylesheet_directory_uri() . '/assets/boss.png);
			height: 65px;
			width: 320px;
			background-size: 320px 65px;
			background-repeat: no-repeat;
			padding-bottom: 30px;
			// background-size: auto 100px;
			// background-repeat: no-repeat;
			// padding-bottom: 35px;
		}
		// .login form {
		// 	margin-top: 20px;
		// 	margin-left: 0;
		// 	font-weight: 400;
		// 	overflow: hidden;
		// 	border: 0px solid #c3c4c7 !important;
		// 	position: relative;
		// 	z-index: 999;
		// 	padding: 3em 2em !important;
		// 	background: rgba(255, 255, 255, 0.04) !important;
		// 	box-shadow: -1px 4px 28px 0px rgb(137 57 1 / 50%) !important;
		// }
		// body.login.js.login-action-login.wp-core-ui.locale-th {
		// 	background-image: linear-gradient(rgb(255 255 255), #f19c44) !important;
		// }
		input#user_login {
			font-family: "Kanit", sans-serif;
			font-size: 15px;
		}
		input#user_pass {
			font-family: "Kanit", sans-serif;
			font-size: 15px;
		}
		// span.dashicons.dashicons-admin-users {
		// 	font-size: 24px;
		// 	color: #999999;
		// 	display: flex;
		// 	align-items: center;
		// 	position: absolute;
		// 	border-radius: 25px;
		// 	bottom: 0;
		// 	left: 0;
		// 	width: 100%;
		// 	height: 100%;
		// 	padding-left: 35px;
		// 	padding-bottom: 52px;
		// 	pointer-events: none;
		// 	transition: all 0.4s;
		// }
		// span.dashicons.dashicons-lock {
		// 	font-size: 24px;
		// 	color: #999999;
		// 	display: flex;
		// 	align-items: center;
		// 	position: absolute;
		// 	border-radius: 25px;
		// 	bottom: 0;
		// 	left: 0;
		// 	width: 100%;
		// 	height: 100%;
		// 	padding-left: 10px;
		// 	padding-bottom: 1px;
		// 	pointer-events: none;
		// 	transition: all 0.4s;
		// }
		// label.ds-label-user {
		// 	display: none !important;
		// }
		// input#wp-submit {
		// 	background: #8b1a1c;
		// 	border-color: #8b1a1c;
		// }
		p, label {
			font-family: "Kanit", sans-serif;
		}
		</style>';
	}

	function dsp_add_meta_box_in_order() {
		$args = [
			[
				'name' => esc_html__("คอมมิชชั่น (%)" , 'dropship'),
				'id' => 'dsp_add_sale_box_in_order',
				'func' => 'dsp_sale_box_in_order_html'
			],
			[
				'name' => esc_html__("สลิปโอนเงิน" , 'dropship'),
				'id' => 'dsp_add_slip_box_in_order',
				'func' => 'dsp_slip_box_in_order_html'
			],
			[
				'name' => esc_html__("ข้อมูลการติดตาม" , 'dropship'),
				'id' => 'dsp_add_tracking_box_in_order',
				'func' => 'dsp_tracking_box_in_order_html'
			],
		];

        foreach ( $args as $array ) {
            add_meta_box(
                $array['id'], // Unique ID
                $array['name'], // Box title
                [ $this , $array['func'] ],  // Content callback, must be of type callable
                'shop_order',
				'side'
            );
        }
    }

    function dsp_sale_box_in_order_html( $post ) {
		$post_id = $post->ID;
		return dsp_show_metabox_commission_in_posttype( $post_id );
    }

	function dsp_slip_box_in_order_html( $post ) {
		$post_id = $post->ID;
		$order = wc_get_order( $post_id );
		$slip_path = get_post_meta($post_id , '_dsp_slip_path' , true);
		if ( empty($slip_path) ) {
			return;
		}

        ?>
		<img src="<?php echo esc_url($slip_path); ?>" style="max-width: 100%;">
		<input type="hidden" id="_dropship_order_status" value="wc-<?php echo esc_attr( $order->get_status() ); ?>">
        <?php
    }

	function dsp_tracking_box_in_order_html($post) {
		$post_id = $post->ID;
		$_dsp_courier_name = get_post_meta($post_id , '_dsp_courier_name' , true);
		$_dsp_tracking_number = get_post_meta($post_id , '_dsp_tracking_number' , true);
		// $_dsp_tracking_status = get_post_meta($post_id , '_dsp_tracking_status' , true);
		?>
			<style> p.wc-customer-user {display: none;} </style>
			<table style="width:100%;text-align: left">
				<tr>
					<th><?php echo esc_html__("ขนส่ง" , 'dropship'); ?>: </th>
					<td><input type="text" id="_dsp_courier_name" name="_dsp_courier_name" value="<?php echo esc_html($_dsp_courier_name); ?>"></td>
				</tr>
				<tr>
					<th><?php echo esc_html__("หมายเลขติดตามพัสดุ" , 'dropship'); ?>: </th>
					<td><input type="text" id="_dsp_tracking_number" name="_dsp_tracking_number" value="<?php echo esc_html($_dsp_tracking_number); ?>"></td>
				</tr>
				<!-- <tr>
					<th><?php // echo esc_html__("Shipping status" , 'dropship'); ?>: </th>
					<td><input type="text" id="_dsp_tracking_status" name="_dsp_tracking_status" value="<?php // echo $_dsp_tracking_status; ?>"></td>
				</tr> -->
			</table>
		<?php
	}

	function dsp_save_post_shop_order( $post_id, $post, $update  ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		if ( $update && isset($_POST['_dsp_courier_name']) && isset($_POST['_dsp_tracking_number']) ) {
			update_post_meta($post_id, '_dsp_courier_name' , sanitize_text_field($_POST['_dsp_courier_name']));
			update_post_meta($post_id, '_dsp_tracking_number' , sanitize_text_field($_POST['_dsp_tracking_number']));
			// update_post_meta($post_id, '_dsp_tracking_status' , sanitize_text_field($_POST['_dsp_tracking_status']));
		}

		return $post_id;
	}

	function dsp_woocommerce_order_actions($actions) {
		$actions['send_order_refunded_to_customer_manual'] = esc_html__("ส่งอีเมลรายการแจ้งเตือน คืนเงิน ให้ลูกค้า" , 'dropship');
		return $actions;
	}

	function dsp_woocommerce_order_action_send_order_refunded_to_customer_manual( $order ) {
		global $woocommerce;
		$mailer = $woocommerce->mailer();
        $email = $mailer->emails['WC_Email_Customer_Refunded_Order'];
		add_filter( 'woocommerce_email_enabled_customer_refunded_order', '__return_true' );
        $email->trigger( $order->get_id() );

		return $order;
	}

	/**
	 * Add file style for admin
	 */
	function enqueue_admin_style() {
		wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/dropship-admin-style.css', array(), $this->version, 'all');
	}

	/**
	 * Custom header in form
	 */
	function dsp_header_form() {
		echo  '<style> #login h1 a { display: none } </style>';
	}

	/**
	 * Custom header above username input field in login form
	 */
	function dsp_header_login_form() {
		echo '<img class="dsp-logo-login" src="' . get_stylesheet_directory_uri() . '/assets/boss.png" />';
	}

	
}
