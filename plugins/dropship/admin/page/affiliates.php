<?php

require_once('table/affiliates.php');
class Dropship_Admin_Affiliates
{
  public $affiliates_obj;

  public function __construct()
  {
    add_filter('set-screen-option', [__CLASS__, 'set_screen'], 10, 3);
    add_action('admin_menu', [$this, 'plugin_menu']);

    $this->affiliates_obj = new Dropship_Affiliates_Dashboard();
  }

  public static function set_screen($status, $option, $value)
  {
    return $value;
  }

  /**
   * Display a admin menu
   */
  function dsp_index()
  {
?>
    <div class="wrap">
      <div id="icon-users" class="icon32"></div>
      <h1 class="wp-heading-inline"><?php echo esc_html__('Affiliates', 'dropship'); ?></h1>
      <a href="<?php echo esc_url(admin_url('admin.php?page=dropship-edit-sale-commission')); ?>" class="page-title-action"><?php echo esc_html__('เพิ่มใหม่', 'dropship'); ?></a>
      <hr class="wp-header-end">
      <div id="post-body" class="metabox-holder columns-2">
        <div id="post-body-content">
          <div class="meta-box-sortables ui-sortable">
            <form method="post">
              <?php
              $this->affiliates_obj->prepare_items();
              $this->affiliates_obj->display(); ?>
            </form>
          </div>
        </div>
      </div>
    </div>
  <?php
  }

  function dsp_edit_sale_commission()
  {
    $sale_id = (isset($_GET['sale_id'])) ? $_GET['sale_id'] : 0;
    if ($sale_id != 0) {
      $user = get_user_by('id', $sale_id);
      if (empty($user)) {
        wp_redirect(admin_url());
        die;
      }
    }

    if ( $_SERVER['REQUEST_METHOD'] === 'POST' && wp_verify_nonce( $_POST['dropship_nonce'], 'dropship_nonce' ) && isset($_POST['submit']) && isset($_POST['sale_id']) && isset($_POST['dropship_commission_rate']) ) {
        $id = Dropship_Model_Commission::get_instance()->dsp_insert_sale_commission( $_POST['sale_id'] , $_POST['_dropship_commission_rate'] , $_POST );
        if ( $id !== false ) {
          update_user_meta( $_POST['sale_id'] , '_dropship_commission_rate' , $_POST['_dropship_commission_rate'] );
          wp_redirect(admin_url('admin.php?page=dropship-manage-affiliates'));
          die;
        } else {
          dsp_dd( $id , true );
        }
    }
  ?>

    <div class="wrap">
      <div id="icon-users" class="icon32"></div>
      <h1 class="wp-heading-inline"><?php echo esc_html__('Affiliate edit', 'dropship'); ?></h1>
      <hr class="wp-header-end">

      <div class="pure-g">
        <div class="pure-u-1-3"></div>
        <div class="pure-u-1-3">
          <form class="pure-form pure-form-aligned" method="POST" action="">
            <?php wp_nonce_field( 'dropship_nonce', 'dropship_nonce' ); ?>
            <fieldset>
              <div class="pure-control-group">
                <label for="aligned-foo"><?php echo esc_html__('ผู้ใช้', 'dropship'); ?></label>
                <?php wp_dropdown_users( ['name' => 'sale_id' , 'selected' => $sale_id] ); ?>
              </div>
              <div class="pure-control-group">
                <label for="_dropship_commission_rate"><?php echo esc_html__('อัตรา', 'dropship'); ?></label>
                <input type="text" pattern="[0-9.]+" name="_dropship_commission_rate" id="_dropship_commission_rate" value="0" min="0.1" placeholder="<?php echo esc_html__('rate %', 'dropship'); ?>" required />
                <span class="pure-form-message-inline">(%)</span>
              </div>
              <div class="pure-controls">
                <input type="submit" name="submit" value="submit" class="pure-button pure-button-primary">
              </div>
            </fieldset>
          </form>
        </div>
        <div class="pure-u-1-3"></div>
      </div>
    </div>
<?php
  }
}
