<?php

require_once('table/report.php');

class Dropship_Admin_Report {
    
    public $report_obj;

    public function __construct() {
		add_filter( 'set-screen-option', [ __CLASS__, 'set_screen' ], 10, 3 );
		add_action( 'admin_menu', [ $this, 'plugin_menu' ] );
    }

	public static function set_screen( $status, $option, $value ) {
		return $value;
	}

    public function dsp_index() {
        $this->report_obj = new Dropship_Report();
		?>
            <div class="wrap ds-dasbord">
                <div id="icon-users" class="icon32"></div>
                <h1 class="wp-heading-inline"><?php echo esc_html__( 'คำสั่งซื้อตามผู้ผลิต', 'dropship' ); ?></h1>
                <?php if ( dsp_is_headsale() || dsp_is_operation() ) : ?>
                    <a href="<?php echo esc_url(admin_url("admin.php?page=dropship-import-report")); ?>" class="page-title-action"><?php echo esc_html__('นำเข้ารายงาน', 'dropship'); ?></a>
                <?php endif; ?>
                <hr class="wp-header-end">
				<div id="post-body" class="metabox-holder columns-2">
					<div id="post-body-content">
						<form method="post" id="dsp-order-list-dashboard" data-action="dsp_sale_report_dashboard_export" data-page="<?php echo esc_attr($_GET['page']); ?>">
						<?php
							$this->report_obj->prepare_items();
							$this->report_obj->display(); ?>
						</form>
					</div>
				</div>
            </div>
		<?php
    }

	function dsp_import_report() {
	  if ($_SERVER['REQUEST_METHOD'] === 'POST' && wp_verify_nonce($_POST['dropship_nonce'], 'dropship_nonce') && isset($_POST['submit']) && !empty($_FILES['file_upload'])) {
		$lines = dsp_import_excel_file_to_array( $_FILES );
		if ( $lines ) {
			foreach ($lines as $i => $values) {
				$order_id = trim($values[0]);
				$courier_name = (isset($values[12])) ? trim($values[12]) : "";
				$tracking_number = (isset($values[13])) ? trim($values[13]) : "";
				$tracking_status = (isset($values[14])) ? trim($values[14]) : "";
	
				$order = wc_get_order($order_id);    
				if (!empty($order) && $order_id == $order->get_id()) {
					update_post_meta( $order_id , '_dsp_courier_name' , $courier_name );
					update_post_meta( $order_id , '_dsp_tracking_number' , $tracking_number );
					if ( !empty( $tracking_status ) ) {
						update_post_meta( $order_id , '_dsp_tracking_status' , $tracking_status );
						$order->update_status('completed', 'complete by import');
					}
				}
			}

			?>
			<div class="notice notice-success is-dismissible">
				<p><?php echo esc_html__( 'ดำเนินการสำเร็จ', 'dropship' ); ?></p>
			</div>
			<?php
		} else {
			?>
			<div class="notice notice-danger is-dismissible">
				<p><?php echo esc_html__( 'Error!', 'dropship' ); ?></p>
			</div>
			<?php
		}
	  }
  
	?>
	  <div class="wrap">
		<div id="icon-users" class="icon32"></div>
		<h1 class="wp-heading-inline"><?php echo esc_html__('นำเข้ารายงาน', 'dropship'); ?></h1>
		<hr class="wp-header-end">
  
		<div class="pure-g">
		  <div class="pure-u-1-3"></div>
		  <div class="pure-u-1-3">
			<form class="pure-form pure-form-aligned" method="POST" enctype="multipart/form-data">
			  <?php wp_nonce_field('dropship_nonce', 'dropship_nonce'); ?>
			  <fieldset>
				<div class="pure-control-group">
				  <label for="aligned-foo"><?php echo esc_html__('เลือกไฟล์ที่ต้องอัปโหลด', 'dropship'); ?></label>
				  <input type="file" id="file_upload" name="file_upload" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required />
				</div>
				<div class="pure-controls">
				  <input type="submit" name="submit" value="submit" class="pure-button pure-button-primary">
				</div>
			  </fieldset>
			</form>
		  </div>
		  <div class="pure-u-1-3"></div>
		</div>
	  </div>
  <?php
	}
}