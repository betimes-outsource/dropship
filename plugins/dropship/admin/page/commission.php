<?php

require_once('table/commission.php');
class Dropship_Admin_Commission
{

  public $commission_obj;

  public function __construct()
  {
    add_filter('set-screen-option', [__CLASS__, 'set_screen'], 10, 3);
    add_action('admin_menu', [$this, 'plugin_menu']);

    $this->commission_obj = new Dropship_Commission_Dashboard();
  }

  public static function set_screen($status, $option, $value)
  {
    return $value;
  }
  /**
   * Display a admin menu
   */
  function dsp_index()
  {
?>
    <div class="wrap">
      <div id="icon-users" class="icon32"></div>
      <h1 class="wp-heading-inline"><?php echo esc_html__('คอมมิชชั่น (%)', 'dropship'); ?></h1>
      <a href="<?php echo esc_url(admin_url('admin.php?page=dropship-import-commission')); ?>" class="page-title-action"><?php echo esc_html__('นำเข้าคอมมิชชั่น', 'dropship'); ?></a>
      <hr class="wp-header-end">
      <div id="post-body" class="metabox-holder columns-2">
        <div id="post-body-content">
          <div class="meta-box-sortables ui-sortable">
            <form method="post">
              <?php
              $this->commission_obj->prepare_items();
              $this->commission_obj->display(); ?>
            </form>
          </div>
        </div>
      </div>
    </div>
  <?php
  }

  function dsp_import_commission()
  {
    if ($_SERVER['REQUEST_METHOD'] === 'POST' && wp_verify_nonce($_POST['dropship_nonce'], 'dropship_nonce') && isset($_POST['submit']) && !empty($_FILES['file_upload'])) {
      $lines = dsp_import_excel_file_to_array( $_FILES );
      $completed = 0;

      if ( $lines ) {
        foreach ($lines as $i => $values) {
          $sale_id = trim($values[0]);
          $product_sku = trim($values[1]);
          $commission = trim($values[2]);
  
          $user_id = Dropship_Model_Users::get_instance()->dsp_get_user_id_by_sale_id($sale_id);
          if (empty($user_id) || $user_id === 0) {
            $skip['sale'][] = $sale_id;
            continue;
          }
  
          $product_id = wc_get_product_id_by_sku($product_sku);
          if (empty($product_id) || $product_id === 0) {
            $skip['product'][] = $product_sku;
            continue;
          }
  
          $insert_id = Dropship_Model_Commission::get_instance()->dsp_insert_sale_product_id_commission($user_id, $product_id, $commission);;
          if ($insert_id > 0) {
            $completed++;
          }
        }

        ?>
        <div class="notice notice-success is-dismissible">
            <p><?php echo esc_html__( 'ดำเนินการสำเร็จ', 'dropship' ); ?></p>
        </div>
        <?php
      } else {
        ?>
        <div class="notice notice-danger is-dismissible">
            <p><?php echo esc_html__( 'Error!', 'dropship' ); ?></p>
        </div>
        <?php
      }
    }

  ?>
    <div class="wrap">
      <div id="icon-users" class="icon32"></div>
      <h1 class="wp-heading-inline"><?php echo esc_html__('นำเข้าคอมมิชชั่น', 'dropship'); ?></h1>
      <hr class="wp-header-end">

      <div class="pure-g">
        <div class="pure-u-1-3"></div>
        <div class="pure-u-1-3">
          <form class="pure-form pure-form-aligned" method="POST" enctype="multipart/form-data">
            <?php wp_nonce_field('dropship_nonce', 'dropship_nonce'); ?>
            <fieldset>
              <div class="pure-control-group">
                <label for="aligned-foo"><?php echo esc_html__('เลือกไฟล์ที่ต้องอัปโหลด', 'dropship'); ?></label>
                <input type="file" id="file_upload" name="file_upload" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required />
              </div>
              <div class="pure-controls">
                <input type="submit" name="submit" value="submit" class="pure-button pure-button-primary">
              </div>
            </fieldset>
          </form>
        </div>
        <div class="pure-u-1-3"></div>
      </div>
    </div>
<?php
  }
}
