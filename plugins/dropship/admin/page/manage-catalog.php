<?php

class Dropship_Admin_ManageCatalog
{
  public function __construct()
  {
  }

  /**
   * Display a admin menu
   */
  function dsp_index()
  {
    $default_catalog_page = get_option('_dropship_default_catalog_page');
    if ( $default_catalog_page ) {
      $page_id = $default_catalog_page;
    } else {
      $page_id = wc_get_page_id('shop');
    }
    $catalogPage = dsp_concat_ref_id(get_permalink($page_id));
    $isIP = @($_SERVER['SERVER_ADDR'] === trim($_SERVER['HTTP_HOST'], '[]'));
?>
    <style scoped="">
      .button-secondary {
        background: rgb(66, 184, 221);
      }
      /* start edit style backend nong */
      .ds-cat-url{
        width: 75%!important;
        border: none!important;
        background: #f1f1f1!important;
      }
      .ds-cat-copy {
        width: 23%!important;
        height: 40px;
        padding: 6px 5px 0px 5px !important;
        font-size: 10px !important;
        background-color: #ffab00!important;
        color: #333333!important;
        border: none!important;
      }
      .ds-cat-copy > img{
        width: 16px;
        height: auto;
      }
      @media (min-width: 320px) and (max-width: 480px) {
        .ds-cat-url{
          width: 80%;
        }
        /* .pure-g {
          flex-flow: column;
        }
        .pure-u-1-3, .pure-u-2-3 {
            width: 100%;
        } */
        .wp-core-ui select {
          max-width: 100%;
        }
      }
      /* end edit style backend nong */
    </style>

    <div class="wrap">
      <div class="pure-g">
        <div class="pure-u-sm-1-2 pure-u-lg-1-5 ds-catalog-left">
          <div class="ds-header-cat">
            <div id="icon-users" class="icon32"></div>
            <h1 class="wp-heading-inline ds-menu-sale-cat"><?php echo esc_html__('Sale Catalog', 'dropship'); ?></h1>
            <div class="clearfix"></div>
          </div>
          <div class="ds-menu-cat">
            <?php if ( $isIP ) : ?>
            <div class="pure-g">
              <div class="pure-u-1-1">
                <h3><?php echo esc_html__('Sale Catalog url', 'dropship'); ?> <a class="ds-link-cat-page" href="<?php echo esc_attr($catalogPage); ?>"><?php echo esc_html__('ดูร้านค้า', 'dropship'); ?></a></h3>
                <input type="text" class="pure-u-1 ds-cat-url" id="dsp-catalog-url" value="<?php echo esc_attr($catalogPage); ?>" readonly>
              </div>
            </div>
            <?php else : ?>
            <div class="pure-g ds-haed-catalog">
              <div class="pure-u-1-1">
                <h3><?php echo esc_html__('Sale Catalog url', 'dropship'); ?> <a class="ds-link-cat-page" target="_blank" href="<?php echo esc_attr($catalogPage); ?>"><?php echo esc_html__('ดูร้านค้า', 'dropship'); ?></a></h3>
                <input type="text" class="pure-u-1 ds-cat-url" id="dsp-catalog-url" value="<?php echo esc_attr($catalogPage); ?>" readonly>
                <button class="button-secondary pure-button ds-cat-copy" id="dsp-btn-catalog-url">
                  <?php //echo esc_html__('Copy', 'dropship'); ?>
                  <img src="/wp-content/themes/dropship/assets/copy-w.png">
                </button>
              </div>
            </div>
            <?php endif; ?>
              <br>
              <hr>
              <h3><?php echo esc_html__('กรองสินค้า', 'dropship'); ?></h3>
              <select class="select2 dsp-select-product-to-catalog-self ds-fillter-product">
                <option value="A"><?php echo esc_html__('แสดงสินค้าที่เลือกขาย', 'dropship'); ?></option>
                <option value="B" selected="selected"><?php echo esc_html__('แสดงสินค้าทั้งหมด', 'dropship'); ?></option>
                <option value="C"><?php echo esc_html__("แสดงสินค้าที่ไม่ได้เลือกขาย", 'dropship'); ?></option>
              </select>

              <!-- <br>
              <hr>
              <h3><?php echo esc_html__('เรียงลำดับจากมากไปน้อย', 'dropship'); ?></h3>
              <select class="select2 dsp-select-product-to-catalog-order ds-product-by">
                <option value="1"><?php echo esc_html__('ID', 'dropship'); ?></option>
                <option value="2"><?php echo esc_html__('คอมมิชชั่น (%)', 'dropship'); ?></option>
              </select> -->

              <br>
              <br>
              <hr>
              <h3><?php echo esc_html__('เลือกหมวดหมู่สินค้า', 'dropship'); ?></h3>
              <ul>
              <?php
              $taxonomy     = 'product_cat';
              $orderby      = 'name';  
              $show_count   = 0;      // 1 for yes, 0 for no
              $pad_counts   = 0;      // 1 for yes, 0 for no
              $hierarchical = 1;      // 1 for yes, 0 for no  
              $title        = '';  
              $empty        = true;
            
              $args = array(
                'taxonomy'     => $taxonomy,
                'orderby'      => $orderby,
                'show_count'   => $show_count,
                'pad_counts'   => $pad_counts,
                'hierarchical' => $hierarchical,
                'title_li'     => $title,
                'hide_empty'   => $empty
              );
              $all_categories = get_categories( $args );
              foreach ($all_categories as $cat) {
                if ($cat->category_parent == 0) {
                  $category_id = $cat->term_id;
                  $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
                  $thumbnail_img = wp_get_attachment_url( $thumbnail_id );
              ?> 
                <label class="ds-cat-icon">
                  <input type="checkbox" class="dsp-checkbox-product-to-catalog" name="dsp-checkbox-product-to-catalog[]" data-name="<?php echo esc_html($cat->name); ?>" value="<?php echo esc_attr($category_id); ?>"/><img src="<?php echo $thumbnail_img; ?>"> <span><?php echo esc_html($cat->name); ?></span>
                </label>
                <?php
                
                // ส่วนของ sub category
                $args2 = array(
                  'taxonomy'     => $taxonomy,
                  'child_of'     => 0,
                  'parent'       => $category_id,
                  'orderby'      => $orderby,
                  'show_count'   => $show_count,
                  'pad_counts'   => $pad_counts,
                  'hierarchical' => $hierarchical,
                  'title_li'     => $title,
                  'hide_empty'   => $empty
                );
                $sub_cats = get_categories( $args2 );
                if ($sub_cats) {
                  foreach($sub_cats as $sub_category) {
                    $_category_id = $sub_category->term_id;
              ?>
                <!-- <label style="display: block;margin-left: 20px;padding: 3px 0px;">
                <input type="checkbox" class="dsp-checkbox-product-to-catalog" name="dsp-checkbox-product-to-catalog[]" data-name="<?php //echo esc_html($sub_category->name); ?>" value="<?php //echo esc_attr($_category_id); ?>"/> - <?php //echo esc_html($sub_category->name); ?>
                </label> -->
              <?php
                    }
                  }
                }
              }
              ?>
              </ul>
          </div>
        </div>
        <div class="pure-u-sm-1-2 pure-u-lg-4-5 ds-catalog-right" id="product-catalog-block"></div>
      </div>
    </div>
<?php
  }
}
