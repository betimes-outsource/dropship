<?php

class Dropship_Admin_Banners
{

    public function __construct()
    {
    }

    /**
     * Display a admin menu
     */
    function dsp_index()
    {
        $limit = 1;
        $users_banners = get_user_meta(get_current_user_id(), '_dsp_sale_banners', true);
        if (empty($users_banners)) {
            $users_banners = [
                0, 0, 0
            ];
        }
?>
        <style>
            .dsp-banners {
                display: block;
                margin-top: 5px;
                max-width: 300px;
                max-height: 300px;
            }

            .pure-table-horizontal td,
            .pure-table-horizontal th {
                color: #000 !important;
            }

            .ds-button-a {
                border-radius: 3px !important;
            }
        </style>
        <div class="wrap" id="wrap-banners">
            <!-- <div id="icon-users" class="icon32"></div>
                <h1 class="wp-heading-inline"><?php //echo esc_html__('แบนเนอร์', 'dropship'); 
                                                ?></h1>
                <hr /> -->

            <div id="post-body" class="metabox-holder columns-5">
                <div id="post-body-content" class="pure-g ds-wrap-content">
                    <div class="pure-u-5-5 banners">
                        <div class="ds-header-cat">
                            <div id="icon-users" class="icon32"></div>
                            <h1 class="wp-heading-inline ds-menu-sale-cat"><?php echo esc_html__('แบนเนอร์', 'dropship'); ?></h1>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <?php for ($i = 0; $i < $limit; $i++) : ?>
                        <?php $image_id = (isset($users_banners[$i]) && $users_banners[$i] > 0) ? $users_banners[$i] : 0; ?>

                        <div class="pure-u-5-5">
                            <div class="ds-banner-button">
                                <input type="hidden" class="image-id" value="<?php echo esc_attr($image_id)  ?>">
                                <button type="button" data-order="<?php echo esc_attr($i); ?>" class="pure-button pure-button-primary dsp-sale-select-image ds-button-a"><?php echo esc_html__('สร้างแบนเนอร์', 'dropship'); ?></button>
                            </div>
                        </div>
                        <div class="pure-u-5-5">
                            <div class="ds-banner-table">
                                <table class="pure-table pure-table-horizontal">
                                    <thead>
                                        <tr>
                                            <th>รูปภาพ</th>
                                            <th>ชื่อ</th>
                                            <th>วันที่ทำการสร้าง</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <?php
                                            if ($image_id) {
                                                $image = wp_get_attachment_image_src($image_id, 'full'); ?>
                                                <td> <?php echo '<img class="dsp-banners" src="' . esc_html($image[0]) . '" />'; ?>
                                                </td>
                                                <?php $attachment_data = get_post($image_id); ?>

                                                <td><?php print_r($attachment_data->post_title) ?></td>
                                                <td><?php print_r($attachment_data->post_date) ?></td>
                                            <?php  } ?>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <?php endfor; ?>


                </div>
            </div>


            <?php for ($i = 0; $i < $limit; $i++) : ?>
                <!-- <div>
                    <?php $image_id = (isset($users_banners[$i]) && $users_banners[$i] > 0) ? $users_banners[$i] : 0; ?>
                    <input type="hidden" class="image-id" value="<?php echo esc_attr($image_id)  ?>">
                    <button type="button" data-order="<?php echo esc_attr($i); ?>" class="pure-button pure-button-primary dsp-sale-select-image"><?php echo esc_html__('เลือกรูป', 'dropship'); ?></button>
                    <?php
                    if ($image_id) {
                        $image = wp_get_attachment_image_src($image_id, 'full');
                        echo '<img class="dsp-banners" src="' . esc_html($image[0]) . '" />';
                    }
                    ?>
                </div>
                <br /> -->
            <?php endfor; ?>


        </div>
<?php
    }
}
