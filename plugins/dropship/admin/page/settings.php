<?php

class Dropship_Admin_Settings {
    
    public function __construct() {
    }

    /**
     * Display a admin menu
     */
    function dsp_index() {

        if ( $_SERVER['REQUEST_METHOD'] === 'POST' && wp_verify_nonce( $_POST['dropship_nonce'], 'dropship_nonce' ) && isset($_POST['submit']) ) {
            foreach ($_POST as $key => $value) {
                update_option( $key , $value );
            }
        }

		?>
            <div class="wrap">
                <div id="icon-users" class="icon32"></div>
                <h1 class="wp-heading-inline"><?php echo esc_html__( 'ตั้งค่า', 'dropship' ); ?></h1>
                <div class="clearfix"></div>

                <style>
                    ._dsp-width-200 {
                        width: 200px;
                    }
                </style>
                <div class="wrap">
                    <form class="pure-form pure-form-stacked" method="POST" action="">
                        <table style="width:100%;text-align: left;">
                            <tr>
                                <th style="width: 15%;">
                                    <label for="_dropship_commission_rate"><?php echo esc_html__( 'คอมมิชชั่นกลาง', 'dropship' ); ?></label></th>
                                <th>
                                    <input type="text" class="_dsp-width-200" pattern="[0-9.]+" name="_dropship_commission_rate" id="_dropship_commission_rate" value="<?php echo esc_attr(get_option('_dropship_commission_rate' , 0)); ?>" min="0.1" placeholder="<?php echo esc_html__('rate %', 'dropship'); ?>" required />
                                </th> 
                            </tr>
                            <tr>
                                <th style="width: 15%;">
                                    <label for="_dropship_default_shipping_cost"><?php echo esc_html__( 'ค่าเริ่มต้น ค่าจัดส่ง (ต่อชิ้น)', 'dropship' ); ?></label>
                                </th>
                                <th>
                                    <input type="text" class="_dsp-width-200" pattern="[0-9.]+" name="_dropship_default_shipping_cost" id="_dropship_default_shipping_cost" value="<?php echo esc_attr(get_option('_dropship_default_shipping_cost' , 0)); ?>" min="0" placeholder="<?php echo esc_html__('บาท', 'dropship'); ?>" required />
                                </th> 
                            </tr>
                            <!-- <tr>
                                <th style="width: 15%;">
                                    <label for="_dropship_default_shipping_cost_next"><?php echo esc_html__( 'ค่าเริ่มต้น ค่าจัดส่งชิ้นถัดไป (ต่อชิ้น)', 'dropship' ); ?></label>
                                </th>
                                <th>
                                    <input type="text" class="_dsp-width-200" pattern="[0-9.]+" name="_dropship_default_shipping_cost_next" id="_dropship_default_shipping_cost_next" value="<?php echo esc_attr(get_option('_dropship_default_shipping_cost_next' , 0)); ?>" min="0" placeholder="<?php echo esc_html__('บาท', 'dropship'); ?>" required />
                                </th> 
                            </tr> -->
                            <tr>
                                <th style="width: 15%;">
                                    <label for="_dropship_default_catalog_page"><?php echo esc_html__( 'หน้าหลักแคตตาลอก', 'dropship' ); ?></label>
                                </th>
                                <th>
                                    <?php
                                        $dropdown_args = array(
                                            'post_type' => 'page',
                                            'name' => '_dropship_default_catalog_page',
                                            'selected' => get_option('_dropship_default_catalog_page'),
                                            'class' => '_dsp-width-200'
                                        );
                                        wp_dropdown_pages( $dropdown_args );
                                    ?>
                                </th> 
                            </tr>
                            <tr>
                                <th style="width: 15%;">
                                   <label for="_dropship_default_complete_page"><?php echo esc_html__( 'ค่าเริ่มต้น หน้ารายการสำเร็จ', 'dropship' ); ?></label>
                                </th>
                                <th>
                                    <?php
                                        $dropdown_args = array(
                                            'post_type' => 'page',
                                            'name' => '_dropship_default_complete_page',
                                            'selected' => get_option('_dropship_default_complete_page'),
                                            'class' => '_dsp-width-200'
                                        );
                                        wp_dropdown_pages( $dropdown_args );
                                    ?>
                                </th> 
                            </tr>
                            <tr>
                                <th style="width: 15%;">
                                   <label for="_dropship_default_tracking_page"><?php echo esc_html__( 'ค่าเริ่มต้น หน้าติดตาม พัสดุ', 'dropship' ); ?></label>
                                </th>
                                <th>
                                    <?php
                                        $dropdown_args = array(
                                            'post_type' => 'page',
                                            'name' => '_dropship_default_tracking_page',
                                            'selected' => get_option('_dropship_default_tracking_page'),
                                            'class' => '_dsp-width-200'
                                        );
                                        wp_dropdown_pages( $dropdown_args );
                                    ?>
                                </th> 
                            </tr>
                            <tr>
                                <th style="width: 15%;">
                                    <label for="_dropship_default_refund_page"><?php echo esc_html__( 'หน้าขั้นตอนการคืนสินค้า / คืนเงิน', 'dropship' ); ?></label>
                                </th>
                                <th>
                                    <?php
                                        $dropdown_args = array(
                                            'post_type' => 'page',
                                            'name' => '_dropship_default_refund_page',
                                            'selected' => get_option('_dropship_default_refund_page'),
                                            'class' => '_dsp-width-200'
                                        );
                                        wp_dropdown_pages( $dropdown_args );
                                    ?>
                                </th> 
                            </tr>
                            <tr>
                                <th style="width: 15%;">
                                   <label for="_dropship_qrcode_promtpay"><?php echo esc_html__( 'Qrcode promtpay', 'dropship' ); ?></label>
                                </th>
                                <th>
                                    <?php $image_id = get_option('_dropship_qrcode_promtpay' , 0); ?>
                                    <input type="hidden" class="image-id" name="_dropship_qrcode_promtpay" id="_dropship_qrcode_promtpay" value="<?php echo esc_attr($image_id); ?>">
                                    <button type="button" class="pure-button pure-button-primary dsp-setting-select-image"><?php echo esc_html__('เลือกรูป', 'dropship'); ?></button>
                                    <?php
                                        if ( $image_id ) {
                                            $image = wp_get_attachment_image_src( $image_id , 'full' );
                                            echo '<input type="text" class="_dsp-width-200" value="' . esc_html($image[0]) . '" />';
                                        }
                                    ?>
                                </th> 
                            </tr>
                            <tr>
                                <th style="width: 15%;">
                                   <label for="_dropship_qrcode_promtpay_step"><?php echo esc_html__( 'Qrcode step', 'dropship' ); ?></label>
                                </th>
                                <th>
                                    <?php $_dropship_qrcode_promtpay_step = get_option('_dropship_qrcode_promtpay_step' , ''); ?>
                                    <textarea style="width: 50%" rows="7" name="_dropship_qrcode_promtpay_step" id="_dropship_qrcode_promtpay_step" ><?php echo esc_attr($_dropship_qrcode_promtpay_step); ?></textarea>
                                </th> 
                            </tr>
                        </table>
                        <?php
                            wp_nonce_field( 'dropship_nonce', 'dropship_nonce' );
                            submit_button();
                        ?>
                    </form>
                </div><!-- .wrap -->
            </div>
		<?php
    }

    function dsp_import_sale_user()
    {
      if ($_SERVER['REQUEST_METHOD'] === 'POST' && wp_verify_nonce($_POST['dropship_nonce'], 'dropship_nonce') && isset($_POST['submit']) && !empty($_FILES['file_upload'])) {
        $lines = dsp_import_excel_file_to_array( $_FILES );

        if ( $lines ) {
            foreach ($lines as $i => $values) {
                $username = trim( $values[0] );
                $firstname = trim( $values[1] );
                $lastname = trim( $values[2] );
                $nickname = trim( $values[3] );
                $email = trim( $values[4] );
                $profile_image = trim( $values[5] );
                $password = trim( $values[6] );
                $saleID = trim( $values[7] );
                $role = str_replace(" " , "" , trim( $values[8] ));

                $exists = email_exists( $email );
                $result = false;
                if ( $exists ) {
                    $result = wp_update_user([
                        'ID' => $exists, // this is the ID of the user you want to update.
                        'first_name' => $firstname,
                        'last_name' => $lastname,
                    ]);
                    update_user_meta( $exists , '_dsp_sale_id' , $saleID);
                    $user = get_user_by('id', $exists);
                    $user->remove_role('subscriber');
                    $user->remove_role(DROPSHIP_ROLE_SALE);
                    $user->remove_role(DROPSHIP_ROLE_HEADSALE);
                    $user->remove_role(DROPSHIP_ROLE_OPERATION);

                    if ( strtoupper($role) == "SALE" ) {
                        $user->set_role(DROPSHIP_ROLE_SALE);
                    } elseif ( strtoupper($role) == "HEADSALE" ) {
                        $user->set_role(DROPSHIP_ROLE_HEADSALE);
                    } elseif ( strtoupper($role) == "OPERATION" ) {
                        $user->set_role(DROPSHIP_ROLE_OPERATION);
                    } else {
                        $user->set_role('subscriber');
                    }
                } else {
                    // Create the new user
                    $user_id = wp_create_user($username, $password, $email);
                    if ( $user_id ) {
                        $result = wp_update_user([
                            'ID' => $user_id, // this is the ID of the user you want to update.
                            'first_name' => $firstname,
                            'last_name' => $lastname,
                        ]);
                        // Get current user object
                        $user = get_user_by('id', $user_id);
                        // Remove role
                        $user->remove_role('subscriber');
                        // Add role
                        if ( strtoupper($role) == "SALE" ) {
                            $user->set_role(DROPSHIP_ROLE_SALE);
                        } elseif ( strtoupper($role) == "HEADSALE" ) {
                            $user->set_role(DROPSHIP_ROLE_HEADSALE);
                        } elseif ( strtoupper($role) == "OPERATION" ) {
                            $user->set_role(DROPSHIP_ROLE_OPERATION);
                        } else {
                            $user->set_role('subscriber');
                        }

                        update_user_meta( $user_id , '_dsp_sale_id' , $saleID);
                    }
                }
              }
  
              ?>
              <div class="notice notice-success is-dismissible">
                  <p><?php echo esc_html__( 'ดำเนินการสำเร็จ', 'dropship' ); ?></p>
              </div>
              <?php
        } else {
            ?>
            <div class="notice notice-danger is-dismissible">
                <p><?php echo esc_html__( 'Error!', 'dropship' ); ?></p>
            </div>
            <?php
        }
      }
  
    ?>
      <div class="wrap">
        <div id="icon-users" class="icon32"></div>
        <h1 class="wp-heading-inline"><?php echo esc_html__('นำเข้าข้อมูลผู้ใช้', 'dropship'); ?></h1>
        <hr class="wp-header-end">
  
        <div class="pure-g">
          <div class="pure-u-1-3"></div>
          <div class="pure-u-1-3">
            <form class="pure-form pure-form-aligned" method="POST" enctype="multipart/form-data">
              <?php wp_nonce_field('dropship_nonce', 'dropship_nonce'); ?>
              <fieldset>
                <div class="pure-control-group">
                  <label for="aligned-foo"><?php echo esc_html__('เลือกไฟล์ที่ต้องอัปโหลด', 'dropship'); ?></label>
                  <input type="file" id="file_upload" name="file_upload" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required />
                </div>
                <div class="pure-controls">
                  <input type="submit" name="submit" value="submit" class="pure-button pure-button-primary">
                </div>
              </fieldset>
            </form>
          </div>
          <div class="pure-u-1-3"></div>
        </div>
      </div>
  <?php
    }


}