<?php

require_once('table/sale-dashboard.php');

class Dropship_Admin_Dashboard {
    
    public $dashboard_obj;

    public function __construct() {
		add_filter( 'set-screen-option', [ __CLASS__, 'set_screen' ], 10, 3 );
		add_action( 'admin_menu', [ $this, 'plugin_menu' ] );
    }

	public static function set_screen( $status, $option, $value ) {
		return $value;
	}

    public function dsp_index() {
        $this->dashboard_obj = new Dropship_Sale_Dashboard();
		?>
            <div class="wrap ds-dasbord">
                <!-- <div id="icon-users" class="icon32"></div>
                <h1 class="wp-heading-inline"><?php echo esc_html__( 'แดชบอร์ดคำสั่งซื้อ', 'dropship' ); ?></h1>
                <hr class="wp-header-end"> -->
				<div id="post-body" class="metabox-holder columns-2">
					<div id="post-body-content" class="pure-g ds-wrap-content">
						<div class="pure-u-5-5 banners">
							<div class="ds-header-cat">
								<h1 class="wp-heading-inline ds-menu-sale-cat"><?php echo esc_html__( 'แดชบอร์ดคำสั่งซื้อ', 'dropship' ); ?></h1>
							</div>
						</div>
						<form method="post" id="dsp-order-list-dashboard" data-action="dsp_sale_order_dashboard_export" data-page="<?php echo esc_attr($_GET['page']); ?>">
						<?php
							$this->dashboard_obj->prepare_items();
							$this->dashboard_obj->display(); ?>
						</form>
					</div>
				</div>
            </div>
		<?php
    }
}