<?php

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
}

/**
 * Create a new table class that will extend the WP_List_Table
 */
class Dropship_Report extends WP_List_Table {

	public $per_page;
	public $option_name = 'toplevel_page_dropship-sale-dashboard';

	public function __construct() {
		// Utilize the parent constructor to build the main class properties.
		parent::__construct(
			array(
				'singular' => 'dropship-sale-dashboard',
				'plural'   => 'dropship-sale-dashboard',
				'ajax'     => false,
			)
		);

		// Default number of forms to show per page.
		$this->per_page = (int) 10;
		add_filter( 'manage_' . $this->option_name . '_columns', array( $this, 'get_columns' ), 0 );
	}

	/**
	 * Prepare the items for the table to process
	 *
	 * @return Void
	 */
	public function prepare_items() {
		$columns  = $this->get_columns();
		$hidden   = $this->get_hidden_columns();
		$sortable = $this->get_sortable_columns();
		$statuses = array( 'wc-processing', 'wc-canceled', 'wc-completed', 'wc-refunded' , 'wc-pending' , 'wc-on-hold' , 'wc-failed' );

		$current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] === 'dropship-report-brand') ? 'brand' : 'supplier';

		$orderby  = ( isset( $_REQUEST['orderby'] ) && in_array( $_REQUEST['orderby'], array_keys( $sortable ) ) ) ? sanitize_text_field( $_REQUEST['orderby'] ) : 'id';
		$order    = ( isset( $_REQUEST['order'] ) ) ? sanitize_text_field( $_REQUEST['order'] ) : 'desc';
		$order_status = ( isset( $_REQUEST['order_status'] ) ) ? sanitize_text_field( $_REQUEST['order_status'] ) : '';
		$supplier  = ( isset( $_REQUEST['supplier'] ) ) ? sanitize_text_field( $_REQUEST['supplier'] ) : '';
		$brand  = ( isset( $_REQUEST['brand'] ) ) ? sanitize_text_field( $_REQUEST['brand'] ) : '';

		$daterange_start = ( isset( $_REQUEST['daterange_start'] ) ) ? sanitize_text_field( $_REQUEST['daterange_start'] ) : false;
		$daterange_end   = ( isset( $_REQUEST['daterange_end'] ) ) ? sanitize_text_field( $_REQUEST['daterange_end'] ) : false;

		if ( ${$current_page} == '' ) {
			$terms = get_terms([
				'taxonomy' => $current_page,
				'hide_empty' => false,
			]);
	
			if ( !empty($terms) ) {
				$terms = end($terms);
				${$current_page} = $terms->term_id;
			}
		}

		$args = [];
		$product_ids = dsp_get_taxonomy_data( $current_page , ${$current_page} );
		$dsp_get_orders_ids_by_product_id = Dropship_Functions::get_instance()->dsp_get_orders_ids_by_product_id( $args , $product_ids , $order_status , $daterange_start , $daterange_end );
		$totalItems = count( $dsp_get_orders_ids_by_product_id );
		
        $per_page    = $this->get_items_per_page( 'dsp_spect_per_page', $this->per_page );
		$currentPage = $this->get_pagenum();

		$query     = new WP_Query(
			array_merge(
				$args,
				array(
					'orderby'        => $orderby,
					'order'          => $order,
					'posts_per_page' => $per_page,
					'paged'          => $currentPage,
					'post_type'   	 => 'shop_order',
				)
			)
		);

		if ( empty( $query->posts ) ) {
			$data = [];
		} else {
			$data = $this->prepair_data_object( $query->posts ) ;
		}
		$this->_column_headers = array( $columns, $hidden, $sortable );
		$this->items           = $data;

		$this->set_pagination_args(
			array(
				'total_items' => $totalItems,
				'per_page'    => $per_page,
				'total_pages' => ceil( $totalItems / $per_page ),
			)
		);
	}

	public function prepair_data_object( $order_ids ) {
		$orders = [];
		foreach ($order_ids as $order_id) {
			$orders[] = wc_get_order( $order_id )->get_data();
		}

		return $orders;
	}
	
	/**
	 * Override the parent columns method. Defines the columns to use in your listing table
	 *
	 * @return Array
	 */
	public function get_columns() {
		$columns = Dropship_Functions::get_instance()->dsp_wp_column_report_dashboard();
		return $columns;
	}

	public function get_bulk_actions() {
        return [];
	}

	public function process_bulk_action() {
		return;
	}

	/**
	 * Define which columns are hidden
	 *
	 * @return Array
	 */
	public function get_hidden_columns() {
		$user_id = get_current_user_id();
		$hiddens = get_user_meta( $user_id, 'manage' . $this->option_name . 'columnshidden', true );
		if ( ! empty( $hiddens ) ) {
			return $hiddens;
		} else {
			return array();
		}
	}

	/**
	 * Define the sortable columns
	 *
	 * @return Array
	 */
	public function get_sortable_columns() {
		return array(
			'order'        => array( 'order', false ),
			'date_created' => array( 'date_created', false ),
		);
	}

	function column_cb( $item ) {
		return sprintf(
			'<input type="checkbox" name="bulk_id[]" class="" value="%s" />',
			$item['id']
		);
	}

	/**
	 * Define what data to show on each column of the table
	 *
	 * @param  Array  $item        Data
	 * @param  String $column_name - Current column name
	 *
	 * @return Mixed
	 */
	public function column_default( $item, $column_name ) {
		return Dropship_Functions::get_instance()->dsp_wp_records_table_data_dashboard( $item , $column_name );
	}

	public function extra_tablenav( $which ) {
		if ( $which == 'top' ) :
			$current_page = (isset($_GET['page']) && $_GET['page'] === 'dropship-report-brand') ? 'brand' : 'supplier';

			$order_status = ( isset( $_REQUEST['order_status'] ) ) ? sanitize_text_field( $_REQUEST['order_status'] ) : '';
			$supplier  = ( isset( $_REQUEST['supplier'] ) ) ? sanitize_text_field( $_REQUEST['supplier'] ) : '';
			$brand  = ( isset( $_REQUEST['brand'] ) ) ? sanitize_text_field( $_REQUEST['brand'] ) : '';

			if ( ${$current_page} == '' ) {
				$terms = get_terms([
					'taxonomy' => $current_page,
					'hide_empty' => false,
				]);
		
				if ( !empty($terms) ) {
					$terms = end($terms);
					${$current_page} = $terms->term_id;
				}
			}

			$daterange_start = ( isset( $_REQUEST['daterange_start'] ) ) ? sanitize_text_field( $_REQUEST['daterange_start'] ) : false;
			$daterange_end   = ( isset( $_REQUEST['daterange_end'] ) ) ? sanitize_text_field( $_REQUEST['daterange_end'] ) : false;

			$start_date = $end_date = "";
			if ( $daterange_start && $daterange_end ) {
				$start_date = date( 'd/m/Y', strtotime( str_replace( '/', '-', $daterange_start ) ) );
				$end_date   = date( 'd/m/Y', strtotime( str_replace( '/', '-', $daterange_end ) ) );
			}
		?>

			<div class="alignleft" style="margin-bottom: 5px;">
				<input class="dsp-input-style ds-search" type="text" name="daterange_start" data-start-date="" value="<?php echo esc_attr($start_date); ?>" placeholder="<?php echo esc_attr__( 'ค้นหาจากวันที่', 'dropship' ); ?>" autocomplete="off" />
				<input class="dsp-input-style ds-search" type="text" name="daterange_end" data-end-date="" value="<?php echo esc_attr($end_date); ?>" placeholder="<?php echo esc_attr__( 'ถึงวันที่', 'dropship' ); ?>" autocomplete="off" />
			</div>

			<div class="alignleft ds-select">
				<select name="order_status" id="order_status" class="ds-box-select">
					<option value="" <?php echo ( empty( $order_status ) ) ? esc_attr( 'selected' ) : ''; ?> ><?php echo esc_html__( 'ค้นหาจากสถานะคำสั่งซื้อ', 'dropship' ); ?></option>
					<?php foreach ( wc_get_order_statuses() as $statuses => $statuses_name) : ?>
						<option value="<?php echo esc_attr($statuses); ?>" <?php echo ( !empty( $order_status ) && sanitize_text_field( $order_status ) == $statuses ) ? esc_attr( 'selected' ) : ''; ?> ><?php echo esc_html__( $statuses_name, 'dropship' ); ?></option>
					<?php endforeach; ?>
				</select>

				<?php if ( $current_page == 'supplier' ) : ?>
					<?php echo dsp_generate_dropdown_taxonomy('supplier', 'supplier' , 'supplier' , $supplier ); ?>
				<?php else : ?>
					<?php echo dsp_generate_dropdown_taxonomy('brand', 'brand' , 'brand' , $brand ); ?>
				<?php endif; ?>
				
				<input type="submit" name="filter_action" id="dsp-post-query-submit" class="button" value="<?php echo esc_html__( 'กรอง', 'dropship' ); ?>">
			</div>

			<?php if ( dsp_is_headsale() || dsp_is_operation() ) : ?>
				<div class="alignright ds-select">
					<button type="button" class="button" id="dsp-order-dashboard-export"> <i class="fa fa-download" aria-hidden="true"></i> <?php echo esc_html__( 'ส่งออกข้อมูล', 'dropship' ); ?></button>
				</div>
			<?php endif; ?>
			<style>
				.tablenav.top > .tablenav-pages {
					display: none;
				}
				.ds-search{
					width: 130px;
				}
				.ds-select{
					margin-left: 5px;
					margin-bottom: 5px;
				}
				.ds-box-select{
					width: 200px;
				}

				button.ui-datepicker-current { display: none; }
				@media (min-width: 320px) and (max-width: 480px) {
					.ds-search{
						width: 100%;
					}
					.ds-select{
						margin-left: 0px;
						margin-bottom: 0px;
					}
					.ds-box-select{
						width: 100%;
					}
					#doaction, #doaction2, #dsp-post-query-submit {
						margin: 0 8px 20px 0;
						width: 100%;
					}
				}
				@media (min-width: 768px) and (max-width: 1024px) {
						/* .ds-search {
							width: 205px;
						} */
					}
			</style>
		<?php
		endif;
	}
}