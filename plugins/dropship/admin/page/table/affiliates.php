<?php

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
}

/**
 * Create a new table class that will extend the WP_List_Table
 */
class Dropship_Affiliates_Dashboard extends WP_List_Table {

	public $per_page;
	public $option_name = 'toplevel_page_dropship-affiliates-dashboard';

	public function __construct() {
		// Utilize the parent constructor to build the main class properties.
		parent::__construct(
			array(
				'singular' => 'dropship-affiliates-dashboard',
				'plural'   => 'dropship-affiliates-dashboard',
				'ajax'     => false,
			)
		);

		// Default number of forms to show per page.
		$this->per_page = (int) 20;
		add_filter( 'manage_' . $this->option_name . '_columns', array( $this, 'get_columns' ), 0 );
	}

	/**
	 * Prepare the items for the table to process
	 *
	 * @return Void
	 */
	public function prepare_items() {
        global $table_prefix, $wpdb;

		$columns  = $this->get_columns();
		$hidden   = $this->get_hidden_columns();
		$sortable = $this->get_sortable_columns();
		$orderby  = ( isset( $_REQUEST['orderby'] ) && in_array( $_REQUEST['orderby'], array_keys( $sortable ) ) ) ? sanitize_text_field( $_REQUEST['orderby'] ) : 'sale_id';
		$order    = ( isset( $_REQUEST['order'] ) ) ? sanitize_text_field( $_REQUEST['order'] ) : 'desc';
		$s        = ( isset( $_REQUEST['s'] ) ) ? sanitize_text_field( $_REQUEST['s'] ) : '';

		$this->process_bulk_action();
        $table_name = $table_prefix . DROPSHIP_TABLE_SALE_COMMISSION;
		$totalItems = $wpdb->get_var("SELECT COUNT(*) FROM $table_name");

		$per_page    = $this->get_items_per_page( 'dsp_spect_per_page', $this->per_page );
		$currentPage = $this->get_pagenum();

        $this->items = $this->prepair_data_object( $table_name , $orderby , $order , $per_page , $currentPage );

        $this->_column_headers = array( $columns, $hidden, $sortable );
        // [REQUIRED] configure pagination
        $this->set_pagination_args(array(
            'total_items' => $totalItems, // total items defined above
            'per_page' => $per_page, // per page constant defined at top of method
            'total_pages' => ceil($totalItems / $per_page) // calculate pages count
        ));
	}

    public function prepair_data_object( $table_name , $orderby , $order , $per_page , $currentPage ) {
        global $wpdb;

		$items = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT * FROM $table_name ORDER BY $orderby $order LIMIT %d OFFSET %d", 
                $per_page, 
                $currentPage - 1
                ), 
                ARRAY_A
        );
        
		return $items;
	}

	/**
	 * Override the parent columns method. Defines the columns to use in your listing table
	 *
	 * @return Array
	 */
	public function get_columns() {
		$columns = array(
			'cb'                => '<input type="checkbox" />',
			'sale_id'			=> esc_html__( 'ID', 'dropship' ),
			'name'				=> esc_html__( 'ชื่อ', 'dropship' ),
			'create_datetime'	=> esc_html__( 'วันที่สร้าง', 'dropship' ),
			'com_value'		    => esc_html__( 'คอมมิชชั่น (%)', 'dropship' )
		);

		return $columns;
	}

	public function get_bulk_actions() {
		return array();
	}

	public function process_bulk_action() {
		if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
			// security check!
			if ( isset( $_POST['_wpnonce'] ) && ! empty( $_POST['_wpnonce'] ) ) {
				$nonce  = filter_input( INPUT_POST, '_wpnonce', FILTER_SANITIZE_STRING );
				$action = 'bulk-' . $this->_args['plural'];

				if ( ! wp_verify_nonce( $nonce, $action ) ) {
					wp_die( 'Nope! Security check failed!' );
				}
			}

			$bulk_ids = isset( $_POST['bulk_id'] ) ? array_map( 'absint', (array) $_POST['bulk_id'] ) : array(); // phpcs:ignore WordPress.Security.NonceVerification
			if ( count( $bulk_ids ) > 0 ) {
				$action = $this->current_action();
				switch ( $action ) {
					case 'bulk_custom_shipping':
						foreach ( $bulk_ids as $bulk_id ) {
							if ( $bulk_id ) {
								update_post_meta( esc_attr( $bulk_id ), '_use_shippop_shipping', 'N' );
							}
						}
						break;
					default:
						// do nothing or something else
						return;
						break;
				}
			}
		}

		return;
	}

	/**
	 * Define which columns are hidden
	 *
	 * @return Array
	 */
	public function get_hidden_columns() {
		$user_id = get_current_user_id();
		$hiddens = get_user_meta( $user_id, 'manage' . $this->option_name . 'columnshidden', true );
		if ( ! empty( $hiddens ) ) {
			return $hiddens;
		} else {
			return array();
		}
	}

	/**
	 * Define the sortable columns
	 *
	 * @return Array
	 */
	public function get_sortable_columns() {
		return array(
			'sale_id'        => array( 'sale_id', false ),
			'create_datetime' => array( 'create_datetime', false ),
		);
	}

	function column_cb( $item ) {
		return sprintf(
			'<input type="checkbox" name="bulk_id[]" class="" value="%s" />',
			$item['sale_id']
		);
	}

	/**
	 * Define what data to show on each column of the table
	 *
	 * @param  Array  $item        Data
	 * @param  String $column_name - Current column name
	 *
	 * @return Mixed
	 */
	public function column_default( $item, $column_name ) {
		switch ( $column_name ) {
			case 'sale_id':
            case 'create_datetime':
            case 'com_value':
				return $item[ $column_name ];
                break;
            case 'name':
                $user = get_user_by( 'id', $item[ 'sale_id' ] );
                return $user->display_name;
			default:
				return print_r( $item, true );
				break;
		}
	}

	public function extra_tablenav( $which ) {
	}
}