<?php

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
}

/**
 * Create a new table class that will extend the WP_List_Table
 */
class Dropship_Sale_Dashboard extends WP_List_Table {

	public $per_page;
	public $option_name = 'toplevel_page_dropship-sale-dashboard';

	public function __construct() {
		// Utilize the parent constructor to build the main class properties.
		parent::__construct(
			array(
				'singular' => 'dropship-sale-dashboard',
				'plural'   => 'dropship-sale-dashboard',
				'ajax'     => false,
			)
		);

		// Default number of forms to show per page.
		$this->per_page = (int) 10;
		add_filter( 'manage_' . $this->option_name . '_columns', array( $this, 'get_columns' ), 0 );
	}

	/**
	 * Prepare the items for the table to process
	 *
	 * @return Void
	 */
	public function prepare_items() {
		$columns  = $this->get_columns();
		$hidden   = $this->get_hidden_columns();
		$sortable = $this->get_sortable_columns();
		$orderby  = ( isset( $_REQUEST['orderby'] ) && in_array( $_REQUEST['orderby'], array_keys( $sortable ) ) ) ? sanitize_text_field( $_REQUEST['orderby'] ) : 'id';
		$order    = ( isset( $_REQUEST['order'] ) ) ? sanitize_text_field( $_REQUEST['order'] ) : 'desc';
		$status   = ( isset( $_REQUEST['status'] ) ) ? sanitize_text_field( $_REQUEST['status'] ) : '';
		$order_status = ( isset( $_REQUEST['order_status'] ) ) ? sanitize_text_field( $_REQUEST['order_status'] ) : '';
		$sale_id  = ( isset( $_REQUEST['sale_id'] ) ) ? sanitize_text_field( $_REQUEST['sale_id'] ) : 0;

		$this->process_bulk_action();

		$daterange_start = ( isset( $_REQUEST['daterange_start'] ) ) ? sanitize_text_field( $_REQUEST['daterange_start'] ) : false;
		$daterange_end   = ( isset( $_REQUEST['daterange_end'] ) ) ? sanitize_text_field( $_REQUEST['daterange_end'] ) : false;

		$args = [];
		$totalItems = Dropship_Functions::get_instance()->dsp_wp_query_sale_dashboard( $args , $status , $order_status , $sale_id , $daterange_start , $daterange_end , true);
		$per_page    = $this->get_items_per_page( 'dsp_spect_per_page', $this->per_page );
		$currentPage = $this->get_pagenum();

		$orderMetaKey = "";
		if ( $orderby == "commission_price" ) {
			$orderby = "meta_value_num";
			$orderMetaKey = "_dsp_order_paid_commission";
		}

		$query     = new WP_Query(
			array_merge(
				$args,
				array(
					'orderby'        => $orderby,
					'order'          => $order,
					'meta_key'		 => $orderMetaKey,
					'posts_per_page' => $per_page,
					'paged'          => $currentPage,
				)
			)
		);
		$data = $this->prepair_data_object( $query->posts ) ;
		$this->_column_headers = array( $columns, $hidden, $sortable );
		$this->items           = $data;

		$this->set_pagination_args(
			array(
				'total_items' => $totalItems,
				'per_page'    => $per_page,
				'total_pages' => ceil( $totalItems / $per_page ),
			)
		);
	}

	public function prepair_data_object( $order_ids ) {
		$orders = [];
		foreach ($order_ids as $order_id) {
			$orders[] = wc_get_order( $order_id )->get_data();
		}

		return $orders;
	}
	
	/**
	 * Override the parent columns method. Defines the columns to use in your listing table
	 *
	 * @return Array
	 */
	public function get_columns() {
		$columns = Dropship_Functions::get_instance()->dsp_wp_column_sale_dashboard();
		return $columns;
	}

	public function get_bulk_actions() {
		if ( dsp_is_headsale() ) {
			return array(
				'update_paid' => esc_html__( 'อัปเดทสถานะเป็นทำจ่ายแล้ว', 'dropship' ),
				'update_unpaid' => esc_html__( 'อัปเดทสถานะเป็นยังไม่ทำจ่าย', 'dropship' ),
			);
		} else {
			return [];
		}
	}

	public function process_bulk_action() {
		if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
			// security check!
			if ( isset( $_POST['_wpnonce'] ) && ! empty( $_POST['_wpnonce'] ) ) {
				$nonce  = filter_input( INPUT_POST, '_wpnonce', FILTER_SANITIZE_STRING );
				$action = 'bulk-' . $this->_args['plural'];

				if ( ! wp_verify_nonce( $nonce, $action ) ) {
					wp_die( 'Nope! Security check failed!' );
				}
			}

			$bulk_ids = isset( $_POST['bulk_id'] ) ? array_map( 'absint', (array) $_POST['bulk_id'] ) : array(); // phpcs:ignore WordPress.Security.NonceVerification
			if ( count( $bulk_ids ) > 0 ) {
				$action = $this->current_action();
				switch ( $action ) {
					case 'update_paid':
						foreach ( $bulk_ids as $bulk_id ) {
							if ( $bulk_id ) {
								update_post_meta( $bulk_id , '_dsp_paid_success' , 'yes' );
							}
						}
						break;
					case 'update_unpaid':
						foreach ( $bulk_ids as $bulk_id ) {
							if ( $bulk_id ) {
								update_post_meta( $bulk_id , '_dsp_paid_success' , 'no' );
							}
						}
						break;
					default:
						// do nothing or something else
						return;
						break;
				}
			}
		}

		return;
	}

	/**
	 * Define which columns are hidden
	 *
	 * @return Array
	 */
	public function get_hidden_columns() {
		$user_id = get_current_user_id();
		$hiddens = get_user_meta( $user_id, 'manage' . $this->option_name . 'columnshidden', true );
		if ( ! empty( $hiddens ) ) {
			return $hiddens;
		} else {
			return array();
		}
	}

	/**
	 * Define the sortable columns
	 *
	 * @return Array
	 */
	public function get_sortable_columns() {
		return array(
			'order'        => array( 'order', false ),
			'date_created' => array( 'date_created', false ),
			'commission_price' => array( 'commission_price', false ),
		);
	}

	function column_cb( $item ) {
		return sprintf(
			'<input type="checkbox" name="bulk_id[]" class="" value="%s" />',
			$item['id']
		);
	}

	/**
	 * Define what data to show on each column of the table
	 *
	 * @param  Array  $item        Data
	 * @param  String $column_name - Current column name
	 *
	 * @return Mixed
	 */
	public function column_default( $item, $column_name ) {
		return Dropship_Functions::get_instance()->dsp_wp_records_table_data_dashboard( $item , $column_name );
	}

	public function extra_tablenav( $which ) {
		if ( $which == 'top' ) :
			$status = ( isset( $_REQUEST['status'] ) ) ? sanitize_text_field( $_REQUEST['status'] ) : '';
			$order_status = ( isset( $_REQUEST['order_status'] ) ) ? sanitize_text_field( $_REQUEST['order_status'] ) : '';
			$sale_id  = ( isset( $_REQUEST['sale_id'] ) ) ? sanitize_text_field( $_REQUEST['sale_id'] ) : 0;
			$supplier  = ( isset( $_REQUEST['supplier'] ) ) ? sanitize_text_field( $_REQUEST['supplier'] ) : '';

			$daterange_start = ( isset( $_REQUEST['daterange_start'] ) ) ? sanitize_text_field( $_REQUEST['daterange_start'] ) : false;
			$daterange_end   = ( isset( $_REQUEST['daterange_end'] ) ) ? sanitize_text_field( $_REQUEST['daterange_end'] ) : false;

			$start_date = $end_date = "";
			if ( $daterange_start && $daterange_end ) {
				$start_date = date( 'd/m/Y', strtotime( str_replace( '/', '-', $daterange_start ) ) );
				$end_date   = date( 'd/m/Y', strtotime( str_replace( '/', '-', $daterange_end ) ) );
			}
		?>

			<div class="alignleft" style="margin-bottom: 5px;">
				<input class="dsp-input-style ds-search" type="text" name="daterange_start" data-start-date="" value="<?php echo esc_attr($start_date); ?>" placeholder="<?php echo esc_attr__( 'ค้นหาจากวันที่', 'dropship' ); ?>" autocomplete="off" />
				<input class="dsp-input-style ds-search" type="text" name="daterange_end" data-end-date="" value="<?php echo esc_attr($end_date); ?>" placeholder="<?php echo esc_attr__( 'ถึงวันที่', 'dropship' ); ?>" autocomplete="off" />
			</div>

			<div class="alignleft ds-select">
				<select name="order_status" id="order_status" class="ds-box-select">
					<option value="" <?php echo ( empty( $order_status ) ) ? esc_attr( 'selected' ) : ''; ?> ><?php echo esc_html__( 'ค้นหาจากสถานะคำสั่งซื้อ', 'dropship' ); ?></option>
					<?php foreach ( wc_get_order_statuses() as $statuses => $statuses_name) : ?>
						<option value="<?php echo esc_attr($statuses); ?>" <?php echo ( !empty( $order_status ) && sanitize_text_field( $order_status ) == $statuses ) ? esc_attr( 'selected' ) : ''; ?> ><?php echo esc_html__( $statuses_name, 'dropship' ); ?></option>
					<?php endforeach; ?>
				</select>

				<select name="status" id="status" class="ds-box-select">
					<option value="" <?php echo ( empty( $status ) ) ? esc_attr( 'selected' ) : ''; ?> ><?php echo esc_html__( 'ค้นหาจากสถานะการทำจ่าย', 'dropship' ); ?></option>
					<option value="paid" <?php echo ( !empty( $status ) && sanitize_text_field( $status ) == 'paid' ) ? esc_attr( 'selected' ) : ''; ?> ><?php echo esc_html__( 'ทำจ่ายแล้ว', 'dropship' ); ?></option>
					<option value="unpaid" <?php echo ( !empty( $status ) && sanitize_text_field( $status ) == 'unpaid' ) ? esc_attr( 'selected' ) : ''; ?> ><?php echo esc_html__( 'ยังไม่ทำจ่าย', 'dropship' ); ?></option>
				</select>

				<?php if ( dsp_is_headsale() || dsp_is_operation() ) : ?>
					<?php wp_dropdown_users( array( 'name' => 'sale_id' , 'id' => 'sale_id' , 'selected' => $sale_id , 'role__in' => ['dropship_sale'] , 'show_option_none' => esc_html__( 'ค้นหาจากชื่อผู้ขาย', 'dropship' ) ) ); ?>
				<?php endif; ?>
				
				<input type="submit" name="filter_action" id="dsp-post-query-submit" class="button ds-button-c" value="<?php echo esc_html__( 'กรอง', 'dropship' ); ?>">
			</div>

			<?php if ( dsp_is_headsale() || dsp_is_sale() || dsp_is_operation() ) : ?>
				<div class="alignright ds-select">
					<button type="button" class="button ds-button-a" id="dsp-order-dashboard-export"> <i class="fa fa-download" aria-hidden="true"></i> <?php echo esc_html__( 'ส่งออกคำสั่งซื้อ', 'dropship' ); ?></button>
				</div>
			<?php endif; ?>
			<style>
				.tablenav.top > .tablenav-pages {
					display: none;
				}
				.ds-search{
					width: 130px;
				}
				.ds-select{
					margin-left: 5px;
					margin-bottom: 5px;
				}
				.ds-box-select{
					width: 200px;
				}
				button.ui-datepicker-current { 
					display: none; 
				}
				@media (min-width: 320px) and (max-width: 480px) {
					.ds-search{
						width: 100%;
					}
					.ds-select{
						margin-left: 0px;
						margin-bottom: 0px;
					}
					.ds-box-select{
						width: 100%;
					}
					#doaction, #doaction2, #dsp-post-query-submit {
						margin: 0 8px 20px 0;
						width: 100%;
					}
				}

				@media (min-width: 768px) and (max-width: 1024px) {
						/* .ds-search {
							width: 205px;
						} */
					}
			</style>
		<?php
		endif;
	}
}