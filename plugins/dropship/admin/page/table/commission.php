<?php

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
}

/**
 * Create a new table class that will extend the WP_List_Table
 */
class Dropship_Commission_Dashboard extends WP_List_Table {

	public $per_page;
	public $option_name = 'toplevel_page_dropship-commission-dashboard';

	public function __construct() {
		// Utilize the parent constructor to build the main class properties.
		parent::__construct(
			array(
				'singular' => 'dropship-commission-dashboard',
				'plural'   => 'dropship-commission-dashboard',
				'ajax'     => false,
			)
		);

		// Default number of forms to show per page.
		$this->per_page = (int) 20;
		add_filter( 'manage_' . $this->option_name . '_columns', array( $this, 'get_columns' ), 0 );
	}

	/**
	 * Prepare the items for the table to process
	 *
	 * @return Void
	 */
	public function prepare_items() {
		$columns  = $this->get_columns();
		$hidden   = $this->get_hidden_columns();
		$sortable = $this->get_sortable_columns();
		$orderby  = ( isset( $_REQUEST['orderby'] ) && in_array( $_REQUEST['orderby'], array_keys( $sortable ) ) ) ? sanitize_text_field( $_REQUEST['orderby'] ) : 'sale_id';
		$order    = ( isset( $_REQUEST['order'] ) ) ? sanitize_text_field( $_REQUEST['order'] ) : 'desc';

		$this->process_bulk_action();
		$per_page    = $this->get_items_per_page( 'dsp_spect_per_page', $this->per_page );
		$currentPage = $this->get_pagenum();

		$result = Dropship_Model_Products::get_instance()->dsp_get_sale_products_commission( $per_page , $currentPage , $orderby , $order );
		$totalItems = $result['total'];
        $this->items = $result['items'];

        $this->_column_headers = array( $columns, $hidden, $sortable );
        // [REQUIRED] configure pagination
        $this->set_pagination_args(array(
            'total_items' => $totalItems, // total items defined above
            'per_page' => $per_page, // per page constant defined at top of method
            'total_pages' => ceil($totalItems / $per_page) // calculate pages count
        ));
	}

	/**
	 * Override the parent columns method. Defines the columns to use in your listing table
	 *
	 * @return Array
	 */
	public function get_columns() {
		$columns = array(
			'cb'                => '<input type="checkbox" />',
			'id'				=> esc_html__( 'ID', 'dropship' ),
			'dsp_sale_id'		=> esc_html__( 'Sale ID', 'dropship' ),
			'sale_id'			=> esc_html__( 'ขาย', 'dropship' ),
			'product_id'		=> esc_html__( 'สินค้า', 'dropship' ),
			'product_sku'		=> esc_html__( 'Sku', 'dropship' ),
			'com_value'		    => esc_html__( 'คอมมิชชั่น (%)', 'dropship' ),
			'update_datetime'	=> esc_html__( 'วันที่สร้าง', 'dropship' ),
		);

		return $columns;
	}

	public function get_bulk_actions() {
        return array(
			'delete' => esc_html__( 'ลบ', 'dropship' ),
		);
	}

	public function process_bulk_action() {
		if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
			// security check!
			if ( isset( $_POST['_wpnonce'] ) && ! empty( $_POST['_wpnonce'] ) ) {
				$nonce  = filter_input( INPUT_POST, '_wpnonce', FILTER_SANITIZE_STRING );
				$action = 'bulk-' . $this->_args['plural'];

				if ( ! wp_verify_nonce( $nonce, $action ) ) {
					wp_die( 'Nope! Security check failed!' );
				}
			}

			$bulk_ids = isset( $_POST['bulk_id'] ) ? array_map( 'absint', (array) $_POST['bulk_id'] ) : array(); // phpcs:ignore WordPress.Security.NonceVerification
			if ( count( $bulk_ids ) > 0 ) {
				$action = $this->current_action();
				switch ( $action ) {
					case 'delete':
						foreach ( $bulk_ids as $bulk_id ) {
							if ( $bulk_id ) {
								Dropship_Model_Products::get_instance()->dsp_delete_sale_products_commission( $bulk_id );
							}
						}
						break;
					default:
						// do nothing or something else
						return;
						break;
				}
			}
		}

		return;
	}

	/**
	 * Define which columns are hidden
	 *
	 * @return Array
	 */
	public function get_hidden_columns() {
		$user_id = get_current_user_id();
		$hiddens = get_user_meta( $user_id, 'manage' . $this->option_name . 'columnshidden', true );
		if ( ! empty( $hiddens ) ) {
			return $hiddens;
		} else {
			return array();
		}
	}

	/**
	 * Define the sortable columns
	 *
	 * @return Array
	 */
	public function get_sortable_columns() {
		return array(
			'sale_id'        => array( 'sale_id', false ),
			'update_datetime' => array( 'update_datetime', false ),
		);
	}

	function column_cb( $item ) {
		return sprintf(
			'<input type="checkbox" name="bulk_id[]" class="" value="%s" />',
			$item['sale_id']
		);
	}

	/**
	 * Define what data to show on each column of the table
	 *
	 * @param  Array  $item        Data
	 * @param  String $column_name - Current column name
	 *
	 * @return Mixed
	 */
	public function column_default( $item, $column_name ) {
		switch ( $column_name ) {
			case 'sale_id':
				$user = get_user_by( 'id', $item[ 'sale_id' ] );
				$name = (!empty($user)) ? $user->display_name : false;
				if ( $name ) {
					return "<a href='" . add_query_arg( 'user_id', $item[ 'sale_id' ] , self_admin_url( 'user-edit.php'))  . "'>" . $name . "</a>";
				} else {
					return "-";
				}
				break;
			case 'dsp_sale_id':
				$dsp_sale_id = get_user_meta( $item[ 'sale_id' ] , '_dsp_sale_id' , true );
				return $dsp_sale_id;
				break;
			case 'product_id':
				$product = wc_get_product( $item[ 'product_id' ] );
				$name = (!empty($product)) ? $product->get_title() : false;
				if ( $name ) {
					return "<a href='" . get_edit_post_link( $item[ 'product_id' ] )  . "'>" . $name . "</a>";
				} else {
					return "-";
				}
				break;
			case 'product_sku':
				$product = wc_get_product( $item[ 'product_id' ] );
				$sku = (!empty($product)) ? $product->get_sku() : "-";
				return $sku;
				break;
			case 'update_datetime':
				return date("d/m/Y H:i:s" , strtotime($item['update_datetime']));
				break;
            case 'com_value':
			case 'id':
				return $item[ $column_name ];
                break;
			default:
				return print_r( $item, true );
				break;
		}
	}

	public function extra_tablenav( $which ) {
	}
}