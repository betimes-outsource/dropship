<?php

class Dropship_Admin_Order_Detail
{

    public function __construct()
    {
    }

    /**
     * Display a admin menu
     */
    function dsp_index()
    {
?>
        <div class="wrap">
            <div class="pure-u-5-5 banners" id="Order-Detail-product">
                <div class="ds-header-cat" style="margin:10px">
                    <div id="icon-users" class="icon32"></div>
                    <h1 class="wp-heading-inline ds-menu-sale-cat"><?php echo esc_html__('Order', 'dropship'); ?></h1>
                </div>
            </div>

            <?php $this->prepair(); ?>
        </div>

    <?php
    }

    private function prepair()
    {
        if (!isset($_GET['order_id'])) {
            wp_redirect(admin_url('admin.php?page=dropship-dashboard'));
            die;
        }
        $order_id = $_GET['order_id'];
        $order = wc_get_order($order_id);
        $_dsp_ref_id = get_post_meta($order_id, '_dsp_ref_id', true);
        if (is_null($order) || $order == false || get_current_user_id() != $_dsp_ref_id) {
            wp_redirect(admin_url('admin.php?page=dropship-dashboard'));
            die;
        }
        $state = WC()->countries->get_states("TH");
    ?>
        <div class="pure-g ds-order-detail-info" id="Order-Detail-pure">
            <div class="pure-u-sm-1-1 pure-u-md-12-24 pure-u-lg-6-24">
                <div class="ds-order-block ds-vertical-line-right">
                    <h3 class="wp-heading-inline ds-order-title"><?php echo esc_html__('รายละเอียด', 'dropship'); ?></h3>
                    <table style="width:100%">
                        <tr>
                            <th style="float: left;"><?php echo esc_html__('หมายเลขคำสั่งซื้อ', 'dropship'); ?></th>
                            <td><?php echo esc_html($order_id); ?></td>
                        </tr>
                        <tr>
                            <th style="float: left;"><?php echo esc_html__('วันที่สั่งซื้อ', 'dropship'); ?></th>
                            <td><?php echo esc_html($order->get_date_modified()->date("d/m/Y H:i:s")); ?></td>
                        </tr>
                        <tr>
                            <th style="float: left;"><?php echo esc_html__('สถานะ', 'dropship'); ?></th>
                            <td><?php echo esc_html(wc_get_order_status_name($order->get_status())); ?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="pure-u-sm-1-1 pure-u-md-12-24 pure-u-lg-6-24">
                <div class="ds-order-block ds-vertical-line-right">
                    <h3 class="wp-heading-inline ds-order-title"><?php echo esc_html__('ขนส่ง', 'dropship'); ?></h3>
                    <table style="width:100%">
                        <tr>
                            <th style="float: left;"><?php echo esc_html__('ลูกค้า', 'dropship'); ?></th>
                            <td><?php echo esc_html($order->get_shipping_first_name()) . " " . esc_html($order->get_shipping_last_name()); ?></td>
                        </tr>
                        <tr>
                            <th style="float: left;"><?php echo esc_html__('ที่อยู่', 'dropship'); ?></th>
                            <td><?php echo esc_html($order->get_shipping_address_1()); ?></td>
                        </tr>
                        <tr>
                            <th style="float: left;"><?php echo esc_html__('แขวง-ตำบล', 'dropship'); ?></th>
                            <td><?php echo esc_html($order->get_shipping_address_2()); ?></td>
                        </tr>
                        <tr>
                            <th style="float: left;"><?php echo esc_html__('เขต-อำเภอ', 'dropship'); ?></th>
                            <td><?php echo esc_html($order->get_shipping_city()); ?></td>
                        </tr>
                        <tr>
                            <th style="float: left;"><?php echo esc_html__('จังหวัด', 'dropship'); ?></th>
                            <td><?php echo (isset($state[$order->get_shipping_state()])) ? esc_html($state[$order->get_shipping_state()]) : "-" . " " . esc_html($order->get_shipping_postcode()); ?></td>
                        </tr>
                        <tr>
                            <th style="float: left;"><?php echo esc_html__('เบอร์โทรศัพท์', 'dropship'); ?></th>
                            <td><?php echo esc_html($order->get_billing_phone()); ?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- <div class="block-emty"></div> -->
            <div class="pure-u-sm-1-1 pure-u-md-12-24 pure-u-lg-6-24">
                <div class="ds-order-block ds-vertical-line-right">
                    <h3 class="wp-heading-inline ds-order-title"><?php echo esc_html__('บัญชี', 'dropship'); ?></h3>
                    <table style="width:100%;float: left">
                        <tr>
                            <th style="float: left;"><?php echo esc_html__('ลูกค้า', 'dropship'); ?></th>
                            <td><?php echo esc_html($order->get_billing_first_name()) . " " . esc_html($order->get_billing_last_name()); ?></td>
                        </tr>
                        <tr>
                            <th style="float: left;"><?php echo esc_html__('ที่อยู่', 'dropship'); ?></th>
                            <td><?php echo esc_html($order->get_billing_address_1()); ?></td>
                        </tr>
                        <tr>
                            <th style="float: left;"><?php echo esc_html__('แขวง-ตำบล', 'dropship'); ?></th>
                            <td><?php echo esc_html($order->get_billing_address_2()); ?></td>
                        </tr>
                        <tr>
                            <th style="float: left;"><?php echo esc_html__('เขต-อำเภอ', 'dropship'); ?></th>
                            <td><?php echo esc_html($order->get_billing_city()); ?></td>
                        </tr>
                        <tr>
                            <th style="float: left;"><?php echo esc_html__('จังหวัด', 'dropship'); ?></th>
                            <td><?php echo (isset($state[$order->get_billing_state()])) ? esc_html($state[$order->get_billing_state()]) : "-" . " " . esc_html($order->get_billing_postcode()); ?></td>
                        </tr>
                        <tr>
                            <th style="float: left;"><?php echo esc_html__('อีเมล', 'dropship'); ?></th>
                            <td><?php echo esc_html($order->get_billing_email()); ?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="pure-u-sm-1-1 pure-u-md-12-24 pure-u-lg-6-24">
                <div class="ds-order-block">
                    <h3 class="wp-heading-inline ds-order-title"><?php echo esc_html__('คอมมิชชั่น (%)', 'dropship'); ?></h3>
                    <?php echo esc_html(dsp_show_metabox_commission_in_posttype($order_id, false)); ?>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <br />
        <div class="pure-g ds-order-detail-product" id="Order-Detail-pure">
            <!----- webmode ------>
            <div class="pure-u-2-3 webmode">
                <div class="ds-header-cat" id="Order-Detail-Label2">
                    <div id="icon-users" class="icon32"></div>
                    <h1 class="wp-heading-inline ds-menu-sale-cat"><?php echo esc_html__('สินค้า', 'dropship'); ?></h1>
                </div>

                <table class="ds-table-list-product">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th><?php echo esc_html__('รูปภาพ', 'dropship'); ?></th>
                            <th><?php echo esc_html__('ชื่อ', 'dropship'); ?></th>
                            <th><?php echo esc_html__('จำนวน', 'dropship'); ?></th>
                            <th><?php echo esc_html__('ราคา', 'dropship'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $count = 1;
                        foreach ($order->get_items() as $key => $item) :
                        ?>
                            <tr>
                                <td><?php echo $count; ?></td>
                                <td><img src="<?php echo esc_url(get_the_post_thumbnail_url($item->get_product_id(), 'thumbnail')); ?>" /></td>
                                <td><?php echo esc_html($item->get_name()); ?></td>
                                <td><?php echo esc_html($item->get_quantity()); ?></td>
                                <td><?php echo (wc_price($item->get_total())); ?></td>
                            </tr>
                        <?php
                            $count++;
                        endforeach; ?>
                    </tbody>
                    <tfoot style="display: contents;">
                        <tr>
                            <td colspan="3"></td>
                            <td><?php echo esc_html__('รวมทั้งหมด', 'dropship'); ?></td>
                            <td><?php echo ($order->get_subtotal_to_display()); ?></td>
                        </tr>
                    </tfoot>
                </table>
            </div>

            <div class="pure-u-1-3 webmode" style="margin-top: 5%">
                <div class="ds-order-block">
                    <h3 class="wp-heading-inline ds-order-title"><?php echo esc_html__('รวม', 'dropship'); ?></h3>
                    <table style="width:100%;float: left">
                        <tr>
                            <th style="float: left;"><?php echo esc_html__('รูปแบบขนส่ง', 'dropship'); ?></th>
                            <td><?php echo esc_html($order->get_shipping_method()); ?></td>
                        </tr>
                        <tr>
                            <th style="float: left;"><?php echo esc_html__('ค่าขนส่ง', 'dropship'); ?></th>
                            <td><?php echo (wc_price($order->get_shipping_total())); ?></td>
                        </tr>
                        <tr>
                            <th style="float: left;"><?php echo esc_html__('รูปแบบการจ่ายเงิน', 'dropship'); ?></th>
                            <td><?php echo esc_html($order->get_payment_method_title()); ?></td>
                        </tr>
                        <tr>
                            <th style="float: left;"><?php echo esc_html__('รวมทั้งหมด', 'dropship'); ?></th>
                            <td><?php echo (wc_price($order->get_total())); ?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <!------ end webmode ------->

            <!-----mobilemode------>
            <div class="pure-u-1-1 mobilemode">
                <div class="ds-header-cat" id="Order-Detail-Label2">
                    <div id="icon-users" class="icon32"></div>
                    <h1 class="wp-heading-inline ds-menu-sale-cat"><?php echo esc_html__('สินค้า', 'dropship'); ?></h1>
                </div>

                <?php foreach ($order->get_items() as $key => $item) : ?>
                    <div class="pure-g">
                        <div class="pure-u-1-3 ds-detail-img-m">
                            <img src="<?php echo esc_url(get_the_post_thumbnail_url($item->get_product_id(), 'thumbnail')); ?>" />
                        </div>
                        <div class="pure-u-2-3">
                            <div class="ds-detail-name-m">
                                <p><?php echo esc_html($item->get_name()); ?></p>
                            </div>
                        </div>
                        <div class="pure-u-1-2">
                            <div class="ds-detail-left-m">
                                <p class="ds-detail-title-m"><?php echo esc_html__('จำนวน', 'dropship'); ?></p>
                            </div>
                        </div>
                        <div class="pure-u-1-2">
                            <div class="ds-detail-right-m">
                                <p class="ds-detail-desc-m">x<?php echo esc_html($item->get_quantity()); ?></p>
                            </div>
                        </div>
                        <div class="pure-u-1-2">
                            <div class="ds-detail-left-m">
                                <p class="ds-detail-title-m"><?php echo esc_html__('ราคา', 'dropship'); ?></p>
                            </div>
                        </div>
                        <div class="pure-u-1-2">
                            <div class="ds-detail-right-m">
                                <p class="ds-detail-desc-m"><?php echo (wc_price($item->get_total())); ?></p>
                            </div>
                        </div>
                    </div>
                    <hr>
                <?php endforeach; ?>

            </div>

            <div class="pure-u-1-1 mobilemode" style="margin-top: 15px">
                <h3 class="wp-heading-inline ds-order-title" style="margin-bottom: 10px;"><?php echo esc_html__('รวม', 'dropship'); ?></h3>
                <table style="width:100%;float: left">
                    <tr>
                        <th style="float: left;"><?php echo esc_html__('รูปแบบขนส่ง', 'dropship'); ?></th>
                        <td><?php echo esc_html($order->get_shipping_method()); ?></td>
                    </tr>
                    <tr>
                        <th style="float: left;"><?php echo esc_html__('ค่าขนส่ง', 'dropship'); ?></th>
                        <td><?php echo (wc_price($order->get_shipping_total())); ?></td>
                    </tr>
                    <tr>
                        <th style="float: left;"><?php echo esc_html__('รูปแบบการจ่ายเงิน', 'dropship'); ?></th>
                        <td><?php echo esc_html($order->get_payment_method_title()); ?></td>
                    </tr>
                    <tr>
                        <th style="float: left;"><?php echo esc_html__('รวมทั้งหมด', 'dropship'); ?></th>
                        <td><?php echo (wc_price($order->get_total())); ?></td>
                    </tr>
                </table>
            </div>
            <!-----end mobilemode------>
        </div>
<?php
    }
}
