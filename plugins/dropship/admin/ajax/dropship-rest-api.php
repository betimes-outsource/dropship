<?php

class Dropship_Rest_API_Admin
{

    function __construct()
    {
        add_action('wp_ajax_dsp_get_generate_custom_url', [$this, 'dsp_get_generate_custom_url']);
        add_action('wp_ajax_dsp_get_product_catalog', [$this, 'dsp_get_product_catalog']);
        add_action('wp_ajax_dsp_add_product_catalog', [$this, 'dsp_add_product_catalog']);

        add_action('wp_ajax_dsp_update_banners_catalog_sale', [$this, 'dsp_update_banners_catalog_sale']);

        add_action('wp_ajax_dsp_sale_order_dashboard_export', [$this, 'dsp_sale_order_dashboard_export']);
        add_action('wp_ajax_dsp_sale_report_dashboard_export', [$this, 'dsp_sale_report_dashboard_export']);
    }

    function dsp_get_generate_custom_url()
    {
        $ret = ['status' => true];
        $url = $_POST['url'];
        if (empty($url) || filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
            $ret = ['status' => false, 'msg' => esc_html__('Require url / url incorrect', 'dropship')];
        } else {
            $generate = dsp_concat_ref_id($url);
            $ret['data'] = $generate;
        }

        return dsp_response_json($ret);
    }

    function dsp_sale_order_dashboard_export() {
        $ret = ['status' => true];

		$status   = ( isset( $_REQUEST['status'] ) ) ? sanitize_text_field( $_REQUEST['status'] ) : '';
		$order_status = ( isset( $_REQUEST['order_status'] ) ) ? sanitize_text_field( $_REQUEST['order_status'] ) : '';
		$sale_id  = ( isset( $_REQUEST['sale_id'] ) ) ? sanitize_text_field( $_REQUEST['sale_id'] ) : 0;

		$daterange_start = ( isset( $_REQUEST['daterange_start'] ) ) ? sanitize_text_field( $_REQUEST['daterange_start'] ) : false;
		$daterange_end   = ( isset( $_REQUEST['daterange_end'] ) ) ? sanitize_text_field( $_REQUEST['daterange_end'] ) : false;

		$args = [];
		$Items = Dropship_Functions::get_instance()->dsp_wp_query_sale_dashboard( $args , $status , $order_status , $sale_id , $daterange_start , $daterange_end );
        if ( count( $Items ) == 0 ) {
            $ret['status'] = false;
            $ret['message'] = esc_html__('ไม่พบข้อมูล', 'dropship');
            return dsp_response_json( $ret );
        }
        $columns = Dropship_Functions::get_instance()->dsp_wp_column_sale_dashboard( true );
        $records = [];
        foreach ($Items as $post_id) {
            $arr = [];
            $item = wc_get_order($post_id);
            $order_items = $item->get_items();
            foreach ($columns as $column_name => $text) {
                $arr[$column_name] = html_entity_decode(strip_tags( Dropship_Functions::get_instance()->dsp_wp_records_table_data_dashboard( $item->get_data() , $column_name , $order_items , true ) ) );
            }

            $records[] = $arr;
        }

        $file_name = "order-". time();
        // $link = dsp_create_csv_file($file_name , $columns , $records , 'dropship_report');
        $link = dsp_create_excel_file($file_name , $columns , $records , 'dropship_report');
        $ret['link'] = $link;
        $ret['file_name'] = $file_name;

        return dsp_response_json($ret);
    }

    function dsp_sale_report_dashboard_export() {
        $ret = ['status' => true];

        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] === 'dropship-report-brand') ? 'brand' : 'supplier';

		$order_status = ( isset( $_REQUEST['order_status'] ) ) ? sanitize_text_field( $_REQUEST['order_status'] ) : '';
		$supplier  = ( isset( $_REQUEST['supplier'] ) ) ? sanitize_text_field( $_REQUEST['supplier'] ) : '';
		$brand  = ( isset( $_REQUEST['brand'] ) ) ? sanitize_text_field( $_REQUEST['brand'] ) : '';

		$daterange_start = ( isset( $_REQUEST['daterange_start'] ) ) ? sanitize_text_field( $_REQUEST['daterange_start'] ) : false;
		$daterange_end   = ( isset( $_REQUEST['daterange_end'] ) ) ? sanitize_text_field( $_REQUEST['daterange_end'] ) : false;

		if ( ${$current_page} == '' ) {
			$terms = get_terms([
				'taxonomy' => $current_page,
				'hide_empty' => false,
			]);
	
			if ( !empty($terms) ) {
				$terms = end($terms);
				${$current_page} = $terms->term_id;
			}
		}

		$args = [];
		$product_ids = dsp_get_taxonomy_data( $current_page , ${$current_page} );
		$Items = Dropship_Functions::get_instance()->dsp_get_orders_ids_by_product_id( $args , $product_ids , $order_status , $daterange_start , $daterange_end );
        if ( count( $Items ) == 0 ) {
            $ret['status'] = false;
            $ret['message'] = esc_html__('ไม่พบข้อมูล', 'dropship');
            return dsp_response_json( $ret );
        }
        $columns = Dropship_Functions::get_instance()->dsp_wp_column_report_dashboard( true );
        $records = [];
        foreach ($Items as $post_id) {
            $arr = [];
            $item = wc_get_order($post_id);
            $order_items = $item->get_items();
            foreach ($columns as $column_name => $text) {
                $arr[$column_name] = html_entity_decode(strip_tags( Dropship_Functions::get_instance()->dsp_wp_records_table_data_dashboard( $item->get_data() , $column_name , $order_items , true ) ) );
            }

            $records[] = $arr;
        }

        $file_name = "report-" . $current_page . "-" . time();
        // $link = dsp_create_csv_file($file_name , $columns , $records , 'dropship_report');
        $link = dsp_create_excel_file($file_name , $columns , $records , 'dropship_report');
        $ret['link'] = $link;
        $ret['file_name'] = $file_name;

        return dsp_response_json($ret);
    }

    function dsp_add_product_catalog() {
        $ret = ['status' => true];
        $product_id = $_POST['product_id'];
        $remove = $_POST['remove'];
        if ( !is_numeric( $product_id ) ) {
            $ret = ['status' => false , 'msg' => esc_html__('ผิดพลาด', 'dropship')];
        } else {
            if ( $remove === "Y" ) {
                $result = Dropship_Model_Products::get_instance()->dsp_delete_sale_products_catalog( get_current_user_id() , $product_id );
                $ret['txt'] = __('เพิ่มสินค้า', 'dropship');
            } else {
                $result = Dropship_Model_Products::get_instance()->dsp_insert_sale_products_catalog( get_current_user_id() , $product_id );
                $ret['txt'] = __('ลบสินค้า', 'dropship');
            }

            if ( $result ) {
                $ret['msg'] = esc_html__('สำเร็จ', 'dropship');
            } else {
                $ret = ['status' => false , 'msg' => esc_html__('ผิดพลาด', 'dropship')];
            }
        }

        return dsp_response_json( $ret );
    }

    function dsp_get_product_catalog()
    {
        $ret = ['status' => true];
        $items_per_page = 18;
        $page = (isset($_POST['page']) && $_POST['page'] > 0) ? $_POST['page'] : 1;
        $self = (isset($_POST['self'])) ? $_POST['self'] : false;
        $order = (isset($_POST['order'])) ? $_POST['order'] : 1;
        $categories = (isset($_POST['categories'])) ? $_POST['categories'] : 0;

        $result = Dropship_Model_Products::get_instance()->dsp_get_post_join_sale_products_catalog( get_current_user_id() , $categories , $page , $items_per_page , $self , $order);
        $args = $result['result'];
        $total = $result['total'];

        $ret['page'] = $page;
        $ret['total'] = $total;
        $ret['data'] = $args;
        $ret['html'] = $this->dsp_product_catalog_card_html( $args , $page , $total , $items_per_page , $categories );
        return dsp_response_json( $ret );
    }

    private function dsp_product_catalog_card_html ( $args , $page , $total , $items_per_page , $categories = [] ) {
        $isIP = @($_SERVER['SERVER_ADDR'] === trim($_SERVER['HTTP_HOST'], '[]'));
        $_args = array_chunk( $args , 5 );
        ob_start();
        $catName = [];
        foreach ($categories as $cid) {
            $catName[] = get_term( $cid )->name;
        }
        ?>

        <div class="pure-g">
            <div class="pure-u-sm-1 ds-select-all-product">
                <h3> <?php echo esc_html( sprintf("จำนวนสินค้าทั้งหมด %s รายการ", $total ) ); ?> </h3>
                <?php if ( $catName ) : ?>
                    <h4> <?php echo esc_html( sprintf("หมวดหมู่ : %s", implode( "," , $catName ) ) ); ?> </h3>
                <?php endif; ?>
            </div>
        </div>

        <div class="ds-cat-row">
            <div class="ds-cat-wrap">
                <div class="pure-g">
        <?php
        foreach ($_args as $_arg) : 
        ?>
                 <!-- <div class="pure-g"> -->
                    <?php foreach ($_arg as $res) : ?>
                        <?php
                            $product = wc_get_product( $res['ID'] );
                            $commission = ( !empty($res['com_value']) ) ? $res['com_value'] : 0;
                            $exist = ( isset( $res['cid'] ) && !empty($res['cid']) ) ? true : false;
                        ?>
                        <div class="pure-u-sm-1 pure-u-md-1-4 pure-u-xl-1-5 ds-sale-cats">
                            <div class="pd-cat-card">
                                <div class="ds-block-image">
                                    <img src="<?php echo esc_url(wp_get_attachment_thumb_url( $product->get_image_id() )); ?>" alt="<?php echo esc_attr($product->get_name()); ?>" style="width:100%">
                                </div>
                                <div class="ds-block-description">
                                    <h3><a href="<?php echo esc_url(dsp_concat_ref_id(get_permalink($product->get_id()))); ?>" target="_blank"> <?php echo esc_html($product->get_name()); ?></a></h3>
                                    <!-- <p class="ds-price-cat"><?php //echo esc_html__('ราคาขาย', 'dropship') . " : " . (wc_price( $product->get_price() )); ?></p> 
                                    <p class="ds-com-cat"><?php //echo esc_html__('คอมมิชชั่น(%)', 'dropship') . " : " . esc_html($commission); ?> % </p> -->
                                    <p class="ds-price-cat"><?php echo wc_price($product->get_price()); ?></p>
                                    <p class="ds-com-cat"><?php echo esc_html__('คอมมิชชั่น', 'dropship') . " : " . esc_html($commission); ?> % </p>

                                    <?php if ( $isIP ) : ?>
                                        <?php if ( $exist ) : ?> 
                                            <p><button class="add-product-to-catalog button button-secondary in-catalog ds-button-d" data-product-id="<?php echo esc_attr($product->get_id()); ?>"><?php echo esc_html__('ลบสินค้า', 'dropship'); ?></button></p>
                                        <?php else : ?>
                                            <p><button class="add-product-to-catalog button button-primary ds-button-a" data-product-id="<?php echo esc_attr($product->get_id()); ?>"><?php echo esc_html__('เพิ่มสินค้า', 'dropship'); ?></button></p>
                                        <?php endif; ?>
                                    <?php else : ?>
                                    <div class="pure-g">
                                        <div class="pure-u-2-3 ds-pure-flex">
                                            <?php if ( $exist ) : ?>
                                                <button class="add-product-to-catalog button button-secondary in-catalog ds-button-d" data-product-id="<?php echo esc_attr($product->get_id()); ?>">
                                                    <?php echo esc_html__('ลบสินค้า', 'dropship'); ?>
                                                </button>
                                            <?php else : ?>
                                                <button class="add-product-to-catalog button button-primary ds-button-a" data-product-id="<?php echo esc_attr($product->get_id()); ?>">
                                                    <?php echo esc_html__('เพิ่มสินค้า', 'dropship'); ?>
                                                </button>
                                            <?php endif; ?>
                                        </div>
                                        <div class="pure-u-1-3 ds-pure-flex">
                                            <button class="copy-product-to-catalog-url button button-secondary ds-button-copy" data-id="<?php echo esc_attr(md5( $product->get_id() )); ?>" data-product-id="<?php echo esc_attr($product->get_id()); ?>">
                                                <?php //echo esc_html__('คัดลอก', 'dropship'); ?>
                                                <img src="/wp-content/themes/dropship/assets/copy-b.png">
                                            </button>
                                        </div>
                                        <div class="pure-u-1-1">
                                        <input type="text" class="ds-input-link" id="<?php echo esc_attr(md5( $product->get_id() )); ?>" value="<?php echo esc_url(dsp_concat_ref_id(get_permalink($product->get_id()))); ?>" readonly>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <!-- </div> -->
        <?php
        endforeach;
        ?>
                </div>
            </div>
        </div>

        <?php if ( empty($args) ) : ?>
            <div class="pure-u-sm-1">
                <h3><?php echo esc_html__( "ไม่พบข้อมูล", 'dropship' ); ?></h3>
            </div>
        <?php endif; ?>
        <br>
        <?php if ( !empty($args) ) : ?>
        <!-- <div class="pure-g" style="float: right;"> -->
        <div class="pure-g" style="float: right;">
            <div class="pure-u-1 ds-next-page">
                <input type="hidden" value="<?php echo esc_attr($page); ?>" class="dsp-product-to-catalog-cpage">
                <button type="button" data-page-id="<?php echo esc_attr(($page - 1)); ?>" <?php echo esc_html(($page == 1) ? "disabled" : ""); ?> class="pure-button button-primary dsp-btn-product-to-catalog-page ds-button-d"><?php echo esc_html__('ก่อนหน้า', 'dropship'); ?></button>
                <button type="button" data-page-id="<?php echo esc_attr(($page + 1)); ?>" <?php echo esc_html((ceil($total / $items_per_page) == $page) ? "disabled" : ""); ?> class="pure-button button-primary dsp-btn-product-to-catalog-page ds-button-a"><?php echo esc_html__('ถัดไป', 'dropship'); ?></button>
            </div>
        </div>
        <?php endif; ?>
        <?php
        $html = ob_get_clean();

        return $html;
    }

    function dsp_update_banners_catalog_sale() {
        $order = (isset($_POST['order']) && $_POST['order'] >= 0) ? $_POST['order'] : 0;
        $image_id = (isset($_POST['image_id']) && $_POST['image_id'] > 0) ? $_POST['image_id'] : 0;
        $users_banners = get_user_meta( get_current_user_id() , '_dsp_sale_banners' , true );
        if ( empty($users_banners) ) {
            $users_banners = [
                0 , 0 , 0
            ];
        }

        $users_banners[ $order ] = $image_id;

        update_user_meta( get_current_user_id() , '_dsp_sale_banners' , $users_banners );
        return dsp_response_json( ['status' => true] );
    }
}

return new Dropship_Rest_API_Admin();
