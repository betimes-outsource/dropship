<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.facebook.com/makkungs/
 * @since      1.0.0
 *
 * @package    Dropship
 * @subpackage Dropship/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Dropship
 * @subpackage Dropship/includes
 * @author     Mc <natdanai.opt@gmail.com>
 */
class Dropship_Activator
{

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate()
	{
		add_role(
			DROPSHIP_ROLE_SALE,
			__('Dropship Sale', 'dropship'),
			array(
				'read' => true,
				'edit_posts'   => true,
				'delete_posts' => false,
				'upload_files' => true,
			)
		);

		add_role(
			DROPSHIP_ROLE_HEADSALE,
			__('Dropship Head Sale', 'dropship'),
			array(
				'read' => true,
				'edit_posts'   => true,
				'delete_posts' => true,
				'upload_files' => true,
				'manage_options' => true,

				'read_shop_order' => true,
				'edit_shop_orders' => true,
				'edit_shop_order' => true,
				'edit_others_shop_orders' => true,

				'read_product' => true,
				'edit_products' => true,
				'edit_product' => true,
				'delete_product' => true,
				'delete_products' => true,
				'delete_published_products' => true,
				'edit_others_products' => true,
				'edit_private_products' => true,
				'edit_product_terms' => true,
				'edit_published_products' => true,
				'manage_product_terms' => true,
				'publish_products' => true,
				'read_private_products' => true,

				'assign_product_terms' => true,
				'delete_product_terms' => true,

				'manage_woocommerce' => true,
				'view_woocommerce_reports' => true,

				'read_shop_coupon' => true,
				'read_private_shop_coupons' => true,
				'publish_shop_coupons' => true,
				'manage_shop_coupon_terms' => true,
				'edit_shop_coupons' => true,
				'edit_shop_coupon_terms' => true,
				'edit_shop_coupon_terms' => true,
				'edit_shop_coupon' => true,
				'edit_published_shop_coupons' => true,
				'edit_private_shop_coupons' => true,
				'edit_others_shop_coupons' => true,
				'delete_shop_coupons' => true,
				'delete_shop_coupon_terms' => true,
				'delete_shop_coupon' => true,
				'delete_published_shop_coupons' => true,
				'delete_private_shop_coupons' => true,
				'delete_others_shop_coupons' => true,
				'assign_shop_coupon_terms' => true,

				'manage_brand' => true,
				'manage_supplier' => true,

				'create_users' => true,
				'delete_users' => true,
				'edit_users' => true,
				'list_users' => true,
				'promote_users' => true,
				'remove_users' => true,
			)
		);

		add_role(
			DROPSHIP_ROLE_OPERATION,
			__('Dropship Operation', 'dropship'),
			array(
				// 'read' => true,
				// 'edit_posts'   => true,
				// 'delete_posts' => true,

				// 'read_shop_order' => true,
				// 'edit_shop_orders' => true,
				// 'edit_shop_order' => true,
				// 'edit_others_shop_orders' => true,

				// 'read_product' => true,
				// 'edit_products' => true,
				// 'edit_product' => true,
				// 'edit_others_products' => true,
				// 'edit_private_products' => true,
				// 'edit_product_terms' => true,
				// 'edit_published_products' => true,
				// 'manage_product_terms' => true,
				// 'publish_products' => true,
				// 'read_private_products' => true,

				// 'manage_brand' => true,
				// 'manage_supplier' => true,
				'read' => true,
				'edit_posts'   => true,
				'delete_posts' => true,
				'upload_files' => true,

				'read_shop_order' => true,
				'edit_shop_orders' => true,
				'edit_shop_order' => true,
				'edit_others_shop_orders' => true,

				'read_product' => true,
				'edit_products' => true,
				'edit_product' => true,
				'delete_product' => true,
				'delete_products' => true,
				'delete_published_products' => true,
				'edit_others_products' => true,
				'edit_private_products' => true,
				'edit_product_terms' => true,
				'edit_published_products' => true,
				'manage_product_terms' => true,
				'publish_products' => true,
				'read_private_products' => true,

				'assign_product_terms' => true,
				'delete_product_terms' => true,

				'manage_woocommerce' => true,
				'view_woocommerce_reports' => true,

				'read_shop_coupon' => true,
				'read_private_shop_coupons' => true,
				'publish_shop_coupons' => true,
				'manage_shop_coupon_terms' => true,
				'edit_shop_coupons' => true,
				'edit_shop_coupon_terms' => true,
				'edit_shop_coupon_terms' => true,
				'edit_shop_coupon' => true,
				'edit_published_shop_coupons' => true,
				'edit_private_shop_coupons' => true,
				'edit_others_shop_coupons' => true,
				'delete_shop_coupons' => true,
				'delete_shop_coupon_terms' => true,
				'delete_shop_coupon' => true,
				'delete_published_shop_coupons' => true,
				'delete_private_shop_coupons' => true,
				'delete_others_shop_coupons' => true,
				'assign_shop_coupon_terms' => true,

				'manage_brand' => true,
				'manage_supplier' => true,

				'create_users' => true,
				'delete_users' => true,
				'edit_users' => true,
				'list_users' => true,
				'promote_users' => true,
				'remove_users' => true,
			)
		);

		$role = get_role( 'administrator' );
		$role->add_cap( 'manage_brand' );
		$role->add_cap( 'manage_supplier' );

		self::dsp_create_table_log_order_commission();
		self::dsp_create_table_sale_commission();
		self::dsp_create_table_sale_producuts_commission();
		self::dsp_create_table_sale_producuts_catalog();
	}

	private static function dsp_create_table_sale_producuts_commission() {
		global $table_prefix, $wpdb;
		$table_name = $table_prefix . DROPSHIP_TABLE_SALE_PRODUCTS_COMMISSION;
	
		#Check to see if the table exists already, if not, then create it
		if($wpdb->get_var( "show tables like '$table_name'" ) != $table_name) {
			$sql = "CREATE TABLE `". $table_name . "` ( ";
			$sql .= " `id` INT NOT NULL AUTO_INCREMENT, ";
			$sql .= " `product_id` INT NOT NULL, ";
			$sql .= " `sale_id` INT NOT NULL, ";
			$sql .= " `com_value` DECIMAL(10,1) NOT NULL, ";
			$sql .= " `logs` VARCHAR(200) NULL, ";
			$sql .= " `create_user_id` INT NOT NULL, ";
			$sql .= " `update_user_id` INT NOT NULL, ";
			$sql .= " `create_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, ";
			$sql .= " `update_datetime` DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, ";
			$sql .= " PRIMARY KEY (`id`) "; 
			$sql .= ") ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ; ";
			require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
			dbDelta($sql);
		}
	}

	private static function dsp_create_table_sale_producuts_catalog() {
		global $table_prefix, $wpdb;
		$table_name = $table_prefix . DROPSHIP_TABLE_SALE_PRODUCTS_CATALOG;
	
		#Check to see if the table exists already, if not, then create it
		if($wpdb->get_var( "show tables like '$table_name'" ) != $table_name) {
			$sql = "CREATE TABLE `". $table_name . "` ( ";
			$sql .= " `id` INT NOT NULL AUTO_INCREMENT, ";
			$sql .= " `product_id` INT NOT NULL, ";
			$sql .= " `sale_id` INT NOT NULL, ";
			$sql .= " PRIMARY KEY (`id`) , UNIQUE `uniue_pd_sl_id` (`product_id`, `sale_id`) "; 
			$sql .= ") ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ; ";
			require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
			dbDelta($sql);
		}
	}

	private static function dsp_create_table_log_order_commission() {
		global $table_prefix, $wpdb;
		$table_name = $table_prefix . DROPSHIP_TABLE_LOG_ORDER_COMMISSION;
	
		#Check to see if the table exists already, if not, then create it
		if($wpdb->get_var( "show tables like '$table_name'" ) != $table_name) {
			$sql = "CREATE TABLE `". $table_name . "` ( ";
			$sql .= " `id` INT NOT NULL AUTO_INCREMENT, ";
			$sql .= " `order_id` INT NOT NULL, ";
			$sql .= " `com_type` VARCHAR(10) NOT NULL, ";
			$sql .= " `com_value` DECIMAL(10,1) NOT NULL, ";
			$sql .= " `logs` VARCHAR(200) NULL, ";
			$sql .= " PRIMARY KEY (`id`) "; 
			$sql .= ") ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ; ";
			require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
			dbDelta($sql);
		}
	}

	private static function dsp_create_table_sale_commission() {
		global $table_prefix, $wpdb;
		$table_name = $table_prefix . DROPSHIP_TABLE_SALE_COMMISSION;
	
		#Check to see if the table exists already, if not, then create it
		if($wpdb->get_var( "show tables like '$table_name'" ) != $table_name) {
			$sql = "CREATE TABLE `". $table_name . "` ( ";
			$sql .= " `id` INT NOT NULL AUTO_INCREMENT, ";
			$sql .= " `sale_id` INT NOT NULL, ";
			$sql .= " `com_value` DECIMAL(10,1) NOT NULL, ";
			$sql .= " `create_user_id` INT NOT NULL, ";
			$sql .= " `update_user_id` INT NOT NULL, ";
			$sql .= " `create_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, ";
			$sql .= " `update_datetime` DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, ";
			$sql .= " `logs` VARCHAR(200) NULL, ";
			$sql .= " PRIMARY KEY (`id`) "; 
			$sql .= ") ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ; ";
			require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
			dbDelta($sql);
		}
	}
}
