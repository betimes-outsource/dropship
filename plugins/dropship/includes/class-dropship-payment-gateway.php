<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

/**
 * Custom Payment Gateway.
 *
 * Provides a Custom Payment Gateway, mainly for testing purposes.
 */
add_action('plugins_loaded', 'init_dropship_gateway_class');
function init_dropship_gateway_class()
{
    class Dropship_Payment_Gateway extends WC_Payment_Gateway
    {
        /**
         * Constructor for the gateway.
         */
        public function __construct()
        {
            $this->domain = 'qrcode_promtpay';

            $this->id                 = 'qrcode_promtpay';
            $this->icon               = apply_filters('woocommerce_custom_gateway_icon', '');
            $this->has_fields         = false;
            $this->method_title       = __('Qrcode Promtpay');
            $this->method_description = __('Qrcode Promtpay');

            // Load the settings.
            $this->init_form_fields();
            $this->init_settings();

            // Define user set variables
            $this->title        = $this->get_option('title');
            $this->description  = $this->get_option('description');
            $this->instructions = $this->get_option('instructions', $this->description);
            $this->order_status = $this->get_option('order_status', 'completed');

            // Actions
            add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
            // add_action('woocommerce_thankyou_' . $this->id, array($this, 'thankyou_page'));
        }

        /**
         * Initialise Gateway Settings Form Fields.
         */
        public function init_form_fields()
        {

            $this->form_fields = array(
                'enabled' => array(
                    'title'   => __('Enable/Disable', 'dropship'),
                    'type'    => 'checkbox',
                    'label'   => __('Enable Qrcode Promtpay', 'dropship'),
                    'default' => 'yes'
                ),
                'title' => array(
                    'title'       => __('Title', 'dropship'),
                    'type'        => 'text',
                    'description' => __('This controls the title which the user sees during checkout.', 'dropship'),
                    'default'     => __('Qrcode Promtpay', 'dropship'),
                    'desc_tip'    => true,
                ),
                'description' => array(
                    'title'       => __('Description', 'dropship'),
                    'type'        => 'textarea',
                    'description' => __('Payment method description that the customer will see on your checkout.', 'dropship'),
                    'default'     => __('Payment Information', 'dropship'),
                    'desc_tip'    => true,
                ),
                'instructions' => array(
                    'title'       => __('Instructions', 'dropship'),
                    'type'        => 'textarea',
                    'description' => __('Instructions that will be added to the thank you page and emails.', 'dropship'),
                    'default'     => '',
                    'desc_tip'    => true,
                ),
            );
        }

        // /**
        //  * Output for the order received page.
        //  */
        // public function thankyou_page()
        // {
        //     if ($this->instructions)
        //         echo wpautop(wptexturize($this->instructions));
        // }

        public function payment_fields()
        {
            if ($description = $this->get_description()) {
                echo wpautop(wptexturize($description));
            }

?>
            <div id="custom_input">
                <p class="form-row form-row-wide">
                    <label for="mobile" class=""><?php _e('Mobile Number', 'dropship'); ?></label>
                    <input type="text" class="" name="mobile" id="mobile" placeholder="" value="">
                </p>
                <p class="form-row form-row-wide">
                    <label for="transaction" class=""><?php _e('Transaction ID', 'dropship'); ?></label>
                    <input type="text" class="" name="transaction" id="transaction" placeholder="" value="">
                </p>
            </div>
<?php
        }

        /**
         * Process the payment and return the result.
         *
         * @param int $order_id
         * @return array
         */
        public function process_payment($order_id)
        {

            $order = wc_get_order($order_id);

            $status = 'wc-' === substr($this->order_status, 0, 3) ? substr($this->order_status, 3) : $this->order_status;

            // Set order status
            $order->update_status($status, __('Checkout with custom payment. ', 'dropship'));

            // or call the Payment complete
            // $order->payment_complete();

            // Reduce stock levels
            $order->reduce_order_stock();

            // Remove cart
            WC()->cart->empty_cart();

            // Return thankyou redirect
            return array(
                'result'    => 'success',
                'redirect'  => $this->get_return_url($order)
            );
        }
    }
}

add_filter('woocommerce_payment_gateways', 'add_custom_gateway_class');
function add_custom_gateway_class($methods)
{
    $methods[] = 'Dropship_Payment_Gateway';
    return $methods;
}
