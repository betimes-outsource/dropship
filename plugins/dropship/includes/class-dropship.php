<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://www.facebook.com/makkungs/
 * @since      1.0.0
 *
 * @package    Dropship
 * @subpackage Dropship/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Dropship
 * @subpackage Dropship/includes
 * @author     Mc <natdanai.opt@gmail.com>
 */
class Dropship {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Dropship_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'DROPSHIP_VERSION' ) ) {
			$this->version = DROPSHIP_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'dropship';
		
		$this->define_table_name();
		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		$this->run_utility();
	}

	public function define_table_name() {
		define( 'DROPSHIP_TABLE_LOG_ORDER_COMMISSION' , 'dropship_log_order_commission' );
		define( 'DROPSHIP_TABLE_SALE_COMMISSION' , 'dropship_sale_commission' );
		define( 'DROPSHIP_TABLE_SALE_PRODUCTS_COMMISSION' , 'dropship_sale_products_commission' );
		define( 'DROPSHIP_TABLE_SALE_PRODUCTS_CATALOG' , 'dropship_sale_products_catalog' );
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Dropship_Loader. Orchestrates the hooks of the plugin.
	 * - Dropship_i18n. Defines internationalization functionality.
	 * - Dropship_Admin. Defines all hooks for the admin area.
	 * - Dropship_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {
		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-dropship-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-dropship-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-dropship-admin.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-dropship-admin-menu.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-dropship-public.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'utility.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'functions.php';

		$this->loader = new Dropship_Loader();
	}

	private function run_utility() {
		$plugin_functions = new Dropship_Functions();

		$this->loader->add_action( 'init', $plugin_functions, 'dps_startSession' );

		$this->loader->add_filter( 'woocommerce_product_data_tabs', $plugin_functions, 'dps_product_settings_tabs_commission' );
		$this->loader->add_action( 'woocommerce_product_data_panels', $plugin_functions, 'dsp_commission_product_panels' );
		$this->loader->add_action( 'woocommerce_process_product_meta', $plugin_functions, 'dsp_commission_product_save_fields' , 10, 2 );
	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Dropship_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {
		$plugin_i18n = new Dropship_i18n();
		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );
	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {
		$plugin_admin = new Dropship_Admin( $this->get_plugin_name(), $this->get_version() );

		

		if ( isset($_GET['page']) && strpos( $_GET['page'] , 'dropship' ) !== false ) {
			$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
			$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		}
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts_global' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_admin_style' ); // add custom style for admin page

        // removes admin color scheme options
        remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );
        //Removes the leftover 'Visual Editor', 'Keyboard Shortcuts' and 'Toolbar' options.
        add_action( 'admin_head', function () {
            ob_start( function( $subject ) {
                $subject = preg_replace( '#<h[0-9]>'.__("Personal Options").'</h[0-9]>.+?/table>#s', '', $subject, 1 );
                return $subject;
            });
        });

        add_action( 'admin_footer', function() {
            ob_end_flush();
        });

		$this->loader->add_action( 'admin_bar_menu', $plugin_admin, 'dsp_remove_wp_logo' , 999 );	
		$this->loader->add_action( 'admin_bar_menu', $plugin_admin, 'dsp_add_my_own_logo' , 1 );	

		$this->loader->add_action( 'init', $plugin_admin, 'dsp_register_taxonomy_dropship' );
		$this->loader->add_action( 'init', $plugin_admin, 'dsp_cronjob' );

		$this->loader->add_action( 'add_meta_boxes', $plugin_admin , 'dsp_add_meta_box_in_order' );
		$this->loader->add_action( 'save_post_shop_order', $plugin_admin , 'dsp_save_post_shop_order', 10, 3 );

		// $this->loader->add_action( 'show_user_profile', $plugin_admin, 'dsp_usermeta_form_field_sale_code' ); // sale code
		$this->loader->add_action( 'edit_user_profile', $plugin_admin, 'dsp_usermeta_form_field_sale_code' ); // sale code
		// $this->loader->add_action( 'personal_options_update', $plugin_admin, 'dsp_usermeta_form_field_sale_code_update' ); // sale code
		$this->loader->add_action( 'edit_user_profile_update', $plugin_admin, 'dsp_usermeta_form_field_sale_code_update' ); // sale code

		$this->loader->add_action( 'admin_head-user-new.php', $plugin_admin , 'dsp_remove_contact_info' );
		$this->loader->add_action( 'admin_head-user-edit.php', $plugin_admin , 'dsp_remove_contact_info' );
		$this->loader->add_action( 'admin_head-profile.php', $plugin_admin , 'dsp_remove_contact_info' );

		// $this->loader->add_action( 'login_enqueue_scripts', $plugin_admin , 'dsp_my_login_logo' ); // add logo image
		$this->loader->add_action( 'login_enqueue_scripts', $plugin_admin , 'dsp_header_form' ); // set blank above user form login
		$this->loader->add_action( 'dsp_header_login_form', $plugin_admin, 'dsp_header_login_form' ); // add logo above input field username.

		$this->loader->add_action( 'woocommerce_order_action_send_order_refunded_to_customer_manual', $plugin_admin , 'dsp_woocommerce_order_action_send_order_refunded_to_customer_manual' );

		$this->loader->add_filter( 'woocommerce_csv_product_import_mapping_options' , $plugin_admin , 'dsp_add_column_import_products_woocommerce');
		$this->loader->add_filter( 'woocommerce_csv_product_import_mapping_default_columns' , $plugin_admin , 'dsp_products_woocommerce_column_to_mapping_screen');
		$this->loader->add_filter( 'woocommerce_product_import_pre_insert_product_object' , $plugin_admin , 'dsp_products_woocommerce_process_import' , 10 , 2);
		$this->loader->add_filter( 'wc_order_statuses' , $plugin_admin , 'dsp_remove_wc_order_statuses');

		$this->loader->add_filter( 'woocommerce_order_actions' , $plugin_admin , 'dsp_woocommerce_order_actions' );
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {
		$plugin_public = new Dropship_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

		$this->loader->add_action( 'init', $plugin_public, 'dsp_set_session_ref_id' );
		$this->loader->add_action( 'woocommerce_checkout_update_order_meta', $plugin_public, 'dsp_add_dsp_ref_id_field_after_placed_order' , 10 , 2 );
		$this->loader->add_action( 'dropship_woocommerce_checkout_update_order_meta', $plugin_public, 'dsp_add_dsp_ref_id_field_after_placed_order' , 10 , 2 );

		$this->loader->add_filter( 'the_content' , $plugin_public , 'dsp_change_page_content');
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Dropship_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
