<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.facebook.com/makkungs/
 * @since      1.0.0
 *
 * @package    Dropship
 * @subpackage Dropship/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Dropship
 * @subpackage Dropship/includes
 * @author     Mc <natdanai.opt@gmail.com>
 */
class Dropship_Deactivator {
	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		remove_role( DROPSHIP_ROLE_SALE );
		remove_role( DROPSHIP_ROLE_HEADSALE );
		remove_role( DROPSHIP_ROLE_OPERATION );
	}
}
