<?php

function dsp_shipping_method_init() {
    if ( ! class_exists( 'Dropship_Shipping_Method' ) ) {
        class Dropship_Shipping_Method extends WC_Shipping_Method {
            /**
             * Constructor for your shipping class
             *
             * @access public
             * @return void
             */
            public function __construct( $instance_id = 0 ) {
                $this->id                 = 'dsp_shipping'; // Id for your shipping method. Should be uunique.
                $this->instance_id        = absint( $instance_id );
                $this->method_title       = __( 'รูปแบบขนส่ง' , 'dropship' );  // Title shown in admin
                $this->method_description = __( 'รูปแบบขนส่ง' , 'dropship' ); // Description shown in admin
                $this->supports             = array(
                    'shipping-zones',
                    'instance-settings',
                    'instance-settings-modal',
                );
                // $this->enabled            = "yes"; // This can be added as an setting but for this example its forced enabled
                // $this->title              = "รูปแบบขนส่ง"; // This can be added as an setting but for this example its forced.

                $this->init();

                $this->enabled = isset( $this->settings['enabled'] ) ? $this->settings['enabled'] : 'yes';
                $this->title = isset( $this->settings['title'] ) ? $this->settings['title'] : __( 'การจัดส่งของฉัน' , 'dropship' );
            }

            /**
             * Init your settings
             *
             * @access public
             * @return void
             */
            function init() {
                // Load the settings API
                $this->init_form_fields(); // This is part of the settings API. Override the method to add your own settings
                $this->init_settings(); // This is part of the settings API. Loads settings you previously init.

                // Save settings in admin if you have any defined
                add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
            }

            function init_form_fields()
            {
                $this->form_fields = array(
                'enabled' => array(
                    'title' => __('ใช้งาน' , 'dropship'),
                    'type' => 'checkbox',
                    'default' => 'yes'
                ),
                'title' => array(
                    'title' => __('ชื่อ' , 'dropship'),
                    'type' => 'text',
                    'default' => __('รูปแบบขนส่ง' , 'dropship')
                    ),
                );
            }
           
            /**
             * calculate_shipping function.
             *
             * @access public
             * @param mixed $package
             * @return void
             */
            public function calculate_shipping( $package  = [] ) {
                $alla_items = $package['contents'];
                $shipping_cost = $index = 0;
                foreach ($alla_items as $orderline) {
                    $product_id = $orderline['product_id'];
                    $quantity = $orderline['quantity'];
                    // $cost = get_post_meta($product_id , '_dropship_shipping_cost' , true);
                    // if ( empty($cost) ) {
                    //     $cost = get_option('_dropship_default_shipping_cost' , 0);
                    // }
                    // $shipping_cost += $cost * $quantity;

                    $cost = $cost_next = 0;
                    if ( $index === 0 ) {
                        $cost = get_post_meta($product_id , '_dropship_shipping_cost' , true);
                        if ( empty($cost) ) {
                            $cost = get_option('_dropship_default_shipping_cost' , 0);
                        }
                        if ( !is_numeric( $cost ) ) {
                            $cost = 0;
                        }
                        $shipping_cost += $cost * 1;

                        if ( intval($quantity) > 1 ) {
                            $cost_next = get_post_meta($product_id , '_dropship_shipping_cost_next' , true);
                            if ( !is_numeric( $cost_next ) ) {
                                $cost_next = 0;
                            }
                            $shipping_cost += $cost_next * ( $quantity - 1 );
                        }
                    } else {
                        $cost_next = get_post_meta($product_id , '_dropship_shipping_cost_next' , true);
                        if ( !is_numeric( $cost_next ) ) {
                            $cost_next = 0;
                        }
                        $shipping_cost += $cost_next * $quantity;
                    }

                    $index++;
                };

                $rate = array(
                    'label' => $this->title,
                    'cost' => $shipping_cost,
                    'calc_tax' => 'per_item'
                );

                // Register the rate
                $this->add_rate( $rate );
            }
        }
    }
}

add_action( 'woocommerce_shipping_init', 'dsp_shipping_method_init' );

function drp_shipping_method( $methods ) {
    $methods['dsp_shipping'] = 'Dropship_Shipping_Method';
    return $methods;
}

add_filter( 'woocommerce_shipping_methods', 'drp_shipping_method' );