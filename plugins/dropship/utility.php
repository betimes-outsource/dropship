<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

function dsp_response_json ( $args = [] ) {
    header('Content-type: application/json');
    echo json_encode( $args , JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES );
    die;
}

function dsp_error_log( $function , $data , $msg ) {
    return error_log( "DSP function : " . $function . " | msg : " . $msg . " | data : " . json_encode( $data , JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE ) );
}

function dsp_dd( $val , $die = false ) {
    echo '<pre>';
    print_r( $val );
    echo '</pre>';
    if ($die) {
        die;
    }
}

function dsp_concat_ref_id( $url , $ref_id = '' ) {
    if ( empty($ref_id) ) {
        $ref_id = get_current_user_id();
    }

    $query = parse_url($url, PHP_URL_QUERY);

    // Returns a string if the URL has parameters or NULL if not
    if ($query) {
        $url .= '&dsp_ref_id=' . $ref_id;
    } else {
        $url .= '?dsp_ref_id=' . $ref_id;
    }

    return urldecode($url);
}

function dsp_create_csv_file( $filename , $header = [] , $records = [], $path = "dropship" ) {
    $name_upload =  time() . "_" . str_replace(" ", "" , $filename);
    $url_insert = wp_upload_dir()['basedir'] . "/$path";
    $uploadPath = wp_upload_dir()['baseurl'] . "/$path/" . $name_upload;
    wp_mkdir_p($url_insert);
    $name_insert =  trailingslashit($url_insert) . $name_upload;

    try {
        $fp = fopen($name_insert, 'w');
        fputs( $fp, chr(0xEF) . chr(0xBB) . chr(0xBF) );
        fputcsv($fp, $header);
        foreach ($records as $array) {
            fputcsv($fp, $array);
        }
        fclose($fp);
    } catch (\Throwable $th) {
        return false;
    }

    return $uploadPath;
}

function dsp_create_excel_file( $filename = 'dropship' , $header = [] , $records = [], $path = "dropship" ) {
    $name_upload =  time() . "_" . str_replace(" ", "" , $filename) . ".xlsx";
    $uploadPathDIR = wp_upload_dir()['basedir'] . "/$path";
    $uploadPathURL = wp_upload_dir()['baseurl'] . "/$path/" . $name_upload;
    wp_mkdir_p($uploadPathDIR);
    $fullPathDIR =  trailingslashit($uploadPathDIR) . $name_upload;

    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $_col = range('A', 'Z');
    $i = 0;
    foreach ($header as $val) {
        $sheet->setCellValue( $_col[$i] . '1', $val);
        $i++;
    }

    $start = 2;
    foreach ($records as $val) {
        $i = 0;
        foreach ($val as $v) {
            $sheet->setCellValue( $_col[$i] . "$start", $v);
            $i++;
        }
        $start++;
    }

    foreach ($_col as $letra) {            
        $spreadsheet->getActiveSheet()->getColumnDimension($letra)->setAutoSize(true);
    }

    $writer = new Xlsx($spreadsheet);
    $writer->save( $fullPathDIR );

    return $uploadPathURL;
}

function dsp_import_excel_file_to_array( $file , $name = 'file_upload' , $path = 'dropship_report' ) {
    if ( !isset( $_FILES[$name] ) ) {
        return false;
    }

    if ($_FILES[$name]["error"] > 0) {
        return false;
    }


    if ($file[$name]["name"] != '') {
        $allowed_extension = array('xls', 'csv', 'xlsx');
        $file_array = explode(".", $file[$name]["name"]);
        $file_extension = end($file_array);
    
        if (in_array($file_extension, $allowed_extension)) {
            $name_upload =  time() . '.' . $file_extension;
            $uploadPathDIR = wp_upload_dir()['basedir'] . "/$path";
            wp_mkdir_p($uploadPathDIR);
            $fullPathDIR =  trailingslashit($uploadPathDIR) . $name_upload;
            move_uploaded_file($file[$name]['tmp_name'], $fullPathDIR);
            $file_type = PhpOffice\PhpSpreadsheet\IOFactory::identify($fullPathDIR);
            $reader = PhpOffice\PhpSpreadsheet\IOFactory::createReader($file_type);

            $spreadsheet = $reader->load($fullPathDIR);
            unlink($fullPathDIR);

            $data = $spreadsheet->getActiveSheet()->toArray();
            unset($data[0]); // remove item at index 0
            $data = array_values($data); // 'reindex' array

            return $data;
        }
    }

    return false;
}

function dsp_billing_address_to_str($billing) {
    $state = WC()->countries->get_states( $billing['country'] );
    $addr = [];
    $addr[] = $billing['address_1'];
    $addr[] = $billing['address_2'];
    $addr[] = $billing['city'];
    $addr[] = (isset($state[ $billing['state'] ])) ? $state[ $billing['state'] ] : "-";
    $addr[] = $billing['postcode'];
    
    return implode( " " , $addr );
}

function dsp_get_orders_products_supplier_show( $taxonomy = 'supplier' , $order_id ) {
    $order = wc_get_order( $order_id );
    $order_items = $order->get_items();
    $pd_ids = [];
    foreach ($order_items as $product) {
        $pd = $product->get_data();
        $pd_ids[] = get_the_term_list( $pd['product_id'] , $taxonomy, '', ', ' );
    }

    return implode( " " , $pd_ids );
}

function dsp_generate_dropdown_taxonomy($taxonomy = 'supplier' , $id , $name , $selected = '') {
        $terms = get_terms($taxonomy, array(
                "orderby"    => "count",
                "hide_empty" => false
            )
        );
        ?>
        <select name="<?php echo esc_attr($name); ?>" id="<?php echo esc_attr($id); ?>">
        <?php
            foreach($terms as $term) {
                if ($term->parent) {
                    continue;
                }
                $html = ($selected == $term->term_id) ? esc_attr( 'selected' ) : "";
                echo '<option ' . $html . ' value="' . $term->term_id . '">' . $term->name . '</option>';
            }
        ?>
        </select>
        <?php
}

function dsp_show_metabox_commission_in_posttype( $post_id , $showTitle = true ) {
    $log_id = get_post_meta($post_id , '_dsp_log_order_commission_id' , true);
    if ( empty($log_id) ) {
        return;
    }
    $items = Dropship_Model_log::get_instance()->dsp_get_log_order_commission( $log_id );
    $logs = json_decode( $items['logs'] , true );
    $user = get_user_by('id' , $logs['sale_id']);
    $price = get_post_meta( $post_id , '_dsp_order_paid_commission' , true);
    ?>
    <?php if ( $showTitle ) : ?>
    <h4><?php echo esc_html__("คอมมิชชั่น" , 'dropship'); ?></h4>
    <?php endif; ?>
    <table style="width:100%;text-align: left">
        <tr>
            <th><?php echo esc_html__("ชื่อผู้ขาย" , 'dropship'); ?> : </th>
            <td><?php echo esc_html($user->display_name); ?></td>
        </tr>
        <tr>
            <th><?php echo esc_html__("คอมมิชชั่น (%)" , 'dropship'); ?> : </th>
            <td><?php echo esc_html($logs['com_value']); ?> % </td>
        </tr>
        <tr>
            <th><?php echo esc_html__("รวมทั้งหมด" , 'dropship'); ?> : </th>
            <td><?php echo (wc_price( $price )); ?></td>
        </tr>
    </table>
    <?php
}

function dsp_get_taxonomy_data ( $taxonomy = 'supplier' , $terms = 20 )  {
    $args = array(
        'post_type' => 'product',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'fields' => 'ids',
        'tax_query' => array(
            array(
                'taxonomy' => $taxonomy,
                'field' => 'id',
                'terms' => $terms
            )
        )
    );
    $the_query = new WP_Query( $args );
    return $the_query->posts;
}