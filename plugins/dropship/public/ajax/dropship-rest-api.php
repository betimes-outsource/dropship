<?php

class Dropship_Rest_API_Public
{
    function __construct()
    {
        add_action('wp_ajax_dsp_add_product_to_cart', [$this, 'dsp_add_product_to_cart']);
        add_action('wp_ajax_nopriv_dsp_add_product_to_cart', [$this, 'dsp_add_product_to_cart']);

        // add_action('wp_ajax_dsp_remove_product_from_cart', [$this, 'dsp_remove_product_from_cart']);
        // add_action('wp_ajax_nopriv_dsp_remove_product_from_cart', [$this, 'dsp_remove_product_from_cart']);

        add_action('wp_ajax_dsp_place_order', [$this, 'dsp_place_order']);
        add_action('wp_ajax_nopriv_dsp_place_order', [$this, 'dsp_place_order']);
    }

    public function dsp_place_order() {
        $required_field = [
            'action' => true,
            'payment_method' => true,
            'address_name' => true,
            'address_tel' => true,
            'address_address' => true,
            'address_email' => false,
            'address_province' => true,
            'address_district' => true,
            'address_subdistrict' => true,
            'address_zipcode' => true
        ];

        $valid_break = [
            'address_province' => ["or" => "=" , "compare" => 0],
            'address_district' => ["or" => ">" , "compare" => 0],
            'address_subdistrict' => ["or" => ">" , "compare" => 0],
        ];

        if ( empty( WC()->cart->get_cart() ) ) {
            return dsp_response_json(['status' => false , 'msg' => esc_html__('กรุณาเลือกสินค้า' , 'dropship'), 'name' => 'product-section' , 'post' => $_POST , 'state' => WC()->countries->get_states( "TH" ) ]);
        }

        foreach ($_POST as $key => $value) {
            if ( empty( $_POST[$key] ) && $required_field[$key] ) {
                return dsp_response_json(['status' => false , 'msg' => esc_html__('Required' , 'dropship'), 'name' => $key , 'post' => $_POST]);
            }

            if ( isset( $valid_break[$key] ) ) {
                if ( $valid_break[$key]['or'] == "=" ) {
                    if ( $_POST[$key] === $valid_break[$key]['compare'] ) {
                        return dsp_response_json(['status' => false , 'msg' => esc_html__('Required' , 'dropship'), 'name' => $key , 'post' => $_POST]);
                    }
                }
            }
        }

        $uploadPath = "";
        if ( !file_exists($_FILES['upload_slip']['tmp_name']) || !is_uploaded_file($_FILES['upload_slip']['tmp_name']) ) {
            return dsp_response_json(['status' => false , 'msg' => 'Required', 'name' => 'upload_slip' ]);
        } else {
            foreach ($_FILES as $file) :
                if ($file['error'] == UPLOAD_ERR_NO_FILE) :
                    continue;
                endif;
        
                $valid_ext = array( 'jpg' , 'jpeg' , 'png');
                $extension_upload = strtolower(  substr(  strrchr($file['name'], '.')  ,1)  );
                if ( in_array($extension_upload , $valid_ext) ) {
                    $name_upload = uniqid();
                    $url_insert = wp_upload_dir()['basedir'] . "/dropship_slip";
                    $uploadPath = wp_upload_dir()['baseurl'] . "/dropship_slip/" . $name_upload;
                    wp_mkdir_p($url_insert);
                    $name_insert =  trailingslashit($url_insert) . $name_upload;
                    $action = move_uploaded_file($file['tmp_name'],$name_insert);
                } else {
                    return dsp_response_json(['status' => false , 'msg' => esc_html__('File type incorrect' , 'dropship') ]);
                }
            endforeach;

            if ( $action === false ) {
                return dsp_response_json(['status' => false , 'msg' => esc_html__('นำเข้าข้อมูลล้มเหลว!' , 'dropship') ]);
            }
        }

        $address = array(
            'first_name' => $_POST['address_name'],
            'last_name'  => '',
            'company'    => '',
            'email'      => $_POST['address_email'],
            'phone'      => $_POST['address_tel'],
            'address_1'  => $_POST['address_address'],
            'address_2'  => $_POST['address_subdistrict'], 
            'city'       => $_POST['address_district'],
            'state'      => $_POST['address_province'],
            'postcode'   => $_POST['address_zipcode'],
            'country'    => 'TH'
        );

        $order = wc_create_order();
        // $imported_total_fee = 0;
        $main_product_id = 0;
        foreach ( WC()->cart->get_cart() as $cartItemKey => $cart_item ) {
            if ( $cart_item['variation_id'] > 0 ) {
                $product = new WC_Product_Variation( $cart_item['variation_id'] );
                $main_product_id = $product->get_parent_id();
            } else {
                $product = new WC_Product( $cart_item['product_id'] );
                $main_product_id = $product->get_id();
            }
            $order->add_product( $product , $cart_item['quantity'] );
        }
        $shipping_fee = [
            'product_id' => $main_product_id,
            'cost' => get_post_meta($main_product_id , '_dropship_shipping_cost' , true),
            'cost_next' => get_post_meta($main_product_id , '_dropship_shipping_cost_next' , true),
        ];
        $order->calculate_totals();
        $order->set_address( $address, 'billing' );
        $order->set_address( $address, 'shipping' );
        if ( $uploadPath != "" ) {
            $order->add_meta_data('_dsp_slip_path' , $uploadPath);
        }
        // $order->add_coupon('Fresher','10','2'); // accepted param $couponcode, $couponamount,$coupon_tax

        $gateways = WC()->payment_gateways->payment_gateways();
        $order->set_payment_method( $gateways[ $_POST['payment_method'] ] );
        // $order->set_shipping_total( WC()->cart->get_shipping_total() );
        // error_log( WC()->cart->get_shipping_total() );
        // error_log( json_encode( $order->get_shipping_methods() ) );
        // $order->legacy_set_total( WC()->cart->get_shipping_total() , 'shipping' );

        // Get the customer country code
        $country_code = $order->get_shipping_country();

        // Set the array for tax calculations
        $calculate_tax_for = array(
            'country' => $country_code,
            'state' => '', // Can be set (optional)
            'postcode' => '', // Can be set (optional)
            'city' => '', // Can be set (optional)
        );

        // Optionally, set a total shipping amount
        $new_ship_price = WC()->cart->get_shipping_total();

        // Get a new instance of the WC_Order_Item_Shipping Object
        $item = new WC_Order_Item_Shipping();
        $method_rate_id =  WC()->session->get( 'chosen_shipping_methods' )[0];

        $item->set_method_title( "Dropship Shipping" );
        $item->set_method_id( $method_rate_id ); // set an existing Shipping method rate ID
        $item->set_total( $new_ship_price ); // (optional)
        $item->calculate_taxes($calculate_tax_for);

        $order->add_item( $item );
        $order->calculate_totals();

        // Set the array for tax calculations
        // $calculate_tax_for = array(
        //     'country' => $country_code, 
        //     'state' => '', 
        //     'postcode' => '', 
        //     'city' => ''
        // );

        // Get a new instance of the WC_Order_Item_Fee Object
        // $item_fee = new WC_Order_Item_Fee();

        // $item_fee->set_name( "Fee" ); // Generic fee name
        // $item_fee->set_amount( $imported_total_fee ); // Fee amount
        // $item_fee->set_tax_class( '' ); // default for ''
        // $item_fee->set_tax_status( 'taxable' ); // or 'none'
        // $item_fee->set_total( $imported_total_fee ); // Fee amount

        // // Calculating Fee taxes
        // $item_fee->calculate_taxes( $calculate_tax_for );

        // // Add Fee item to the order
        // $order->add_item( $item_fee );

        // $shipping_tax = array(); 
        // $shipping_rate = new WC_Shipping_Rate(  , '' , WC()->cart->get_cart_shipping_total() , $shipping_tax);
        // $order->add_shipping($shipping_rate);

        // Set calculated totals
        $order->calculate_totals();
        $order_status = [
            'status' => 'on-hold',
            'note'   => 'change status to on-hold',
        ];
        $order->update_status($order_status['status'], $order_status['note']);
        $order_id = 0;
        $msg = "";
        try {
            $order_id = $order->save();
        } catch (\Throwable $th) {
            $msg = $th->getMessage();
            error_log( $msg );
        }

        if ( is_numeric( $order_id ) && $order_id > 0 ) {
            WC()->cart->empty_cart();
            WC()->session->set( 'order_id', $order_id );
            update_post_meta( $order_id , '_dsp_paid_success' , 'no');
            update_post_meta( $order_id , '_dsp_shipping_fee_log' , $shipping_fee );
            
            do_action( 'dropship_woocommerce_checkout_update_order_meta', $order_id, $order );
            do_action( 'woocommerce_checkout_update_order_meta', $order_id, $order );

            setcookie( 'dsp_customer_cookie_name',  $_POST['address_name'] , time() + 31556926 , '/', COOKIE_DOMAIN);
            setcookie( 'dsp_customer_cookie_tel', $_POST['address_tel'] , time() + 31556926 , '/', COOKIE_DOMAIN);
            setcookie( 'dsp_customer_cookie_email', $_POST['address_email'] , time() + 31556926 , '/', COOKIE_DOMAIN);
            setcookie( 'dsp_customer_cookie_address', $_POST['address_address'] , time() + 31556926 , '/', COOKIE_DOMAIN);
            setcookie( 'dsp_customer_cookie_address_province', $_POST['address_province'] , time() + 31556926 , '/', COOKIE_DOMAIN);
            setcookie( 'dsp_customer_cookie_address_district', $_POST['address_district'] , time() + 31556926 , '/', COOKIE_DOMAIN);
            setcookie( 'dsp_customer_cookie_address_subdistrict', $_POST['address_subdistrict'] , time() + 31556926 , '/', COOKIE_DOMAIN);
            setcookie( 'dsp_customer_cookie_address_zipcode', $_POST['address_zipcode'] , time() + 31556926 , '/', COOKIE_DOMAIN);

            $query = get_page_link( get_option('_dropship_default_complete_page', 0 ) );
            $new_query = add_query_arg( array(
                'order_id' => $order_id
            ), $query );

            $ret = [
                'status' => true,
                'redirect' => $new_query,
                'order_id' => $order_id
            ];
        } else {
            $ret = [
                'status' => false,
                'msg' => $msg
            ];
        }

        return dsp_response_json( $ret );
    }

    public function dsp_add_product_to_cart()
    {
        // FIX TO USE CUSTOM SHIPPING METHOD //
        $lang = "TH";
        WC()->customer->set_billing_country( $lang ); // Updated HERE Too
        WC()->customer->set_shipping_country( $lang );
        WC()->session->set('chosen_shipping_methods', array( 'dsp_shipping' ) );
        // FIX TO USE CUSTOM SHIPPING METHOD //

        $product_id     = $_POST['product_id'];
        $variation_id   = $_POST['variation_id'];
        $product_qty    = $_POST['product_qty'];
        $option         = $_POST['option'];

        if ( $option === 'minus' ) {
            $result = false;
            foreach ( WC()->cart->get_cart() as $cartItemKey => $cart_item ) {
                if ( $cart_item['product_id'] == $product_id && $cart_item['variation_id'] == $variation_id ) {
                    if ( $product_qty == 0 ) {
                        WC()->cart->remove_cart_item( $cartItemKey );
                    } else {
                        WC()->cart->set_quantity($cartItemKey, $product_qty);
                    }
                    $result = true;
                }
             }
        } else {
            $result = false;
            $exist = false;
            foreach ( WC()->cart->get_cart() as $cartItemKey => $cart_item ) {
                if ( $cart_item['product_id'] == $product_id && $cart_item['variation_id'] == $variation_id ) {
                    WC()->cart->set_quantity($cartItemKey, $product_qty);
                    $result = $exist = true;
                }
            }

            if ( $exist === false ) {
                $result = WC()->cart->add_to_cart($product_id, $product_qty, $variation_id);
            }
        }

        $name = ( isset($_COOKIE['dsp_customer_cookie_name']) ) ? $_COOKIE['dsp_customer_cookie_name'] : '';
        $tel = ( isset($_COOKIE['dsp_customer_cookie_tel']) ) ? $_COOKIE['dsp_customer_cookie_tel'] : '';
        $email = ( isset($_COOKIE['dsp_customer_cookie_email']) ) ? $_COOKIE['dsp_customer_cookie_email'] : '';
        $address = ( isset($_COOKIE['dsp_customer_cookie_address']) ) ? $_COOKIE['dsp_customer_cookie_address'] : '';

        $ret = [ 'status' => $result, 'html' => $this->dsp_generate_product_from_cart() , 'ship' => WC()->session->get( 'chosen_shipping_methods' ) , 'ref' => (isset($_COOKIE['dsp_ref_id'])) ? $_COOKIE['dsp_ref_id'] : false ];
        return dsp_response_json($ret);
    }

    // function dsp_remove_product_from_cart() {
    //     $cart_key = $_POST['cart_key'];
    //     $result = WC()->cart->remove_cart_item( $cart_key );
    //     if ( $result ) {
    //         $status = true;
    //     } else {
    //         $status = false;
    //     }
    //     $ret = [ 'status' => $status, 'html' => $this->dsp_generate_product_from_cart() , 'cart' => WC()->cart->get_cart() , 'message' => $result ];
    //     return dsp_response_json($ret);
    // }


    private function dsp_generate_product_from_cart() {
        $cart = WC()->cart->get_cart();
        // WC()->cart->set_shipping_total();
        WC()->cart->calculate_shipping();
        $shippingTotal = WC()->cart->get_cart_shipping_total();
        ob_start();
        // $sum = 0;
        $qty = 0;
        if ( !empty($cart) ) :
        ?>
        <table class="table table-hover table-summary">
            <thead class="thead-light">
                <tr class="ds-head-product">
                    <th style="width: 70%;"><?php esc_html_e( 'สินค้า', 'dropship' ); ?></th>
                    <th class="text-center" style="width: 15%;"><?php esc_html_e( 'Amount', 'dropship' ); ?></th>
                    <th class="text-center" style="width: 15%;"><?php esc_html_e( 'ราคา', 'dropship' ); ?></th>
                    <!-- <th class="text-center" style="width: 5%;"><?php //esc_html_e( 'Remove', 'dropship' ); ?></th> -->
                </tr>
            </thead>
            <tbody>
                <?php foreach( $cart as $cart_item ) : ?>
                    <?php 
                        $cart_product_id = $cart_item['product_id'];
                        $cart_variation_id = $cart_item['variation_id'];
                        if ( $cart_variation_id ) {
                            $cart_product_name = get_the_title( $cart_variation_id );
                        } else {
                            $cart_product_name = get_the_title( $cart_product_id );
                        }
                        $cart_product_qty = $cart_item['quantity'];
                        $qty += $cart_product_qty;
                        $cart_line_subtotal = $cart_item['line_subtotal'];
                        // $cart_key = $cart_item['key'];
                        // $sum += $cart_line_subtotal;
                    ?>
                    <tr class="ds-product">
                        <td><?php echo esc_html($cart_product_name); ?></td>
                        <td class="text-center"><?php echo esc_html($cart_product_qty); ?></td>
                        <td class="text-center"><?php echo (wc_price( $cart_line_subtotal )); ?></td>
                        <!-- <td class="text-center">
                            <button type="button" class="btn btn-danger remove-from-cart" data-cart-key="<?php //echo $cart_key; ?>"><?php //esc_html_e( 'Remove', 'dropship' ); ?></button>
                        </td> -->
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot class="border-button-hilight">
                <tr class="ds-product">
                    <td><?php esc_html_e( 'ค่าขนส่ง', 'dropship' ); ?></td>
                    <td class="text-center"><?php echo esc_html($qty); ?></td>
                    <td class="text-center"><?php echo ($shippingTotal); ?></td>
                </tr>
                <tr class="ds-summary">
                    <td class="ds-summary" colspan="2"><?php esc_html_e( 'รวมทั้งหมด', 'dropship' ); ?></td>
                    <td class="text-center ds-summary"><?php echo (WC()->cart->get_total()); ?></td>
                </tr>
            </tfoot>
        </table>
        <?php
        $html = ob_get_clean();
        else :
            $html = "";
        endif;
        return $html;
    }
}

return new Dropship_Rest_API_Public();
