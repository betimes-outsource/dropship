<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://www.facebook.com/makkungs/
 * @since      1.0.0
 *
 * @package    Dropship
 * @subpackage Dropship/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Dropship
 * @subpackage Dropship/public
 * @author     Mc <natdanai.opt@gmail.com>
 */
class Dropship_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {
		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->dsp_require_file();
	}

	private function dsp_require_file()
	{
		require plugin_dir_path(__FILE__) . 'ajax/dropship-rest-api.php';
	}
	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Dropship_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Dropship_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/dropship-public.css', array(), $this->version, 'all' );

		wp_enqueue_style($this->plugin_name . "-public-alertifycss", plugins_url($this->plugin_name) . '/vendor/alertifyjs/css/alertify.min.css', array(), $this->version, 'all');
		wp_enqueue_style($this->plugin_name . "-public-alertifycss-themes", plugins_url($this->plugin_name) . '/vendor/alertifyjs/css/themes/default.min.css', array(), $this->version, 'all');
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Dropship_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Dropship_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/dropship-public.js', array( 'jquery' ), $this->version, false );
		wp_localize_script($this->plugin_name, 'dropshipjs', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) , 'plugin_url' => DROPSHIP_PLUGIN_DIR_URL ) );

		wp_enqueue_script($this->plugin_name . "-public-alertifyjs", plugins_url( $this->plugin_name ) . '/vendor/alertifyjs/alertify.min.js', array('jquery'), $this->version, false);
		wp_enqueue_script($this->plugin_name . "-public-preload", plugins_url( $this->plugin_name ) . '/vendor/preload/loadingoverlay.min.js', array('jquery'), $this->version, false);
	}

	// set cookie ref ID
	public function dsp_set_session_ref_id() {
		if ( isset($_GET['dsp_ref_id']) && $_GET['dsp_ref_id'] > 0 ) {
			setcookie( 'dsp_ref_id',  $_GET['dsp_ref_id'] , time() + 31556926 , '/', COOKIE_DOMAIN);
		}
	}

	public function dsp_add_dsp_ref_id_field_after_placed_order( $order_id, $data ) {
		$dsp_ref_id = false;
		if ( isset($_COOKIE['dsp_ref_id']) ) {
			$dsp_ref_id = $_COOKIE['dsp_ref_id'];
		}
		if ( $dsp_ref_id == false || !is_numeric($dsp_ref_id) ) {
			return;
		}

		$order = wc_get_order( $order_id );
		$total_without_tax_shipping = number_format( (float) $order->get_total() - $order->get_total_tax() - $order->get_shipping_total(), wc_get_price_decimals(), '.', '' );
		$product_id = 0;
		foreach ( $order->get_items() as $item_id => $product_item ) {
			$product_data  = $product_item->get_data();
			$product_id = $product_data['product_id'];
		}

		$result = Dropship_Model_Commission::get_instance()->dsp_get_sale_products_commission( $dsp_ref_id , $product_id );
		if ( !empty($result) ) {
			$com_value = $result[0]['com_value'];
		} else {
			$com_value = get_option('_dropship_commission_rate', 0);
		}

		if ( $com_value > 0 ) {
			$total_commission = ( $total_without_tax_shipping * $com_value ) / 100;
		} else {
			$total_commission = 0;
		}

		$logs = [
			'com_value' => $com_value,
			'product_id' => $product_id,
			'sale_id' => $dsp_ref_id,
			'total_without_tax_shipping' => $total_without_tax_shipping,
			'total_commission' => $total_commission
		];

		$log_id = Dropship_Model_log::get_instance()->dsp_insert_log_order_commission( $order_id , 0 , $com_value , $logs );
		update_post_meta($order_id, '_dsp_ref_id', $dsp_ref_id );
		update_post_meta($order_id, '_dsp_log_order_commission_id', $log_id );
		update_post_meta($order_id, '_dsp_order_paid_commission', $total_commission );
	}

	public function dsp_change_page_content( $content ) {
		global $post;
		if ( is_single() || is_page() ) {
			foreach (DROPSHIP_PAGE_CONTENT as $option => $code) {
				if ( get_option( $option , 0 ) == $post->ID ) {
					if ( !has_shortcode( $content , $code ) ) {
						$content = do_shortcode( "[$code]" );
					}
				}
			}
		}

		return $content;
	}
}
