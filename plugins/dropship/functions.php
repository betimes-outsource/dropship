<?php

class Dropship_Functions
{
    public static $instance;

    public function __construct() {
        self::$instance = $this;
    }

    public static function get_instance() {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    function dps_startSession() {
        if(!session_id()) {
            session_start();
        }
    }

    function dsp_wp_column_sale_dashboard( $export = false ) {
        $admin_columns = [
			'cb' => '<input class="dsp_checkbox_value" type="checkbox" />',
			'sale_id' => __( 'ชื่อผู้ขาย', 'dropship' )
		];
		$columns = array(
			'order'				=> __( 'คำสั่งซื้อ', 'dropship' ),
			'date_created'		=> esc_html__( 'วันที่สั่งซื้อ', 'dropship' ),
			'status'			=> esc_html__( 'สถานะคำสั่งซื้อ', 'dropship' ),
			'sub_total'			=> esc_html__( 'ทั้งหมด', 'dropship' ),
			'shipping_cost'		=> esc_html__( 'ขนส่ง', 'dropship' ),
			'total'				=> esc_html__( 'รวมทั้งหมด', 'dropship' ),
			'commission'		=> esc_html__( 'คอมมิชชั่น (%)', 'dropship' ),
			'commission_price'	=> esc_html__( 'คอมมิชชั่น (บาท)', 'dropship' ),
			'supplier'			=> esc_html__( 'ผู้ผลิต', 'dropship' ),
			'payment_status'	=> esc_html__( 'สถานะคอมมิชชั่น', 'dropship' ),
		);

		if ( dsp_is_headsale() ) {
			$columns = array_merge( $admin_columns , $columns );
		}

        if ( $export ) {
            if ( isset( $columns['cb'] ) ) {
                unset($columns['cb']);
            }
            $columns['cus_name'] = esc_html__('ลูกค้า', 'dropship');
            $columns['cus_address'] = esc_html__('ที่อยู่', 'dropship');
            $columns['cus_tel'] = esc_html__('เบอร์โทรศัพท์', 'dropship');
            $columns['product'] = esc_html__('สินค้า', 'dropship');
			$columns['courier_name'] = esc_html__('บริษัทขนส่ง', 'dropship');
			$columns['tracking_number'] = esc_html__('หมายเลขติดตามพัสดุ', 'dropship');
			$columns['tracking_status'] = esc_html__('สถานะการจัดส่ง', 'dropship');
        }

        return $columns;
    }

	function dsp_wp_column_report_dashboard( $export = false ) {
		$columns = array(
			'order'				=> __( 'คำสั่งซื้อ', 'dropship' ),
			'date_created'		=> esc_html__( 'วันที่สั่งซื้อ', 'dropship' ),
			'status'			=> esc_html__( 'สถานะคำสั่งซื้อ', 'dropship' ),
			'sub_total'			=> esc_html__( 'ทั้งหมด', 'dropship' ),
			'shipping_cost'		=> esc_html__( 'ขนส่ง', 'dropship' ),
			'total'				=> esc_html__( 'รวมทั้งหมด', 'dropship' ),
			'supplier'			=> esc_html__( 'ผู้ผลิต', 'dropship' ),
			'brand'				=> esc_html__( 'ยี่ห้อ', 'dropship' ),
			'courier_name'		=> esc_html__('บริษัทขนส่ง', 'dropship'),
			'tracking_number'   => esc_html__('หมายเลขติดตามพัสดุ', 'dropship')
		);

        if ( $export ) {
			unset($columns['courier_name']);
			unset($columns['tracking_number']);

            $columns['cus_name'] = esc_html__('ลูกค้า', 'dropship');
            $columns['cus_address'] = esc_html__('ที่อยู่', 'dropship');
            $columns['cus_tel'] = esc_html__('เบอร์โทรศัพท์', 'dropship');
            $columns['product'] = esc_html__('สินค้า', 'dropship');
			$columns['courier_name'] = esc_html__('บริษัทขนส่ง', 'dropship');
			$columns['tracking_number'] = esc_html__('หมายเลขติดตามพัสดุ', 'dropship');
			$columns['tracking_status'] = esc_html__('สถานะการจัดส่ง', 'dropship');
        }

        return $columns;
    }

    function dsp_wp_records_table_data_dashboard( $item , $column_name , $order_items = null , $export = false ) {
        switch ( $column_name ) {
			case 'order':
				if ( current_user_can( 'edit_shop_orders' ) ) {
					$url = get_edit_post_link( $item['id'] );
				} else {
					$url = admin_url( 'admin.php?page=dropship-order-detail&order_id=' . $item['id'] );
				}
				if ( $export ) {
					return $item['id'];
				} else {
					return " <a href='" . $url . "'> #" . $item['id'] . ' ' . $item['billing']['first_name'] . ' ' . $item['billing']['last_name'] . ' </a> ';
				}
				break;
			case 'date_created':
				// return date("d-m-Y H:i:s" , strtotime($item['date_created']));
				$order = wc_get_order( $item['id'] );
				return $order->get_date_modified()->date("d/m/Y H:i:s");
				break;
			case 'commission':
				$log_id = get_post_meta($item['id'] , '_dsp_log_order_commission_id' , true);
				$items = Dropship_Model_log::get_instance()->dsp_get_log_order_commission( $log_id );
				return (isset($items['com_value'])) ? $items['com_value'] . " %" : 0;
				break;
			case 'commission_price':
				$price = get_post_meta( $item['id'] , '_dsp_order_paid_commission' , true);
				$price = ( !empty($price) ) ? wc_price( $price ) : "-";
				if ( $export ) {
					return str_replace("," , "", $price);
				} else {
					return $price;
				}
				break;
			case 'payment_status':
				$payment_status = get_post_meta( $item['id'] , '_dsp_paid_success' , true);
				$payment_success = '<div class="ds-commis-s">'.esc_html__( 'ทำจ่ายแล้ว', 'dropship' ).'</div>';
				$payment_error = '<div class="ds-commis-e">'.esc_html__( 'ยังไม่ทำจ่าย', 'dropship' ).'</div>';
				return ( !empty($payment_status) && $payment_status == 'yes' ) ? $payment_success : $payment_error;
				break;
			case 'sub_total':
				$order = wc_get_order( $item['id'] );
				if ( $export ) {
					return $order->get_subtotal();
				} else {
					return wc_price( $order->get_subtotal() );
				}
				break;
			case 'shipping_cost':
				$order = wc_get_order( $item['id'] );
				if ( $export ) {
					return $order->get_shipping_total();
				} else {
					return wc_price( $order->get_shipping_total() );
				}
				break;
			case 'total':
				$order = wc_get_order( $item['id'] );
				if ( $export ) {
					return $order->get_total();
				} else {
					return wc_price( $order->get_total() );
				}
				break;
			case 'status':
				return esc_html( wc_get_order_status_name( $item[ $column_name ] ) );
				break;
			case 'sale_id':
				$sale_id = get_post_meta( $item['id'] , '_dsp_ref_id' , true);
				$user = get_user_by( 'id', $sale_id );
				$name = (!empty($user)) ? $user->display_name : false;
				return ($name) ? $name : "-";
				break;
            case 'product':
                if ( is_null( $order_items ) ) {
					$order = wc_get_order( $item['id'] );
					$order_items = $order->get_items();
                }
				$name = [];
				foreach ($order_items as $pd) {
					$name[] = $pd->get_name() . " ( " . $pd->get_quantity() . " items) ";
				}
                return (!empty($name)) ? implode( " | " , $name ) : "-";
                break;
            case 'cus_address':
                return dsp_billing_address_to_str( $item['billing'] );
                break;
            case 'cus_name':
                return $item['billing']['first_name'] . " " . $item['billing']['last_name'];
                break;
            case 'cus_tel':
                return $item['billing']['phone'];
                break;
			case 'supplier':
			case 'brand':
				$sup = dsp_get_orders_products_supplier_show( $column_name , $item['id'] );
				return strip_tags($sup);
				break;
			case 'courier_name':
			case 'tracking_number':
			case 'tracking_status':
				$meta = get_post_meta( $item['id'] , '_dsp_' . $column_name , true);
				return ($meta) ? $meta : "";
				break;
			default:
				return "";
				break;
		}
    }

    function dsp_wp_query_sale_dashboard( &$args , $status = '', $order_status = '' , $sale_id = 0 , $daterange_start = null , $daterange_end = null , $getTotal = false ) {
        $paidStatus = 0;
		if ( !empty($status) ) {
			switch ($status) {
				case 'paid':
					$paidStatus = true;
					break;
				case 'unpaid':
					$paidStatus = false;
					break;				
				default:
					$paidStatus = 0;
					break;
			}
		}

		if ( empty($order_status) ) {
			$statuses = array( 'wc-processing', 'wc-canceled', 'wc-completed', 'wc-refunded' , 'wc-pending' , 'wc-on-hold' , 'wc-failed' );
		} else {
			$statuses = array( $order_status );
		}

		$_meta_query = [];
		$meta_query  = array(
			'relation' => 'AND',
		);

		$_meta_query[] = array(
			'relation' => 'AND',
		);

        if ( dsp_is_headsale() || dsp_is_operation() ) {
			if ( $sale_id > 0 ) {
				$_meta_query[] = array(
					'key'     => '_dsp_ref_id',
					'value'   => $sale_id,
					'compare' => '=',
				);
			}
        } else {
			$_meta_query[] = array(
				'key'     => '_dsp_ref_id',
				'value'   => get_current_user_id(),
				'compare' => '=',
			);
		}

		if ( is_bool($paidStatus) ) {
			if ( $paidStatus ) {
				$_meta_query[] = array(
					'key'     => '_dsp_paid_success',
					'value'   => 'yes',
					'compare' => '=',
				);
			} else {
				$AND = array(
					'relation' => 'OR',
				);
				$AND[] = array(
					'key'     => '_dsp_paid_success',
					'compare' => 'NOT EXISTS',
				);
				$AND[] = array(
					'key'     => '_dsp_paid_success',
					'value'   => 'no',
					'compare' => '=',
				);

				$_meta_query[] = $AND;
			}
		}

		$meta_query[] = $_meta_query;
		$args = array(
			'post_status' => $statuses,
			'post_type'   => 'shop_order',
			'fields'      => 'ids',
			'meta_query'  => $meta_query,
		);
		
		if ( $daterange_start && $daterange_end ) {
			$start_date = date( 'Y-m-d', strtotime( str_replace( '/', '-', $daterange_start ) ) );
			$end_date   = date( 'Y-m-d', strtotime( str_replace( '/', '-', $daterange_end ) ) );
			$args['date_query'] = array(
				'after'     => $start_date,
				'before'    => $end_date,
				'inclusive' => true,
			);
		}

		$query      = new WP_Query( $args );
        if ( $getTotal ) {
            return $query->found_posts;
        }
        return $query->posts;
    }

	function dsp_get_orders_ids_by_product_id( &$args , $product_ids , $order_status , $daterange_start , $daterange_end ) {
		global $table_prefix, $wpdb;

		if ( empty($order_status) ) {
			$statuses = array( 'wc-processing', 'wc-canceled', 'wc-completed', 'wc-refunded' , 'wc-on-hold' );
		} else {
			$statuses = array( $order_status );
		}

		if ( empty($product_ids) ) {
			return [];
		}

		/* AND p.post_status IN ( 'wc-completed', 'wc-processing', 'wc-on-hold' ) */
		$orders_ids_by_product_id = $wpdb->get_col( "
			SELECT DISTINCT woi.order_id
			FROM {$table_prefix}woocommerce_order_itemmeta as woim, 
				 {$table_prefix}woocommerce_order_items as woi, 
				 {$table_prefix}posts as p
			WHERE woi.order_item_id = woim.order_item_id
			AND woi.order_id = p.ID
			AND woim.meta_key IN ( '_product_id', '_variation_id' )
			AND woim.meta_value IN ( " . implode( ',' , $product_ids ) . " )
			ORDER BY woi.order_id DESC"
		);

		if ( empty($orders_ids_by_product_id) ) {
			return [];
		}

		$args = array(
			'post_status' => $statuses,
			'post_type'   => 'shop_order',
			'fields'      => 'ids',
			'post__in' 	  => $orders_ids_by_product_id
		);

		if ( $daterange_start && $daterange_end ) {
			$start_date = date( 'Y-m-d', strtotime( str_replace( '/', '-', $daterange_start ) ) );
			$end_date   = date( 'Y-m-d', strtotime( str_replace( '/', '-', $daterange_end ) ) );
			$args['date_query'] = array(
				'after'     => $start_date,
				'before'    => $end_date,
				'inclusive' => true,
			);
		}

		$query = new WP_Query(array_merge( $args , array('posts_per_page' => -1 )) ) ;
		return $query->posts;
	}

	function dsp_update_order_status_pending_to_onhold() {
		$_dropship_update_order_cron = get_option( '_dropship_update_order_cron' , 0 );
		if ( $_dropship_update_order_cron === 0 ) {
			$orders = wc_get_orders([
					'limit' => -1,
					'type' => 'shop_order',
					'status' => ['wc-pending'],
					'return' => 'ids',
				]
			);

			foreach ($orders as $order_id) {
				$order = new WC_Order($order_id);
				if (!empty($order)) {
					$order->update_status( 'on-hold' );
				}
			}

			update_option( '_dropship_update_order_cron' , date('Y-m-d') );
			return $orders;
		}
	}

    function dps_product_settings_tabs_commission($tabs)
    {
        $tabs['dropship_commission'] = array(
            'label'    => esc_html__( 'ราคาจัดส่ง', 'dropship' ),
            'target'   => 'dropship_commission_product_data'
        );
        return $tabs;
    }

    function dsp_commission_product_panels()
    {
        echo '<div id="dropship_commission_product_data" class="panel woocommerce_options_panel hidden">';
            // woocommerce_wp_text_input(array(
            //     'id'                => '_dropship_commission_rate',
            //     'value'             => get_post_meta(get_the_ID(), '_dropship_commission_rate', true),
            //     'label'             => esc_html__( 'อัตรา (%)', 'dropship' ),
            //     'description'       => esc_html__( 'Commission rate', 'dropship' ),
            //     'custom_attributes' => [
            //         'pattern' => "[0-9.]+"
            //     ]
            // ));

            woocommerce_wp_text_input([
                'id'                => '_dropship_shipping_cost',
                'value'             => get_post_meta(get_the_ID(), '_dropship_shipping_cost', true),
                'label'             => esc_html__( 'ค่าขนส่ง (ต่อชิ้น)', 'dropship' ),
                'description'       => esc_html__( 'บาท', 'dropship' ),
                'custom_attributes' => [
                    'pattern' => "[0-9.]+"
                ]
			]);

			woocommerce_wp_text_input([
                'id'                => '_dropship_shipping_cost_next',
                'value'             => get_post_meta(get_the_ID(), '_dropship_shipping_cost_next', true),
                'label'             => esc_html__( 'ค่าขนส่งชิ้นถัดไป (ต่อชิ้น)', 'dropship' ),
                'description'       => esc_html__( 'บาท', 'dropship' ),
                'custom_attributes' => [
                    'pattern' => "[0-9.]+"
                ]
			]);
        echo '</div>';
    }

    function dsp_commission_product_save_fields( $id, $post ) {
        // if( !empty( $_POST['_dropship_commission_rate'] ) && is_numeric( $_POST['_dropship_commission_rate'] ) ) {
        //     update_post_meta( $id, '_dropship_commission_rate', $_POST['_dropship_commission_rate'] );
        // }

		update_post_meta( $id, '_dropship_shipping_cost', sanitize_text_field( $_POST['_dropship_shipping_cost'] ) );
		update_post_meta( $id, '_dropship_shipping_cost_next', sanitize_text_field( $_POST['_dropship_shipping_cost_next'] ) );
    }
}
