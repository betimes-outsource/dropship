<?php

class Dropship_Model_Commission
{
    public static $instance;

    public function __construct() {
        self::$instance = $this;
    }

    public static function get_instance() {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    function dsp_insert_sale_product_id_commission( $sale_id , $product_id , $com_value , $logs = [] ) {
        global $table_prefix, $wpdb;
        $table = $table_prefix . DROPSHIP_TABLE_SALE_PRODUCTS_COMMISSION;
        $insert_id = 0;
        $data = array( 'sale_id' => $sale_id, 'product_id' => $product_id, 'com_value' => $com_value , 'create_user_id' => get_current_user_id()  , 'update_user_id' => get_current_user_id() , 'logs' => json_encode( $logs ) );
        $format = array( '%d' , '%d' , '%f'  , '%d' , '%d' , '%s' );
        try {
            $id = $wpdb->get_var("SELECT `id` FROM `$table` WHERE `sale_id` = $sale_id AND `product_id` = $product_id");
            if ( empty($id) ) {
                $wpdb->insert( $table , $data , $format );
                $insert_id = $wpdb->insert_id;
            } else {
                $wpdb->update( $table , $data , ['id' => $id] , $format );
                $insert_id = $id;
            }
        } catch (\Throwable $th) {
            dsp_error_log( 'dsp_insert_sale_product_id_commission' , $data , $th->getMessage() );
        }

        return $insert_id;
    }

    function dsp_insert_sale_commission( $sale_id , $com_value , $logs = [] ) {
        global $table_prefix, $wpdb;
        $table = $table_prefix . DROPSHIP_TABLE_SALE_COMMISSION;
        $insert_id = 0;
        $data = array( 'sale_id' => $sale_id, 'com_value' => $com_value , 'create_user_id' => get_current_user_id()  , 'update_user_id' => get_current_user_id() , 'logs' => json_encode( $logs ) );
        $format = array( '%d' , '%f'  , '%d' , '%d' , '%s' );
        try {
            $wpdb->insert( $table , $data , $format );
            $insert_id = $wpdb->insert_id;
        } catch (\Throwable $th) {
            dsp_error_log( 'dsp_insert_sale_commission' , $data , $th->getMessage() );
        }

        return $insert_id;
    }

    function dsp_get_sale_products_commission( $sale_id , $product_id ) {
        global $table_prefix, $wpdb;
        $table = $table_prefix . DROPSHIP_TABLE_SALE_PRODUCTS_COMMISSION;
		$items = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT com_value FROM $table WHERE sale_id = %d AND product_id = %d", 
                $sale_id,
                $product_id
                ), 
                ARRAY_A
        );

        return $items;
    }
}