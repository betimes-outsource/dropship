<?php

class Dropship_Model_log
{
    public static $instance;

    public function __construct() {
        self::$instance = $this;
    }

    public static function get_instance() {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    function dsp_insert_log_order_commission( $order_id , $com_type , $com_value , $logs = [] ) {
        global $table_prefix, $wpdb;
        $table = $table_prefix . DROPSHIP_TABLE_LOG_ORDER_COMMISSION;
        $insert_id = 0;
        $data = array( 'order_id' => $order_id, 'com_type' => $com_type , 'com_value' => $com_value , 'logs' => json_encode( $logs ) );
        $format = array( '%d' , '%s' , '%f' , '%s' );
        try {
            $wpdb->insert( $table , $data , $format );
            $insert_id = $wpdb->insert_id;
        } catch (\Throwable $th) {
            dsp_error_log( 'dsp_insert_log_order_commission' , $data , $th->getMessage() );
        }

        return $insert_id;
    }

    function dsp_get_log_order_commission ($log_id) {
        global $table_prefix, $wpdb;
        $table = $table_prefix . DROPSHIP_TABLE_LOG_ORDER_COMMISSION;
        $items = $wpdb->get_row(
            $wpdb->prepare(
                "SELECT * FROM $table WHERE id = %d", 
                $log_id
            ), 
            ARRAY_A
        );

        return $items;
    }
}