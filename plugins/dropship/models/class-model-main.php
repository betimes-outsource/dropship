<?php

class Dropship_Model {

    public function __construct() {
        self::init();
    }

    public static function init()
	{
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'models/log.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'models/commission.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'models/products.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'models/users.php';
    }
}

return new Dropship_Model();