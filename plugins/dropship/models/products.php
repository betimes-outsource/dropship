<?php

class Dropship_Model_Products
{
    public static $instance;

    public function __construct() {
        self::$instance = $this;
    }

    public static function get_instance() {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    function dsp_insert_sale_products_catalog( $sale_id , $product_id ) {
        global $table_prefix, $wpdb;
        $table = $table_prefix . DROPSHIP_TABLE_SALE_PRODUCTS_CATALOG;
        $insert_id = 0;
        $data = array( 'sale_id' => $sale_id, 'product_id' => $product_id );
        $format = array( '%d' , '%d' );
        try {
            $wpdb->insert( $table , $data , $format );
            $insert_id = $wpdb->insert_id;
        } catch (\Throwable $th) {
            dsp_error_log( 'dsp_insert_sale_products_catalog' , $data , $th->getMessage() );
        }

        return $insert_id;
    }

    function dsp_delete_sale_products_catalog( $sale_id , $product_id ) {
        global $table_prefix, $wpdb;
        $table = $table_prefix . DROPSHIP_TABLE_SALE_PRODUCTS_CATALOG;
        $result = false;
        $data = array( 'sale_id' => $sale_id, 'product_id' => $product_id );
        $format = array( '%d' , '%d' );
        try {
            $result = $wpdb->delete( $table,  $data , $format );
        } catch (\Throwable $th) {
            dsp_error_log( 'dsp_delete_sale_products_catalog' , $data , $th->getMessage() );
        }

        return $result;
    }

    function dsp_get_sale_products_commission( $per_page = 10 , $currentPage = 1 , $orderby = '' , $order = '' ) {
        global $table_prefix, $wpdb;
        $table = $table_prefix . DROPSHIP_TABLE_SALE_PRODUCTS_COMMISSION;

        $total = $wpdb->get_var("SELECT COUNT(*) FROM $table");

		$items = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT * FROM $table ORDER BY $orderby $order LIMIT %d OFFSET %d", 
                $per_page, 
                $currentPage - 1
                ), 
                ARRAY_A
        );

        return [ 'total' => $total , 'items' => $items ];
    }

    function dsp_delete_sale_products_commission( $sale_id ) {
        global $table_prefix, $wpdb;
        $table_name = $table_prefix . DROPSHIP_TABLE_SALE_PRODUCTS_COMMISSION;
        $wpdb->delete( $table_name, array( 'sale_id' => $sale_id ) , [ '%d' ] );

        return true;
    }

    function dsp_get_post_join_sale_products_catalog( $sale_id , $categories_ids = [] , $page = 1 , $items_per_page = 1 , $self = false , $order = 1) {
        global $table_prefix, $wpdb;
        $catalog_table = $table_prefix . DROPSHIP_TABLE_SALE_PRODUCTS_CATALOG;
        $commission_table = $table_prefix . DROPSHIP_TABLE_SALE_PRODUCTS_COMMISSION;

        $offset = ( $page * $items_per_page ) - $items_per_page;

        switch ($order) {
            case '1':
                $orderBy = 'ORDER BY wposts.ID DESC';
                break;
            case '2':
                $orderBy = 'ORDER BY wcommission.com_value DESC';
                break;
            default:
                $orderBy = 'ORDER BY wposts.ID DESC';
                break;
        }

        $groupBy = 'GROUP BY wposts.ID, wcommission.id, cid';

        $whereCategories = "";
        if ( !empty($categories_ids) ) {
            $whereCategories = $wpdb->prepare(" AND terms.term_id IN (" . implode( "," , $categories_ids ) . ") AND %d = %d" , 1 , 1 );
        }

        if ( $self == "A" ) {
            $totalItems = $wpdb->get_results(
                $sqlT = $wpdb->prepare(
                    "SELECT DISTINCT wposts.ID , wcommission.com_value, wcatalog.id as cid
                    FROM {$wpdb->posts} AS wposts
                    INNER JOIN $catalog_table AS wcatalog ON wposts.ID = wcatalog.product_id
                    LEFT JOIN $commission_table AS wcommission ON (wposts.ID = wcommission.product_id AND wcommission.sale_id = %d)
                    LEFT JOIN {$wpdb->postmeta} AS wpostmeta ON (wposts.ID = wpostmeta.post_id)
                    LEFT JOIN {$wpdb->term_relationships} AS tax_rel ON (wposts.ID = tax_rel.object_id)
                    LEFT JOIN {$wpdb->term_taxonomy} AS term_tax ON (tax_rel.term_taxonomy_id = term_tax.term_taxonomy_id)
                    LEFT JOIN {$wpdb->terms} AS terms ON (terms.term_id = term_tax.term_id)
                    WHERE wposts.post_type = %s AND wposts.post_status = %s AND wcatalog.sale_id = %d $whereCategories $groupBy",
                    $sale_id,
                    "product",
                    "publish",
                    $sale_id,
                )
            );
            $totalItems = count($totalItems);
            $results = $wpdb->get_results(
                $sqlS = $wpdb->prepare(
                    "SELECT DISTINCT wposts.ID , wcommission.com_value, wcatalog.id as cid
                    FROM {$wpdb->posts} AS wposts
                    INNER JOIN $catalog_table AS wcatalog ON wposts.ID = wcatalog.product_id 
                    LEFT JOIN $commission_table AS wcommission ON (wposts.ID = wcommission.product_id AND wcommission.sale_id = %d)
                    LEFT JOIN {$wpdb->postmeta} AS wpostmeta ON (wposts.ID = wpostmeta.post_id)
                    LEFT JOIN {$wpdb->term_relationships} AS tax_rel ON (wposts.ID = tax_rel.object_id)
                    LEFT JOIN {$wpdb->term_taxonomy} AS term_tax ON (tax_rel.term_taxonomy_id = term_tax.term_taxonomy_id)
                    LEFT JOIN {$wpdb->terms} AS terms ON (terms.term_id = term_tax.term_id)
                    WHERE wposts.post_type = %s AND wposts.post_status = %s AND wcatalog.sale_id = %d $whereCategories $groupBy $orderBy LIMIT %d,%d",
                    $sale_id,
                    "product",
                    "publish",
                    $sale_id,
                    $offset,
                    $items_per_page
                ),
                ARRAY_A
            );
        } elseif ( $self == "B" ) {
            $totalItems = $wpdb->get_results(
                $sqlT = $wpdb->prepare(
                    "SELECT DISTINCT wposts.ID , wcommission.com_value, (SELECT id FROM wp_dropship_sale_products_catalog WHERE product_id = wposts.ID AND sale_id = $sale_id ) AS cid
                    FROM {$wpdb->posts} AS wposts
                    LEFT JOIN $catalog_table AS wcatalog ON wposts.ID = wcatalog.product_id
                    LEFT JOIN $commission_table AS wcommission ON (wposts.ID = wcommission.product_id AND wcommission.sale_id = %d)
                    LEFT JOIN {$wpdb->postmeta} AS wpostmeta ON (wposts.ID = wpostmeta.post_id)
                    LEFT JOIN {$wpdb->term_relationships} AS tax_rel ON (wposts.ID = tax_rel.object_id)
                    LEFT JOIN {$wpdb->term_taxonomy} AS term_tax ON (tax_rel.term_taxonomy_id = term_tax.term_taxonomy_id)
                    LEFT JOIN {$wpdb->terms} AS terms ON (terms.term_id = term_tax.term_id)
                    WHERE wposts.post_type = %s AND wposts.post_status = %s $whereCategories $groupBy",
                    $sale_id,
                    "product",
                    "publish"
                )
            );
            $totalItems = count($totalItems);
            $results = $wpdb->get_results(
                $sqlS = $wpdb->prepare(
                    "SELECT DISTINCT wposts.ID , wcommission.com_value, (SELECT id FROM $catalog_table WHERE product_id = wposts.ID AND sale_id = $sale_id ) AS cid
                    FROM {$wpdb->posts} AS wposts
                    LEFT JOIN $catalog_table AS wcatalog ON wposts.ID = wcatalog.product_id
                    LEFT JOIN $commission_table AS wcommission ON (wposts.ID = wcommission.product_id AND wcommission.sale_id = %d)
                    LEFT JOIN {$wpdb->postmeta} AS wpostmeta ON (wposts.ID = wpostmeta.post_id)
                    LEFT JOIN {$wpdb->term_relationships} AS tax_rel ON (wposts.ID = tax_rel.object_id)
                    LEFT JOIN {$wpdb->term_taxonomy} AS term_tax ON (tax_rel.term_taxonomy_id = term_tax.term_taxonomy_id)
                    LEFT JOIN {$wpdb->terms} AS terms ON (terms.term_id = term_tax.term_id)
                    WHERE wposts.post_type = %s AND wposts.post_status = %s $whereCategories $groupBy $orderBy LIMIT %d,%d",
                    $sale_id,
                    "product",
                    "publish",
                    $offset,
                    $items_per_page
                ),
                ARRAY_A
            );
        } else {
            $product_ids = $wpdb->get_var($wpdb->prepare("SELECT GROUP_CONCAT(product_id SEPARATOR ', ') FROM $catalog_table WHERE sale_id = %d", $sale_id));
            $totalItems = $wpdb->get_results(
                $sqlT = $wpdb->prepare(
                    "SELECT DISTINCT wposts.ID , wcommission.com_value, (SELECT 0) AS cid
                    FROM {$wpdb->posts} AS wposts
                    LEFT JOIN $commission_table AS wcommission ON (wposts.ID = wcommission.product_id AND wcommission.sale_id = %d)
                    LEFT JOIN {$wpdb->postmeta} AS wpostmeta ON (wposts.ID = wpostmeta.post_id)
                    LEFT JOIN {$wpdb->term_relationships} AS tax_rel ON (wposts.ID = tax_rel.object_id)
                    LEFT JOIN {$wpdb->term_taxonomy} AS term_tax ON (tax_rel.term_taxonomy_id = term_tax.term_taxonomy_id)
                    LEFT JOIN {$wpdb->terms} AS terms ON (terms.term_id = term_tax.term_id)
                    WHERE wposts.post_type = %s AND wposts.post_status = %s AND wposts.ID NOT IN (" . $product_ids . ") $whereCategories $groupBy",
                    $sale_id,
                    "product",
                    "publish",
                    $sale_id
                )
            );
            $totalItems = count($totalItems);
            $results = $wpdb->get_results(
                $sqlS = $wpdb->prepare(
                    "SELECT DISTINCT wposts.ID , wcommission.com_value, (SELECT 0) AS cid
                    FROM {$wpdb->posts} AS wposts
                    LEFT JOIN $commission_table AS wcommission ON (wposts.ID = wcommission.product_id AND wcommission.sale_id = %d)
                    LEFT JOIN {$wpdb->postmeta} AS wpostmeta ON (wposts.ID = wpostmeta.post_id)
                    LEFT JOIN {$wpdb->term_relationships} AS tax_rel ON (wposts.ID = tax_rel.object_id)
                    LEFT JOIN {$wpdb->term_taxonomy} AS term_tax ON (tax_rel.term_taxonomy_id = term_tax.term_taxonomy_id)
                    LEFT JOIN {$wpdb->terms} AS terms ON (terms.term_id = term_tax.term_id)
                    WHERE wposts.post_type = %s AND wposts.post_status = %s AND wposts.ID NOT IN (" . $product_ids . ") $whereCategories $groupBy $orderBy LIMIT %d,%d",
                    $sale_id,
                    "product",
                    "publish",
                    $offset,
                    $items_per_page
                ),
                ARRAY_A
            );
        }

        // error_log( $sqlT );
        // error_log( $sqlS );

        return [ 'result' => $results , 'total' => $totalItems];
    }
}