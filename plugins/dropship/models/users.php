<?php

class Dropship_Model_Users
{
    public static $instance;

    public function __construct() {
        self::$instance = $this;
    }

    public static function get_instance() {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    function dsp_check_sale_id_duplicate( $value , $sale_id ) {
        global $table_prefix, $wpdb;
        $table = $table_prefix . 'usermeta';

        $totalItems = $wpdb->get_var("SELECT COUNT(`user_id`) FROM `$table` WHERE `user_id` != $sale_id AND `meta_key` = '_dsp_sale_id' AND `meta_value` = '$value'");
        return ( $totalItems > 0 ) ? true : false;
    }

    function dsp_get_user_id_by_sale_id( $_dsp_sale_id ) {
        global $table_prefix , $wpdb;
        $table = $table_prefix . 'usermeta';

        $user_id = $wpdb->get_var("SELECT `user_id` FROM `$table` WHERE `meta_key` = '_dsp_sale_id' AND `meta_value` = '$_dsp_sale_id'");
        return $user_id;
    }

}