<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.facebook.com/makkungs/
 * @since             1.0.0
 * @package           Dropship
 *
 * @wordpress-plugin
 * Plugin Name:       Dropship
 * Plugin URI:        https://www.facebook.com/makkungs/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.17
 * Author:            Mc
 * Author URI:        https://www.facebook.com/makkungs/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       dropship
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'DROPSHIP_VERSION', '1.0.17' );

// ROLE
define( 'DROPSHIP_ROLE_SALE' , 'dropship_sale' );
define( 'DROPSHIP_ROLE_HEADSALE' , 'dropship_headsale' );
define( 'DROPSHIP_ROLE_OPERATION' , 'dropship_operation' );
define( 'DROPSHIP_PAGE_CONTENT' , [
	'_dropship_default_catalog_page' => 'dsp_catalog_page',
	'_dropship_default_complete_page' => 'dsp_complete_page',
	'_dropship_default_tracking_page' => 'dsp_tracking_page',
	// '_dropship_default_refund_page' => 'dsp_refund_page',
] );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-dropship-activator.php
 */
function activate_dropship() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-dropship-activator.php';
	Dropship_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-dropship-deactivator.php
 */
function deactivate_dropship() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-dropship-deactivator.php';
	Dropship_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_dropship' );
register_deactivation_hook( __FILE__, 'deactivate_dropship' );

require plugin_dir_path( __FILE__ ) . 'includes/class-dropship-shipping-method.php';
require plugin_dir_path( __FILE__ ) . 'includes/class-dropship-payment-gateway.php';

/* model */
require plugin_dir_path( __FILE__ ) . 'models/class-model-main.php';

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-dropship.php';

require plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_dropship() {
	$plugin = new Dropship();
	$plugin->run();
	
	define( 'DROPSHIP_PLUGIN_DIR_URL' , plugin_dir_url( __FILE__ ) );
}

function dsp_is_sale( $user = null ) {
	if ( is_null( $user ) ) {
		$user = wp_get_current_user();
	}
	if ( ! is_wp_error( $user ) ) {
		if (in_array(DROPSHIP_ROLE_SALE, $user->roles) ) {
			return true;
		}
	}
	return false;
}

function dsp_is_headsale( $user = null , $adminstrator = true ) {
	if ( is_null( $user ) ) {
		$user = wp_get_current_user();
	}
	if ( ! is_wp_error( $user ) ) {
		if (in_array(DROPSHIP_ROLE_HEADSALE, $user->roles) || ( in_array('administrator', $user->roles) && $adminstrator ) ) {
			return true;
		}
	}
	return false;
}

function dsp_is_operation($user = null) {
	if ( is_null( $user ) ) {
		$user = wp_get_current_user();
	}
	if ( ! is_wp_error( $user ) ) {
		if (in_array(DROPSHIP_ROLE_OPERATION, $user->roles)) {
			return true;
		}
	}
	return false;
}

run_dropship();