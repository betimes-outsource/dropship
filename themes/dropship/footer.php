<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Dropship
 */
v
?>
</section>
<footer class="footer text-center site-footer bg-color" id="colophon">
	<div class="container bg-color-main ds-footer-container ds-layout-responsive py-4" id="mobile-font">
		<div class="row">
			<div class="col-12 col-md-8 mb-3 mb-md-0">
				<div class="d-flex flex-column">
					<h5 class="ds-footer-title mb-3"><?php esc_html_e('ติดต่อเรา', 'dropship'); ?></h5>
					<div class="d-flex flex-column flex-md-row">
						<p class="ds-footer-social"><img class="ds-footer-icon" src="<?php echo esc_url(get_template_directory_uri() . "/assets/line.png"); ?>" class="img-fluid"> <?php echo esc_html__('@Bedropship', 'dropship'); ?></p>
						<p class="ds-footer-social"><img class="ds-footer-icon" src="<?php echo esc_url(get_template_directory_uri() . "/assets/facebook.png"); ?>" class="img-fluid ms-3"> <?php echo esc_html__('Bedropship', 'dropship'); ?></p>
						<p class="ds-footer-social"><img class="ds-footer-icon" src="<?php echo esc_url(get_template_directory_uri() . "/assets/telephone.png"); ?>" class="img-fluid ms-3"> <?php echo esc_html__('02-123-8022', 'dropship'); ?></p>
					</div>
				</div>
			</div>

			<div class="col-12 col-md-4">
				<div class="d-flex flex-column">
					<h5 class="ds-footer-title mb-3"><?php esc_html_e('คำถามที่พบบ่อย', 'dropship'); ?></h5>
					<div class="d-flex flex-column flex-md-row">
						<?php
						$_refund_page = get_option('_dropship_default_refund_page', 0);
						$link = get_permalink($_refund_page);
						if ($link === false) {
							$link = get_home_url();
						}
						?>
						<a class="ds-footer-social" href="<?php echo esc_url($link); ?>">
							<img class="ds-footer-icon" src="<?php echo esc_url(get_template_directory_uri() . "/assets/refund.png"); ?>" class="img-fluid"> <?php echo esc_html__('ขั้นตอนการขอคืนสินค้า คืนเงิน', 'dropship'); ?>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>