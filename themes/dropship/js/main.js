(function($) {
    'use strict';

    /**
     * All of the code for your admin-facing JavaScript source
     * should reside in this file.
     *
     * Note: It has been assumed you will write jQuery code here, so the
     * $ function reference has been prepared for usage within the scope
     * of this function.
     *
     * This enables you to define handlers, for when the DOM is ready:
     *
     * $(function() {
     *
     * });
     *
     * When the window is loaded:
     *
     * $( window ).load(function() {
     *
     * });
     *
     * ...and/or other possibilities.
     *
     * Ideally, it is not considered best practise to attach more than a
     * single DOM-ready or window-load handler for a particular page.
     * Although scripts in the WordPress core, Plugins and Themes may be
     * practising this, we should strive to set a better example in our own work.
     */

    $(function() {
        alertify.set('notifier', 'position', 'top-right');
        // console.log(dropshipjs_main)
        $.LoadingOverlaySetup({
            background: "rgba(0, 0, 0, 0.5)",
            image: dropshipjs_main.theme_url + "/assets/loading.svg",
            imageAnimation: false,
            size: 15,
            imageColor: false
        });

        $(document).ajaxSend(function(event, jqxhr, settings) {
            $.LoadingOverlay("show");
        });
        $(document).ajaxComplete(function(event, jqxhr, settings) {
            $.LoadingOverlay("hide");
        });
        $(document).ajaxError(function(event, jqxhr, settings) {
            $.LoadingOverlay("hide");
        });
    });

})(jQuery);