(function($) {

    $(function() {
        if ($("#address_province").length > 0) {
            dsp_generate_select_province();
        }
    });

    $('body').on("click", "form#form-payment-address button.submit-form", function(e) {
        e.preventDefault();
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if ($("input#address_email").val() != '') {
            if (regex.test($("input#address_email").val()) === false) {
                $("input#address_email").addClass("required-field");
                $("input#address_email").focus();
                return false;
            }
        }
        $("form#form-payment-address").submit();
    });

    $("form#form-payment-address").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        formData.append("action", "dsp_place_order");
        $.ajax({
            url: dropshipjs_salepage.ajax_url,
            type: "POST",
            contentType: false,
            processData: false,
            dataType: 'JSON',
            status: 200,
            data: formData,
            beforeSend: function() {
                $(".required-field").removeClass("required-field");
            },
            success: function(res) {
                if (res.status) {
                    if (res.redirect != undefined) {
                        window.location.href = res.redirect;
                    } else {
                        alertify.success(res.order_id);
                    }
                } else if (res.status == false && res.name != undefined) {
                    $("#" + res.name + "").addClass("required-field");
                    if (res.name == 'product-section') {
                        var pos = $("#" + res.name + "").offset().top - 150;
                        $('body, html').animate({ scrollTop: pos });
                    } else {
                        $("#" + res.name + "").focus();
                    }
                    alertify.error(res.msg);
                } else {
                    alertify.error(res.msg);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alertify.error(textStatus);
            }
        });

        return false;
    });

    $('body').on("click", "button.add-to-cart", function(e) {
        var card = $(this).closest("div.card");
        var product_id = $(card).data("product-id");
        var variation_id = $(card).data("variation-id");
        var product_qty = $(card).find("input[name='quantity']").val();

        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: dropshipjs_salepage.ajax_url,
            data: { 'product_id': product_id, 'variation_id': variation_id, 'product_qty': product_qty, 'action': 'dsp_add_product_to_cart' },
            beforeSend: function() {},
            success: function(res) {
                if (res.status) {
                    $("#cart-table-summary").html(res.html);
                } else {
                    alertify.error(res.msg);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alertify.error(textStatus);
            }
        });
    });

    $('body').on("click", "button.remove-from-cart", function(e) {
        var cart_key = $(this).data("cart-key");
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: dropshipjs_salepage.ajax_url,
            data: { 'cart_key': cart_key, 'action': 'dsp_remove_product_from_cart' },
            beforeSend: function() {},
            success: function(res) {
                if (res.status) {
                    $("#cart-table-summary").html(res.html);
                } else {
                    alertify.error(res.msg);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alertify.error(textStatus);
            }
        });
    });

    $('.input-group.salepage-quantity').on('click', '.button-plus', function(e) {
        incrementValue(e);
    });

    $('.input-group.salepage-quantity').on('click', '.button-minus', function(e) {
        decrementValue(e);
    });

    $(".select-file-upload-slip").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".select-file-upload-slip-label").addClass("selected").html(fileName);
    });

    $("#upload_slip").on("change", function(e) {
        readURL(this, '#upload_slip_preview');
    });

    $('body').on("keyup", "input#address_name", function(e) {
        var val = this.value;
        var cookie_name = Cookies.get("dsp_customer_cookie_name");
        if (cookie_name != '' && cookie_name != undefined) {
            cookie_name = cookie_name.replaceAll("+", " ");
        } else {
            cookie_name = '';
        }
        if (val === '' || cookie_name === '') {
            return;
        } else if (cookie_name.includes(val)) {
            var cookie_addr = Cookies.get("dsp_customer_cookie_address");
            var cookie_tel = Cookies.get("dsp_customer_cookie_tel");
            var cookie_email = Cookies.get("dsp_customer_cookie_email");

            var cookie_province = Cookies.get("dsp_customer_cookie_address_province");
            var cookie_district = Cookies.get("dsp_customer_cookie_address_district");
            var cookie_subdistrict = Cookies.get("dsp_customer_cookie_address_subdistrict");
            var cookie_zipcode = Cookies.get("dsp_customer_cookie_address_zipcode");
            if (cookie_tel != '' && cookie_tel != undefined) {
                cookie_tel = cookie_tel.replaceAll("+", " ");
            }
            if (cookie_email != '' && cookie_email != undefined) {
                cookie_email = cookie_email.replaceAll("+", " ");
            } else {
                cookie_email = '';
            }
            if (cookie_addr != '' && cookie_addr != undefined) {
                cookie_addr = cookie_addr.replaceAll("+", " ");
            }
            var pv = $("select#address_province").find("option[value=" + cookie_province + "]").text();
            var msg = cookie_addr + '<br />' + cookie_tel + '<br />' + cookie_email + '<br />' + pv + '<br />' + cookie_district + '<br />' + cookie_subdistrict + '<br />' + cookie_zipcode;
            alertify.confirm('Found your address data', msg, function() {
                $("input#address_name").val(cookie_name);
                $("input#address_tel").val(cookie_tel);
                $("input#address_email").val(cookie_email);
                $("textarea#address_address").val(cookie_addr);

                $("select#address_province").val(cookie_province).change();
                setTimeout(function() {
                    $("select#address_district").val(cookie_district).change();
                    setTimeout(function() {
                        $("select#address_subdistrict").val(cookie_subdistrict).change();
                        setTimeout(() => {
                            if ($('select#address_subdistrict option[value=' + cookie_subdistrict + ']').data("zipcode") == null) {
                                $("input#address_zipcode").val(cookie_zipcode);
                            }
                        }, 1000);
                    }, 1000);
                }, 1000);
            }, function() {
                document.cookie = 'dsp_customer_cookie_name=; Max-Age=0; path=/; domain=' + location.host;
                document.cookie = 'dsp_customer_cookie_address=; Max-Age=0; path=/; domain=' + location.host;
                document.cookie = 'dsp_customer_cookie_tel=; Max-Age=0; path=/; domain=' + location.host;
                document.cookie = 'dsp_customer_cookie_email=; Max-Age=0; path=/; domain=' + location.host;
                document.cookie = 'dsp_customer_cookie_address_province=; Max-Age=0; path=/; domain=' + location.host;
                document.cookie = 'dsp_customer_cookie_address_district=; Max-Age=0; path=/; domain=' + location.host;
                document.cookie = 'dsp_customer_cookie_address_subdistrict=; Max-Age=0; path=/; domain=' + location.host;
                document.cookie = 'dsp_customer_cookie_address_zipcode=; Max-Age=0; path=/; domain=' + location.host;
            });
        }
    });

    $("#address_province").on("change", function() {
        $("#address_district").empty();
        $("#address_district").append('<option value="0">' + dropshipjs_main.lang.choose + '</option>');

        $("#address_subdistrict").empty();
        $("#address_subdistrict").append('<option value="0">' + dropshipjs_main.lang.choose + '</option>');

        $("input#address_zipcode").val('');

        dsp_generate_select_district();
    });

    $("#address_district").on("change", function() {
        $("#address_subdistrict").empty();
        $("#address_subdistrict").append('<option value="0">' + dropshipjs_main.lang.choose + '</option>');

        $("input#address_zipcode").val('');

        dsp_generate_select_subdistrict();
    });

    $("#address_subdistrict").on("change", function() {
        var zipcode = $(this).find('option:selected').data("zipcode");
        $("input#address_zipcode").val(zipcode);
    });

    $(".dropship-tabs-payment").on("click", function() {
        var tab = $(this).val();
        $(".dropship-tabs-payment-content").hide();
        $("." + tab).fadeIn();
    });

    $(".dsp-copy-bank-number").on("click", function() {
        var div = $(this).closest("div.block-payment-bacs-form");
        // console.log(div.find("#bank_copy").text());

        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(div.find("#bank_copy").text()).select();
        document.execCommand("copy");
        $temp.remove();

        alertify.success("คัดลอกเรียบร้อย!");
    });

    $(".dropship-popup-qrcode").on("click", function() {
        Swal.fire({
            imageUrl: dropshipjs_catalog.qrcode_promtpay,
            // imageHeight: 1500,
            // imageAlt: 'A tall image',
            html: dropshipjs_catalog.qrcode_promtpay_step,
            showCancelButton: true,
            confirmButtonText: '<i class="fa fa-download"></i> บันทึก',
            cancelButtonText: 'ปิด'
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                var link = document.createElement('a');
                link.href = dropshipjs_catalog.qrcode_promtpay;
                link.download = "qrcode";
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        })
    });

    function dsp_generate_select_province() {
        // var url = dropshipjs_catalog.province;
        // $.getJSON(url, function(data) {
        //     $.each(data, function(id, val) {
        //         $("#address_province").append('<option data-id="' + id + '" value="TH-' + id + '">' + val.name.th + '</option>');
        //     });
        // });

        var url = dropshipjs_catalog.full_address;
        var province_code = dropshipjs_catalog.province_code;
        $.getJSON(url, function(data) {
            $.each(data, function(id, val) {
                var code = getKeyByValue(province_code, id);
                $("#address_province").append('<option data-id="' + id + '" value="' + code + '">' + id + '</option>');
            });
        });
    }

    function dsp_generate_select_district() {
        // var url = dropshipjs_catalog.district;
        // var _id = $('#address_province option:selected').data("id");
        // $.getJSON(url, function(data) {
        //     $("#address_district").empty();
        //     $("#address_district").append('<option value="0">' + dropshipjs_main.lang.choose + '</option>');
        //     $.each(data, function(id, val) {
        //         if (val.changwat_id == _id) {
        //             $("#address_district").append('<option data-id="' + id + '" value="' + val.name.th + '">' + val.name.th + '</option>');
        //         }
        //     });
        // });


        var url = dropshipjs_catalog.full_address;
        var _id = $('#address_province option:selected').data("id");
        $.getJSON(url, function(data) {
            $("#address_district").empty();
            $("#address_district").append('<option value="0">' + dropshipjs_main.lang.choose + '</option>');
            $.each(data[_id], function(id, val) {
                $("#address_district").append('<option data-id="' + id + '" value="' + id + '">' + id + '</option>');
            });
        });
    }

    function dsp_generate_select_subdistrict() {
        // var url = dropshipjs_catalog.subdistrict;
        // var _id = $('#address_district option:selected').data("id");
        // $.getJSON(url, function(data) {
        //     $("#address_subdistrict").empty();
        //     $("#address_subdistrict").append('<option value="0">' + dropshipjs_main.lang.choose + '</option>');
        //     $.each(data, function(id, val) {
        //         if (val.amphoe_id == _id) {
        //             $("#address_subdistrict").append('<option data-id="' + id + '" data-zipcode="' + val.zipcode + '" value="' + val.name.th + '">' + val.name.th + '</option>');
        //         }
        //     })
        // });

        var url = dropshipjs_catalog.full_address;
        var p_id = $('#address_province option:selected').data("id");
        var _id = $('#address_district option:selected').data("id");
        $.getJSON(url, function(data) {
            $("#address_subdistrict").empty();
            $("#address_subdistrict").append('<option value="0">' + dropshipjs_main.lang.choose + '</option>');
            $.each(data[p_id][_id], function(id, val) {
                $("#address_subdistrict").append('<option data-id="' + id + '" data-zipcode="' + val + '" value="' + id + '">' + id + '</option>');
            })
        });
    }

    function readURL(input, previewId) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(previewId).attr("src", e.target.result);
                $(previewId).hide();
                $(previewId).fadeIn(650);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function addToCart(e, option) {
        var card = $(e).closest("div.card");
        var product_id = $(card).data("product-id");
        var variation_id = $(card).data("variation-id");
        var product_qty = $(card).find("input[name='quantity']").val();

        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: dropshipjs_salepage.ajax_url,
            data: { 'option': option, 'product_id': product_id, 'variation_id': variation_id, 'product_qty': product_qty, 'action': 'dsp_add_product_to_cart' },
            beforeSend: function() {},
            success: function(res) {
                if (res.status) {
                    $("#cart-table-summary").html(res.html);
                } else {
                    alertify.error(res.msg);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alertify.error(textStatus);
            }
        });
    }

    function incrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);
        var max = parseInt(parent.find('input[name=' + fieldName + ']').attr("max"));

        if (currentVal + 1 > max) {
            return;
        }
        if (!isNaN(currentVal)) {
            parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(1);
        }
        addToCart(e.target, 'plus');
    }

    function decrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);
        var min = parseInt(parent.find('input[name=' + fieldName + ']').attr("min"));

        if (currentVal - 1 < min) {
            return;
        }
        if (!isNaN(currentVal) && currentVal > 0) {
            parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(1);
        }
        addToCart(e.target, 'minus');
    }

    function CopyToClipboard(id) {
        var r = document.createRange();
        r.selectNode(document.getElementById(id));
        window.getSelection().removeAllRanges();
        window.getSelection().addRange(r);
        document.execCommand('copy');
        window.getSelection().removeAllRanges();
    }

    function getKeyByValue(object, value) {
        return Object.keys(object).find(key => object[key] === value);
    }

}(jQuery));