(function($) {
    'use strict';
    $(function() {
        if ($("#product-catalog-block").length > 0) {
            var page = 1;
            dsp_generate_products_catalog_sale(page);
        }

        $('body').on("click", "button.dropship-btn-product-to-catalog-page", function(e) {
            var page = $(this).data("page-id");
            dsp_generate_products_catalog_sale(page);
        });

        function dsp_generate_products_catalog_sale(page) {
            var categories = location.search.split('categories=')[1];
            var per_page = $("input[type='hidden']#per_page").val();
            if (categories == undefined) {
                categories = 0
            }

            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: dropshipjs_catalog.ajax_url,
                data: { action: 'dropship_get_product_catalog', page: page, categories: categories, per_page: per_page },
                beforeSend: function() {},
                success: function(res) {
                    if (res.status) {
                        $("#product-catalog-block").html(res.html);
                    } else {
                        alertify.error(res.msg);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alertify.error(textStatus);
                }
            });
        }
    });
}(jQuery));