<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Dropship
 */

get_header();
?>
	<div class="">
		<div class="">
			<?php if ( 'page' == get_post_type() && !is_shop()) : ?>
				<div class="">
					<main id="primary" class="site-main">
						<?php
						while ( have_posts() ) :
							the_post();
							get_template_part( 'template-parts/content', 'page' );
						endwhile; // End of the loop.
						?>
					</main><!-- #main -->
				</div>
			<?php else : ?>
				<div class="col-md-4 col-lg-4 mb-5">
					<?php
						get_sidebar();
					?>
				</div>
				<div class="col-md-8 col-lg-8 mb-5">
					<main id="primary" class="site-main">
						<?php
						while ( have_posts() ) :
							the_post();
							get_template_part( 'template-parts/content', 'page' );
						endwhile; // End of the loop.
						?>
					</main><!-- #main -->
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php
	get_footer();
?>