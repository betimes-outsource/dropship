<?php

/**
 * Dropship functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Dropship
 */

if (!defined('DROPSHIP_THEME_VERSION')) {
	// Replace the version number of the theme on each release.
	define('DROPSHIP_THEME_VERSION', '1.0.7');
}
remove_action('shutdown', 'wp_ob_end_flush_all', 1);

if (!function_exists('dropship_setup')) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function dropship_setup()
	{
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Dropship, use a find and replace
		 * to change 'dropship' to the name of your theme in all the template files.
		 */
		load_theme_textdomain('dropship', get_template_directory() . '/languages');

		// Add default posts and comments RSS feed links to head.
		add_theme_support('automatic-feed-links');

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support('title-tag');

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support('post-thumbnails');

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__('Primary', 'dropship'),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'dropship_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support('customize-selective-refresh-widgets');

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action('after_setup_theme', 'dropship_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function dropship_content_width()
{
	$GLOBALS['content_width'] = apply_filters('dropship_content_width', 640);
}
add_action('after_setup_theme', 'dropship_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function dropship_widgets_init()
{
	register_sidebar(
		array(
			'name'          => esc_html__('Sidebar', 'dropship'),
			'id'            => 'sidebar-1',
			'description'   => esc_html__('Add widgets here.', 'dropship'),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action('widgets_init', 'dropship_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function dropship_scripts()
{
	wp_enqueue_style('dropship-style', get_stylesheet_uri(), array(), DROPSHIP_THEME_VERSION);
	wp_style_add_data('dropship-style', 'rtl', 'replace');

	wp_enqueue_style('dropship-theme-style', get_template_directory_uri() . '/css/styles.css', array(), DROPSHIP_THEME_VERSION);
	wp_enqueue_style('dropship-theme-modal', get_template_directory_uri() . '/css/sweetalert2.min.css', array(), DROPSHIP_THEME_VERSION);

	wp_enqueue_script('dropship-theme-modal', get_template_directory_uri() . '/js/sweetalert2.all.min.js', array('jquery'), DROPSHIP_THEME_VERSION, true);
	wp_enqueue_script('dropship-navigation', get_template_directory_uri() . '/js/navigation.js', array(), DROPSHIP_THEME_VERSION, true);
	wp_enqueue_script('dropship-theme-bootstrap', get_template_directory_uri() . '/js/bootstrap.bundle.min.js', array(), DROPSHIP_THEME_VERSION, true);
	wp_enqueue_script('dropship-theme-scripts', get_template_directory_uri() . '/js/scripts.js', array(), DROPSHIP_THEME_VERSION, true);
	wp_enqueue_script('dropship-theme-main', get_template_directory_uri() . '/js/main.js', array(), DROPSHIP_THEME_VERSION, true);
	wp_enqueue_script('dropship-theme-catalog', get_template_directory_uri() . '/js/catalog.js', array('jquery'), DROPSHIP_THEME_VERSION, true);
	wp_enqueue_script('dropship-theme-salepage', get_template_directory_uri() . '/js/salepage.js', array('jquery'), DROPSHIP_THEME_VERSION, true);

	wp_localize_script('dropship-theme-main', 'dropshipjs_main', array(
		'ajax_url' => admin_url('admin-ajax.php'),
		'theme_url' => get_template_directory_uri(),
		'lang' => [
			'choose' => esc_html__('เลือก', 'dropship'),
		]
	));
	wp_localize_script('dropship-theme-catalog', 'dropshipjs_catalog', array(
		'ajax_url' => admin_url('admin-ajax.php'),
		'province' => get_template_directory_uri() . '/js/province.json',
		'district' => get_template_directory_uri() . '/js/district.json',
		'subdistrict' => get_template_directory_uri() . '/js/subdistrict.json',
		'full_address' => get_template_directory_uri() . '/js/full_address.json',
		'province_code' => dropship_woocommerce_match_states_thai(),
		'qrcode_promtpay' => (get_option('_dropship_qrcode_promtpay', 0)) ? wp_get_attachment_image_src(get_option('_dropship_qrcode_promtpay', 0), 'full')[0] : "https://placeholder.pics/svg/300x300",
		'qrcode_promtpay_step' => get_option('_dropship_qrcode_promtpay_step', '')
	));
	wp_localize_script('dropship-theme-salepage', 'dropshipjs_salepage', array('ajax_url' => admin_url('admin-ajax.php')));

	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
}
add_action('wp_enqueue_scripts', 'dropship_scripts');

/**
 * Remove related products output
 */
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);

function dropship_get_custom_post_type_template($single_template)
{
	global $post;
	if ($post->post_type == 'product') {
		$single_template = dirname(__FILE__) . '/salepage/theme1.php';
	}
	return $single_template;
}
add_filter('single_template', 'dropship_get_custom_post_type_template');

function dropship_template_redirect()
{
	if (is_shop() || is_product()) :
		WC()->cart->empty_cart();
	endif;

	if (is_shop()) {
		die();
	}
}
add_action('template_redirect', 'dropship_template_redirect');

add_filter('woocommerce_states', 'dropship_woocommerce_states_thai');

add_shortcode('dsp_catalog_page', 'dsp_sale_catalog_page');
add_shortcode('dsp_complete_page', 'dsp_place_order_complete_page');
add_shortcode('dsp_tracking_page', 'dsp_tracking_page');

function dsp_tracking_page($atts)
{
	extract(shortcode_atts(array(), $atts, 'dsp_tracking_page'));

	if (is_admin() || $_SERVER['REQUEST_METHOD'] === 'POST' || (isset($_GET['action']) && $_GET['action'] == "action")) {
		return;
	}

	$order_id = (isset($_GET['order_id'])) ? $_GET['order_id'] : '';

	ob_start();
?>
	<style>
		header.entry-header {
			display: none;
		}
	</style>

	<section class="page-section">
		<div class="container ds-block-container mt-5 p-3">
			<div class="row">
				<div class="col text-center">
					<h3 class="mt-3 text-center ds-content-title"><?php echo esc_html__('ติดตามสถานะคำสั่งซื้อ', 'dropship'); ?></h3>
					<p class="text-center"><?php echo esc_html__('กรุณากรอกหมายเลขคำสั่งซื้อ', 'dropship'); ?></p>
					<form action="" method="GET">
						<div class="panel panel-default">
							<div class="panel-body">
								<input type="text" autocomplete="off" name="order_id" class="form-control ds-form-input" placeholder="<?php echo esc_html__('เลขที่คำสั่งซื้อ', 'dropship'); ?>" value="<?php echo esc_attr($order_id); ?>" required>
							</div>
							<div class="panel-footer">
								<button type="submit" class="btn ds-btn-orange mt-3"><?php echo esc_html__('ตรวจสอบสถานะ', 'dropship'); ?></button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<?php
			if (!empty($order_id)) {
				echo dropship_order_detail_section($order_id);
			}
			?>
		</div>
	</section>

<?php
	$html = ob_get_clean();

	return $html;
}

function dropship_get_product_catalog()
{
	$categories_ids = [];
	$self = "A";
	$items_per_page = (isset($_REQUEST['per_page']) && $_REQUEST['per_page'] > 0) ? $_REQUEST['per_page'] : 1;
	$page = (isset($_REQUEST['page']) && $_REQUEST['page'] > 0) ? $_REQUEST['page'] : 1;
	if (!empty($_REQUEST['categories']) && $_REQUEST['categories'] !== 0) {
		$categories_ids[] = $_REQUEST['categories'];
	}

	$dsp_ref_id = false;
	if (isset($_COOKIE['dsp_ref_id'])) {
		$dsp_ref_id = $_COOKIE['dsp_ref_id'];
	}
	if ($dsp_ref_id === false) {
		return dropship_response_json(['status' => false, 'msg' => 'Not found']);
	}


	$result = Dropship_Model_Products::get_instance()->dsp_get_post_join_sale_products_catalog($dsp_ref_id, $categories_ids, $page, $items_per_page, $self);
	$args = $result['result'];
	$total = $result['total'];
	$_args = array_chunk($args, 5);
	ob_start();
?>
	<?php if (!empty($categories_ids)) : ?>
		<h3><?php echo esc_html(get_term(reset($categories_ids))->name); ?></h3>
	<?php else : ?>
		<h3  id="total-product"><?php echo esc_html__('สินค้า', 'dropship'); ?>
		<?php echo esc_html($total) . " " . esc_html__('รายการ', 'dropship'); ?></h3>
	<?php endif; ?>
	<div class="row align-items-center mb-2">
	<?php foreach ($_args as $_arg) : ?>
		<!-- <div class="row align-items-center mb-2"> -->
			<?php foreach ($_arg as $res) : ?>
				<?php
				$product = wc_get_product($res['ID']);
				$image_content = wp_get_attachment_image_src($product->get_image_id(), 'shop_catalog');
				if ($image_content[1] < 300 || $image_content[2] < 300) {
					$image_url = wp_get_attachment_thumb_url($product->get_image_id());
				} else {
					$image_url = $image_content[0];
				}
				?>
				<div class="mb-3 ds-box-product">
					<a href="<?php echo esc_url(get_post_permalink($product->get_id())); ?>">
						<div class="card ds-sale-cat">
							<!-- <?php // echo get_the_post_thumbnail( $res['ID'], 'shop_catalog', ['alt' => $product->get_name(), 'class' => 'card-img-top img-fluid'] ); 
									?> -->
							<div class="ds-block-image">
								<img src="<?php echo esc_url($image_url); ?>" alt="<?php echo esc_attr($product->get_name()); ?>" class="card-img-top img-fluid">
							</div>
							<div class="card-body ds-text-center">
								<h5 class="card-title"><?php echo esc_html($product->get_name()); ?></h5>
								<div class="pure">
									<p class="ds-price-sale-cat"><?php echo wc_price($product->get_price()); ?></p>
								</div>
							</div>
						</div>
					</a>
				</div>

			<?php endforeach; ?>
		<!-- </div> -->
	<?php endforeach; ?>
	</div>

	<div class="row text-center mt-5">
		<div class="col" style="text-align: right;">
			<input type="hidden" value="<?php echo esc_attr($page); ?>" class="dropship-product-to-catalog-cpage">
			<button type="button" data-page-id="<?php echo esc_attr(($page - 1)); ?>" <?php echo ($page == 1) ? esc_attr("disabled") : ""; ?> class="btn dropship-btn-product-to-catalog-page ds-btn-grey" id="btnpage"><?php echo esc_html__('ก่อนหน้า', 'dropship'); ?></button>
			<button type="button" data-page-id="<?php echo esc_attr(($page + 1)); ?>" <?php echo (ceil($total / $items_per_page) == $page) ? esc_attr("disabled") : ""; ?> class="btn dropship-btn-product-to-catalog-page ds-btn-grey" id="btnpage"><?php echo esc_html__('ถัดไป', 'dropship'); ?></button>
		</div>
	</div>
<?php
	$html = ob_get_clean();

	return dropship_response_json(['status' => true, 'html' => $html]);
}

function dsp_sale_catalog_page($atts)
{
	extract(shortcode_atts(array(
		'per_page' => 8
	), $atts, 'dsp_catalog_page'));

	if (is_admin() || $_SERVER['REQUEST_METHOD'] === 'POST' || (isset($_GET['action']) && $_GET['action'] == "action")) {
		return;
	}

	$categories_ids = $count_categories = [];
	$self = true;
	if (!empty($_REQUEST['categories']) && $_REQUEST['categories'] !== 0) {
		$categories_ids[] = $_REQUEST['categories'];
	}

	$dsp_ref_id = false;
	if (isset($_COOKIE['dsp_ref_id'])) {
		$dsp_ref_id = $_COOKIE['dsp_ref_id'];
	}
	if ($dsp_ref_id === false) {
		// wp_redirect( site_url() );
		// die;
	}

	$users_banners = get_user_meta($dsp_ref_id, '_dsp_sale_banners', true);
	$limit = false;
	ob_start();
?>
	<style>
		header.entry-header {
			display: none;
		}
	</style>
	<section class="page-section">
		<?php if (!empty($users_banners)) : ?>
			<?php foreach ($users_banners as $image_id) : ?>
				<?php
				if ($image_id == 0 || $limit) {
					continue;
				}
				$limit = true;
				$image = wp_get_attachment_image_src($image_id, 'full');
				?>
				<div class="container p-0 text-center ds-box-banner ds-layout-responsive">
					<img src="<?php echo esc_url($image[0]); ?>" class="img-fluid" style="width: 100%;">
				</div>
			<?php endforeach; ?> 
		<?php endif; ?>
		<div class="container mt-3 ds-layout-responsive">
			<div class="row">
				<div class="ds-catalog-left">
					<div class="ds-header-cat2" id="Order-Detail-Label2">
						<div id="icon-users" class="icon32"></div>
						<h5 class="wp-heading-inline ds-menu-sale-cat" id="total-product"><?php echo esc_html__('หมวดหมู่สินค้า', 'dropship'); ?></h5>
						
					</div>
					<!-- <hr class="wp-header-end"> -->
					<div class="scroll-menu">
						<div class="list-group">
							<a href="<?php echo get_site_url(); ?>" class="list-group-item list-group-item-action"><?php echo esc_html__('สินค้าทั้งหมด', 'dropship'); ?></a>

							<?php
							$taxonomy     = 'product_cat';
							$orderby      = 'name';
							$show_count   = 0;      // 1 for yes, 0 for no
							$pad_counts   = 0;      // 1 for yes, 0 for no
							$hierarchical = 1;      // 1 for yes, 0 for no  
							$title        = '';
							$empty        = true;

							$args = array(
								'taxonomy'     => $taxonomy,
								'orderby'      => $orderby,
								'show_count'   => $show_count,
								'pad_counts'   => $pad_counts,
								'hierarchical' => $hierarchical,
								'title_li'     => $title,
								'hide_empty'   => $empty
							);

							$all_categories = get_categories($args);
							foreach ($all_categories as $cat) {
								if ($cat->category_parent == 0) {
									$category_id = $cat->term_id;
									$c = Dropship_Model_Products::get_instance()->dsp_get_post_join_sale_products_catalog($dsp_ref_id, [$category_id], 1, 1, $self);
									if ($c['total'] === 0) {
										continue;
									}
									$count_categories[$category_id] = $c['total'];
									$currentURL = add_query_arg(['categories' => $category_id]);
							?>


								<a href="<?php echo esc_url($currentURL); ?>" class="list-group-item list-group-item-action 
								<?php echo (isset($_GET['categories']) && $_GET['categories'] == $category_id) ? esc_attr("active") : ""; ?>" data-cat-id="
								<?php echo esc_attr($category_id); ?>">
										<?php echo esc_html($cat->name . " (" . $c['total'] . ")"); ?></a>
									<?php
									$args2 = array(
										'taxonomy'     => $taxonomy,
										'child_of'     => 0,
										'parent'       => $category_id,
										'orderby'      => $orderby,
										'show_count'   => $show_count,
										'pad_counts'   => $pad_counts,
										'hierarchical' => $hierarchical,
										'title_li'     => $title,
										'hide_empty'   => $empty
									);
									$sub_cats = get_categories($args2);
									if ($sub_cats) {
										foreach ($sub_cats as $sub_category) {
											$_category_id = $sub_category->term_id;
											$c = Dropship_Model_Products::get_instance()->dsp_get_post_join_sale_products_catalog($dsp_ref_id, [$_category_id], 1, 1, $self);
											if ($c['total'] === 0) {
												continue;
											}
											$count_categories[$_category_id] = $c['total'];
											$currentURL = add_query_arg(['categories' => $_category_id]);
									?>
											<a href="<?php echo esc_url($currentURL); ?>" class="list-group-item list-group-item-action <?php echo (isset($_GET['categories']) && $_GET['categories'] == $_category_id) ? esc_attr("active") : ""; ?>" data-cat-id="<?php echo esc_attr($_category_id); ?>"> - <?php echo esc_html($sub_category->name . " (" . $c['total'] . ")"); ?></a>
							<?php
										}
									}
								}
							}
							?>
						</div>
					</div>
					<input type="hidden" id="per_page" value="<?php echo esc_attr($per_page); ?>">
				</div>

				
				<div class="ds-catalog-right" id="product-catalog-block">

				</div>
			</div>
		</div>
	</section>
	<?php
	$html = ob_get_clean();

	return $html;
}
add_action('wp_ajax_dropship_get_product_catalog', 'dropship_get_product_catalog');
add_action('wp_ajax_nopriv_dropship_get_product_catalog', 'dropship_get_product_catalog');

function dsp_place_order_complete_page($atts)
{
	$atts = shortcode_atts(array(), $atts, 'dsp_complete_page');

	if (is_admin() || $_SERVER['REQUEST_METHOD'] === 'POST' || (isset($_GET['action']) && $_GET['action'] == "action")) {
		return;
	}

	$order_id  = (isset($_REQUEST['order_id'])) ? sanitize_text_field($_REQUEST['order_id']) : '';
	$ss_order_id = WC()->session->get('order_id');
	if (!empty($order_id) && $order_id == $ss_order_id) {
		ob_start();
	?>
		<style>
			header.entry-header {
				display: none;
			}
		</style>
		<section class="page-section">
			<div class="container">
				<div class="row">
					<div class="col text-center">
						<h3 class="mt-3"><?php echo esc_html__('ขอบคุณ', 'dropship'); ?></h3>
						<p><?php echo esc_html__('เราจะทำการจัดส่งสินค้าถึงท่านภายใน 1-2 วัน', 'dropship'); ?></p>
					</div>
				</div>
				<?php echo dropship_order_detail_section($order_id); ?>
			</div>
		</section>
	<?php
		$html = ob_get_clean();
	} else {
		ob_start();
	?>
		<style>
			header.entry-header {
				display: none;
			}
		</style>
		Denind
	<?php
		$html = ob_get_clean();
	}

	return $html;
}

function dropship_order_detail_section($order_id)
{
	$order = wc_get_order($order_id);
	if (empty($order)) {
		return '';
	}
	$qty = 0;
	$shippingTotal = $order->get_total_shipping();
	$_dsp_courier_name = get_post_meta($order_id, '_dsp_courier_name', true);
	$_dsp_tracking_number = get_post_meta($order_id, '_dsp_tracking_number', true);
	// $_dsp_tracking_status = get_post_meta($order_id, '_dsp_tracking_status', true);

	$_status = 1;
	if (!empty($_dsp_courier_name) && !empty($_dsp_tracking_number) && ($order->get_status() == "completed")) {
		$_status = 3;
	} elseif (!empty($_dsp_courier_name) && !empty($_dsp_tracking_number)) {
		$_status = 2;
	}
	ob_start();
	?>

	<div class="row">
		<div class="col-lg-12 text-center">
			<ul id="progressbar">
				<li class="first <?php echo ($_status > 0) ? esc_attr('active') : "" ?>" id="account"><strong style="float: left;"><?php echo esc_html__('ตรวจสอบการชำระเงิน', 'dropship'); ?></strong></li>
				<li class="second <?php echo ($_status > 1) ? esc_attr('active') : "" ?>" id="personal"><strong><?php echo esc_html__('กำลังจัดส่ง', 'dropship'); ?></strong></li>
				<li class="third <?php echo ($_status > 2) ? esc_attr('active') : "" ?>" id="payment"><strong style="float: right;"><?php echo esc_html__('เสร็จสมบูรณ์', 'dropship'); ?></strong></li>
			</ul>
		</div>
	</div>
	<div class="row ">
		<div class="col">
			<p class="mb-1"><?php echo esc_html__('เลขที่คำสั่งซื้อ', 'dropship'); ?> : #<?php echo esc_html($order_id); ?></p>
			<p class="mb-1"><?php echo esc_html__('วันที่สั่งซื้อ', 'dropship'); ?> : <?php echo esc_html($order->get_date_modified()->date("d/m/Y H:i:s")); ?></p>
			<?php if ($_dsp_courier_name && $_dsp_tracking_number) : ?>
				<p class="mb-1"><?php echo esc_html__('ชื่อบริษัทขนส่ง', 'dropship'); ?> : <?php echo esc_html($_dsp_courier_name); ?></p>
				<p class="mb-1"><?php echo esc_html__('หมายเลขติดตามพัสดุ', 'dropship'); ?> : <?php echo esc_html($_dsp_tracking_number); ?></p>
				<!-- <p class="mb-1"><?php //echo esc_html__( 'Tracking status', 'dropship' ); 
										?> : <?php // echo $_dsp_tracking_status; 
																									?></p> -->
			<?php endif; ?>
		</div>
	</div>
	<hr />
	<div class="row">
		<div class="col">
			<table class="table table-hover table-summary">
				<thead class="thead-light">
					<tr>
						<th style="width: 70%;"><?php esc_html_e('สินค้า', 'dropship'); ?></th>
						<th class="text-center" style="width: 15%;"><?php esc_html_e('จำนวนรวม', 'dropship'); ?></th>
						<th class="text-center" style="width: 15%;"><?php esc_html_e('Total', 'dropship'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($order->get_items() as $item_id => $item) : ?>
						<?php
						$item = new WC_Order_Item_Product($item_id);
						$cart_product_id = $item->get_product_id();
						$cart_variation_id = $item->get_variation_id();
						if ($cart_variation_id) {
							$cart_product_name = get_the_title($cart_variation_id);
						} else {
							$cart_product_name = get_the_title($cart_product_id);
						}
						$cart_product_qty = $item->get_quantity();
						$qty += $cart_product_qty;
						$cart_line_subtotal = $item->get_total();
						?>
						<tr>
							<td><?php echo esc_html($cart_product_name); ?></td>
							<td class="text-center"><?php echo esc_html($cart_product_qty); ?></td>
							<td class="text-center"><?php echo (wc_price($cart_line_subtotal)); ?></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
				<tfoot class="border-button-hilight">
					<tr>
						<td><?php esc_html_e('ราคาค่าจัดส่ง', 'dropship'); ?></td>
						<td class="text-center"><?php echo esc_html($qty); ?></td>
						<td class="text-center"><?php echo (wc_price($shippingTotal)); ?></td>
					</tr>
					<tr>
						<td colspan="2"><?php esc_html_e('Summary', 'dropship'); ?></td>
						<td class="text-center"><?php echo (wc_price($order->get_total())); ?></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>

<?php
	$html = ob_get_clean();

	return $html;
}

function dropship_response_json($args = [])
{
	header('Content-type: application/json');
	echo json_encode($args, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
	die;
}

function dropship_woocommerce_match_states_thai()
{
	return array(
		'TH-81' => 'กระบี่',
		'TH-10' => 'กรุงเทพมหานคร',
		'TH-71' => 'กาญจนบุรี',
		'TH-46' => 'กาฬสินธุ์',
		'TH-62' => 'กำแพงเพชร',
		'TH-40' => 'ขอนแก่น',
		'TH-22' => 'จันทบุรี',
		'TH-24' => 'ฉะเชิงเทรา',
		'TH-20' => 'ชลบุรี',
		'TH-18' => 'ชัยนาท',
		'TH-36' => 'ชัยภูมิ',
		'TH-86' => 'ชุมพร',
		'TH-57' => 'เชียงราย',
		'TH-50' => 'เชียงใหม่',
		'TH-92' => 'ตรัง',
		'TH-23' => 'ตราด',
		'TH-63' => 'ตาก',
		'TH-26' => 'นครนายก',
		'TH-73' => 'นครปฐม',
		'TH-48' => 'นครพนม',
		'TH-30' => 'นครราชสีมา',
		'TH-80' => 'นครศรีธรรมราช',
		'TH-60' => 'นครสวรรค์',
		'TH-12' => 'นนทบุรี',
		'TH-96' => 'นราธิวาส',
		'TH-55' => 'น่าน',
		'TH-38' => 'บึงกาฬ',
		'TH-31' => 'บุรีรัมย์',
		'TH-13' => 'ปทุมธานี',
		'TH-77' => 'ประจวบคีรีขันธ์',
		'TH-25' => 'ปราจีนบุรี',
		'TH-94' => 'ปัตตานี',
		'TH-14' => 'พระนครศรีอยุธยา',
		'TH-56' => 'พะเยา',
		'TH-82' => 'พังงา',
		'TH-93' => 'พัทลุง',
		'TH-66' => 'พิจิตร',
		'TH-65' => 'พิษณุโลก',
		'TH-76' => 'เพชรบุรี',
		'TH-67' => 'เพชรบูรณ์',
		'TH-54' => 'แพร่',
		'TH-83' => 'ภูเก็ต',
		'TH-44' => 'มหาสารคาม',
		'TH-49' => 'มุกดาหาร',
		'TH-58' => 'แม่ฮ่องสอน',
		'TH-35' => 'ยโสธร',
		'TH-95' => 'ยะลา',
		'TH-45' => 'ร้อยเอ็ด',
		'TH-85' => 'ระนอง',
		'TH-21' => 'ระยอง',
		'TH-70' => 'ราชบุรี',
		'TH-16' => 'ลพบุรี',
		'TH-52' => 'ลำปาง',
		'TH-51' => 'ลำพูน',
		'TH-42' => 'เลย',
		'TH-33' => 'ศรีสะเกษ',
		'TH-47' => 'สกลนคร',
		'TH-90' => 'สงขลา',
		'TH-91' => 'สตูล',
		'TH-11' => 'สมุทรปราการ',
		'TH-75' => 'สมุทรสงคราม',
		'TH-74' => 'สมุทรสาคร',
		'TH-27' => 'สระแก้ว',
		'TH-19' => 'สระบุรี',
		'TH-17' => 'สิงห์บุรี',
		'TH-64' => 'สุโขทัย',
		'TH-72' => 'สุพรรณบุรี',
		'TH-84' => 'สุราษฎร์ธานี',
		'TH-32' => 'สุรินทร์',
		'TH-43' => 'หนองคาย',
		'TH-39' => 'หนองบัวลำภู',
		'TH-15' => 'อ่างทอง',
		'TH-37' => 'อำนาจเจริญ',
		'TH-41' => 'อุดรธานี',
		'TH-53' => 'อุตรดิตถ์',
		'TH-61' => 'อุทัยธานี',
		'TH-34' => 'อุบลราชธานี'
	);
}

function dropship_woocommerce_states_thai($states)
{
	$states['TH'] = dropship_woocommerce_match_states_thai();
	return $states;
}

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Custom Dropship Style
 */
function dropship_style(){
	wp_enqueue_style('dropship-theme-login-style', get_template_directory_uri() . '/css/dropship-style.css', array(), DROPSHIP_THEME_VERSION);
}
add_action('login_enqueue_scripts', 'dropship_style');