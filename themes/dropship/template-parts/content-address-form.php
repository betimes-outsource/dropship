<?php

?>

<div class="row g-3">
    <div class="col-md-4">
        <label for="address_name" class="form-label"><?php echo esc_html__( 'ชื่อ', 'dropship' ); ?>:</label><span class="-required">*</span>
        <input type="text" class="form-control" id="address_name" name="address_name" placeholder="" autocomplete="off" required>
    </div>
    <div class="col-md-4">
        <label for="address_tel" class="form-label"><?php echo esc_html__( 'เบอร์โทรศัพท์', 'dropship' ); ?>:</label><span class="-required">*</span>
        <input type="text" class="form-control" id="address_tel" name="address_tel" autocomplete="off" required>
    </div>
    <div class="col-md-4">
        <label for="address_email" class="form-label"><?php echo esc_html__( 'อีเมล', 'dropship' ); ?>:</label>
        <input type="email" class="form-control" id="address_email" name="address_email" autocomplete="off">
    </div>
    <div class="col-12">
        <label for="address_address" class="form-label"><?php echo esc_html__( 'ที่อยู่', 'dropship' ); ?></label><span class="-required">*</span>
        <textarea class="form-control" id="address_address" name="address_address" placeholder="" autocomplete="off" style="resize: none;" rows="2"></textarea>
    </div>
    <div class="col-md-3">
        <label for="address_province" class="form-label"><?php echo esc_html__( 'จังหวัด', 'dropship' ); ?>:</label><span class="-required">*</span>
        <select class="form-control" id="address_province" name="address_province">
            <option value="0"><?php echo esc_html__( 'เลือก', 'dropship' ); ?></option>
        </select>
    </div>
    <div class="col-md-3">
        <label for="address_district" class="form-label"><?php echo esc_html__( 'เขต/อำเภอ', 'dropship' ); ?>:</label><span class="-required">*</span>
        <select class="form-control" id="address_district" name="address_district">
            <option value="0"><?php echo esc_html__( 'เลือก', 'dropship' ); ?></option>
        </select>
    </div>
    <div class="col-md-3">
        <label for="address_subdistrict" class="form-label"><?php echo esc_html__( 'แขวง/ตำบล', 'dropship' ); ?>:</label><span class="-required">*</span>
        <select class="form-control" id="address_subdistrict" name="address_subdistrict">
            <option value="0"><?php echo esc_html__( 'เลือก', 'dropship' ); ?></option>
        </select>
    </div>
    <div class="col-md-3">
        <label for="address_zipcode" class="form-label"><?php echo esc_html__( 'รหัสไปรษณีย์', 'dropship' ); ?>:</label><span class="-required">*</span>
        <input type="text" class="form-control input-zipcode" id="address_zipcode" name="address_zipcode" autocomplete="off">
    </div>
</div>