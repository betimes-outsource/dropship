<?php
$payment_gateways = WC_Payment_Gateways::instance();
$payment_gateway = $payment_gateways->payment_gateways();

$payment_bacs = (isset($payment_gateway['bacs']) && $payment_gateway['bacs']->enabled == 'yes') ? $payment_gateway['bacs'] : false;
$payment_cod = (isset($payment_gateway['cod']) && $payment_gateway['cod']->enabled == 'yes') ? $payment_gateway['cod'] : false;
$payment_qrcode = (isset($payment_gateway['qrcode_promtpay']) && $payment_gateway['qrcode_promtpay']->enabled == 'yes') ? $payment_gateway['qrcode_promtpay'] : false;

// echo '<pre>';
// print_r($payment_qrcode);
// echo '</pre>';
?>

<!-- <div class="row">
    <?php if ($payment_bacs) : ?>
        <div class="col">
            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" class="custom-control-input" id="bank_transfer" name="payment_method" value="bacs" checked>
                <label class="custom-control-label" for="customRadio"><?php echo esc_html__('โอนเข้าบัญชี', 'dropship'); ?></label>
            </div>
        </div>
    <?php endif; ?>
    <?php if ($payment_cod) : ?>
        <div class="col">
            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" class="custom-control-input" id="cod_transfer" name="payment_method" value="cod">
                <label class="custom-control-label" for="customRadio2"><?php echo esc_html__('COD (Cash on Delivery)', 'dropship'); ?></label>
            </div>
        </div>
    <?php endif; ?>
</div> -->

<div class="w3-bar w3-black">
    <?php if ($payment_bacs) : ?>
        <button type="button" class="btn btn-primary btn-payment-border" data-tab="bacs">
            <div class="custom-control custom-radio custom-control-inline btn-payment-radio" style="border: none;">
                <input type="radio" class="custom-control-input dropship-tabs-payment" id="bank_transfer" name="payment_method" value="<?php echo $payment_bacs->id; ?>" checked>
                <label class="custom-control-label" for="customRadio"><?php echo esc_html__('โอนเข้าบัญชี', 'dropship'); ?></label>
            </div>
        </button>
    <?php endif; ?>
    <?php if ($payment_qrcode) : ?>
        <button type="button" class="btn btn-primary btn-payment-border" data-tab="qrcode">
            <div class="custom-control custom-radio custom-control-inline btn-payment-radio" style="border: none;">
                <input type="radio" class="custom-control-input dropship-tabs-payment" id="qrcode_transfer" name="payment_method" value="<?php echo $payment_qrcode->id; ?>">
                <label class="custom-control-label" for="customRadio2"><?php echo esc_html__('Promtpay', 'dropship'); ?></label>
            </div>
        </button>
    <?php endif; ?>
    <?php if ($payment_cod) : ?>
        <button type="button" class="btn btn-primary btn-payment-border" data-tab="cod">
            <div class="custom-control custom-radio custom-control-inline btn-payment-radio" style="border: none;">
                <input type="radio" class="custom-control-input dropship-tabs-payment" id="cod_transfer" name="payment_method" value="<?php echo $payment_cod->id; ?>">
                <label class="custom-control-label" for="customRadio2"><?php echo esc_html__('COD (Cash on Delivery)', 'dropship'); ?></label>
            </div>
        </button>
    <?php endif; ?>
</div>

<br />

<?php if ($payment_bacs) : ?>
    <div class="dropship-tabs-payment-content <?php echo $payment_bacs->id; ?>">
        <div class="row">
            <div class="col">
                <?php foreach ($payment_bacs->account_details as $k => $account_detail) : ?>
                    <!-- <div class="d-flex align-items-center block-payment-bacs-form <?php echo ($k != 0) ? esc_attr("no-border-top") : ""; ?> ">
                        <div class="flex-shrink-0">
                            <?php $bic = (isset($account_detail['bic'])) ? $account_detail['bic'] : "blank"; ?>
                            <img src="<?php echo esc_url(get_template_directory_uri() . "/assets/bank/" . $bic . ".png"); ?>" style="height: 65px;" class="img-fluid <?php echo esc_attr("bank-" . $bic); ?>">
                        </div>
                        <div class="flex-grow-1 ms-3 ds-left">
                            <div><label><?php echo esc_html($account_detail['bank_name']); ?></label></div>
                            <div><label><?php echo esc_html__('เลขที่บัญชี ', 'dropship'); ?>: </label>&nbsp;<label class="ds-label-bank" id="bank_copy"><?php echo esc_html($account_detail['account_number']); ?></label></div>
                            <div><label><?php echo esc_html__('ชื่อบัญชี ', 'dropship'); ?>: &nbsp;<?php echo esc_html($account_detail['account_name']); ?></label></div>
                        </div>
                        <div class="flex-grow-1 ms-3 ds-right">
                            <div class="ds-right"><button type="button" class="ds-button-bank dsp-copy-bank-number"><img class="ds-img-copy" src="/wp-content/themes/dropship/assets/img/copy-icon.png"></button></div>
                        </div>
                    </div> -->
                    <div class="row block-payment-bacs-form">
                        <div class="col-3 col-sm-1">
                            <?php $bic = (isset($account_detail['bic'])) ? $account_detail['bic'] : "blank"; ?>
                            <img src="<?php echo esc_url(get_template_directory_uri() . "/assets/bank/" . $bic . ".png"); ?>" style="height: 65px;" class="img-fluid <?php echo esc_attr("bank-" . $bic); ?>">
                        </div>
                        <div class="col">
                            <div><label><?php echo esc_html($account_detail['bank_name']); ?></label></div>
                            <div><label><?php echo esc_html__('เลขที่บัญชี ', 'dropship'); ?>: </label>&nbsp;<label class="ds-label-bank" id="bank_copy"><?php echo esc_html($account_detail['account_number']); ?></label></div>
                            <div><label><?php echo esc_html__('ชื่อบัญชี ', 'dropship'); ?>: &nbsp;<?php echo esc_html($account_detail['account_name']); ?></label></div>
                        </div>
                        <div class="col-3 col-sm-1">
                            <div class="ds-right"><button type="button" class="ds-button-bank dsp-copy-bank-number"><img class="ds-img-copy" src="/wp-content/themes/dropship/assets/img/copy-icon.png"></button></div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if ($payment_cod) : ?>
    <div class="dropship-tabs-payment-content <?php echo $payment_cod->id; ?>" style="display:none"></div>
<?php endif; ?>

<?php if ($payment_qrcode) : ?>
    <div class="dropship-tabs-payment-content <?php echo $payment_qrcode->id; ?>" style="display:none">
        <div class="row">
            <div class="col">
                <!-- <div class="d-flex align-items-center block-payment-bacs-form">
                    <div class="flex-shrink-0">
                        <img src="<?php echo esc_url(get_template_directory_uri() . "/assets/bank/promtpay.png"); ?>" style="height: 65px;" class="img-fluid">
                    </div>
                    <div class="flex-grow-1 ms-3 ds-left">
                        <div><label><?php echo esc_html__($payment_qrcode->title); ?></label></div>
                        <div><label><?php echo esc_html__($payment_qrcode->description); ?></label></div>
                    </div>
                    <div id="qrcode-instructions" style="display: none;">
                        <?php // echo esc_attr( $payment_qrcode->instructions ); ?>
                    </div>
                    <div class="flex-grow-1 ms-3 ds-right">
                        <div class="ds-right"><button type="button" class="ds-button-bank dropship-popup-qrcode"><img class="ds-img-copy" src="/wp-content/themes/dropship/assets/img/copy-icon.png"></button></div>
                    </div>
                </div> -->
                <div class="row block-payment-bacs-form">
                    <div class="col-3 col-sm-1">
                        <img src="<?php echo esc_url(get_template_directory_uri() . "/assets/bank/promtpay.png"); ?>" style="height: 65px;" class="img-fluid">
                    </div>
                    <div class="col">
                        <div><label><?php echo esc_html__($payment_qrcode->title); ?></label></div>
                        <div><label><?php echo esc_html__($payment_qrcode->description); ?></label></div>
                    </div>
                    <div class="col-3 col-sm-1">
                        <div class="ds-right"><button type="button" class="ds-button-bank dropship-popup-qrcode"><img class="ds-img-copy" src="/wp-content/themes/dropship/assets/img/copy-icon.png"></button></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>


<!-- <div class="clearfix"></div>
<br /> -->

<div class="clearfix"></div>
<br />

<div class="row">
    <div class="col">
        <div class="upload_slip">
            <h3><?php echo esc_html__('หลักฐานการโอนเงิน', 'dropship'); ?></h3>
            <p><?php echo esc_html__('กรุณาแนบหลักฐานการโอนเงิน', 'dropship'); ?></p>
            <div class="col-12">
                <div class="d-grid gap-2 my-fancy-container">
                    <!-- <i class="fa fa-upload" aria-hidden="true"></i> -->
                    <input type="file" class="select-file-upload-slip custom-file-input" id="upload_slip" name="upload_slip" accept="image/png, image/gif, image/jpeg" required>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <img id="upload_slip_preview" style="display: none;width: 250px;" class="mt-3 img-thumbnail">
                </div>
            </div>
        </div>
    </div>
</div>
<br><br>
<div class="col-12">
    <div class="d-grid gap-2">
        <button class="btn btn-primary submit-form" type="button"><?php echo esc_html__('ยืนยันคำสั่งซื้อ', 'dropship'); ?></button>
    </div>
</div>
<br><br>