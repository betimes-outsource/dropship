<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Dropship
 */

get_header();
?>
	<div class="">
		<div class="">
			<div class="">
				<main id="primary" class="site-main">
					<?php
					while (have_posts()) :
						the_post();
						get_template_part('template-parts/content', get_post_type());
					endwhile; // End of the loop.
					?>
				</main><!-- #main -->
			</div>
		</div>
	</div>
<?php
get_footer();
