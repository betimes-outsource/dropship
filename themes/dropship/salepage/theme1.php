<?php
get_header();

if ( have_posts() ) {
    global $woocommerce, $product, $post;
    $items = [];
    $product = wc_get_product();
    $product_id = $product->get_id();
    $product_type = $product->get_type();
    $feature_image = get_the_post_thumbnail_url( $product_id, 'full' );

    $itemsTotal = 1;
    if ( $product_type == 'simple' ) {
        $items[] = $product;
    } elseif ( $product_type == 'variable' ) {
        $children_ids = $product->get_children();
        $itemsTotal = count( $children_ids );
        foreach ($children_ids as $children_id) {
			$items[] = new WC_Product_Variation( $children_id );
        }
    }
    ?>
        <div class="container ds-salepage">
            <div class="row" style="background-color: #fff;">
                <div class="col-lg-2 mb-5" style="visibility: hidden;"></div>
                <div class="col-lg-8 mb-5">
                    <!-- <img src="<?php //echo esc_url($feature_image) ?>" class="img-fluid" /> -->
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
                        <h2 class="page-section-heading text-center text-uppercase" style="margin-top: 20px;">
                            <?php echo wp_kses_post(get_the_title()); ?>
                        </h2>

                        <div class="wp-block-group has-background" style="background:linear-gradient(135deg,rgb(255,241,202) 0%,rgb(255,221,126) 100%)">
                            <div class="wp-block-group__inner-container">
                                <p class="text-center"><?php echo the_excerpt(); ?></p>
                            </div>
                        </div>

                        <p><?php echo the_content(); ?></p>

                        <?php
                            $attachment_ids = $product->get_gallery_image_ids();
                            foreach( $attachment_ids as $attachment_id ) {
                                $image_link = wp_get_attachment_url( $attachment_id );
                                echo '<img src="'. $image_link .'" class="img-fluid" />';
                            }
                        ?>
                        <br>

                        <h3><?php echo esc_html__( 'ซื้อสินค้าโดย', 'dropship' ); ?></h3>
                        <p><?php echo esc_html__( 'เลือกตัวเลือกและจำนวนสินค้า', 'dropship' ); ?></p>
                        <div class="row justify-content-center product-section" id="product-section">
                            <?php foreach ($items as $item) : ?>
                                <div class="col-12 col-sm-8 col-md-6 col-lg-4">
                                    <?php
                                        $id = ( $product_type == 'simple' ) ? 0 : $item->get_id();
                                        $pd_type = $item->get_type();
                                        $product_name = $item->get_name();
                                        $product_sku = $item->get_sku();
                                        $product_img = get_the_post_thumbnail_url( $item->get_id() );
                                        if ( empty($product_img) ) {
                                            $product_img = get_the_post_thumbnail_url( $product_id );
                                        }
                                        if ( $product->is_in_stock() ) {
                                            $stock_qty = $item->get_stock_quantity();
                                            if ( is_null( $stock_qty ) ) {
                                                $stock_qty = 9999999;
                                            }
                                        } else {
                                            $stock_qty = 0;
                                        }

                                    ?>
                                    <div class="card" data-product-id="<?php echo esc_attr($product_id); ?>" data-variation-id="<?php echo esc_attr($id); ?>" data-product-type="<?php echo esc_attr($pd_type); ?>">
                                        <img class="card-img img-fluid" src="<?php echo esc_url($product_img); ?>">
                                        <div class="card-body">
                                            <h4 class="card-title"><?php echo esc_html($product_name); ?></h4>
                                            <h6 class="card-subtitle mb-2 text-muted"><?php echo esc_html__( 'SKU', 'dropship' ); ?> : <?php echo esc_html($product_sku); ?></h4></h6>
                                            <h4 class="card-title text-center"><?php echo ($item->get_price_html()); ?></h4>
                                            <?php if ( $stock_qty > 0 ) : ?>
                                                <div class="buy d-flex justify-content-between align-items-center">
                                                    <div class="input-group salepage-quantity">
                                                        <input type="button" value="-" class="button-minus salepage-quantity" data-field="quantity" style="display: block;margin: 0 auto;right: -13px;">
                                                        <input type="number" step="1" min="0" max="<?php echo esc_html($stock_qty); ?>" value="0" name="quantity" readonly class="quantity-field salepage-quantity" style="display: block;margin: 0 auto;">
                                                        <input type="button" value="+" class="button-plus salepage-quantity" data-field="quantity" style="display: block;margin: 0 auto;">
                                                    </div>
                                                </div>
                                            <?php else : ?>
                                                <h3 class="text-red text-center"><?php echo esc_html__( 'สินค้าหมด', 'dropship' ); ?></h3>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                <?php if ( count($items) === 1 ) : ?>
                                    <div class="col-12 col-sm-8 col-md-6 col-lg-4"></div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            
                            <div class="clearfix"></div>
                            <br />
                            <div id="cart-table-summary"></div>
                        </div>

                        <br>
                        <form method="POST" id="form-payment-address" enctype="multipart/form-data">
                            <h3><?php echo esc_html__( 'ข้อมูลที่อยู่จัดส่ง', 'dropship' ); ?></h3>
                            <p><?php echo esc_html__( 'กรุณาตรวจสอบที่อยู่ก่อนยืนยันคำสั่งซื้อ', 'dropship' ); ?></p>
                            <?php get_template_part( 'template-parts/content-address-form', 'address-form' ); ?>

                            <br>
                            <h3><?php echo esc_html__( 'รูปแบบการชำระเงิน', 'dropship' ); ?></h3>
                            <p><?php echo esc_html__( 'เลือกรูปแบบชำระเงิน', 'dropship' ); ?></p>
                            <?php get_template_part( 'template-parts/content-payment-form', 'payment-form' ); ?>
                        </form>
                    </article><!-- #post-<?php the_ID(); ?> -->
                </div>
                <div class="col-lg-2 mb-5 d-none" style="visibility: hidden;"></div>
            </div>
        </div>
    <?php
}

get_footer();
